import React, { Component } from 'react';
import { inject } from 'mobx-react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import 'babel-polyfill'; // Promise 오류 관련

import PrivateRoute from 'modules/layouts/main/PrivateRoute'
import Spinner from 'components/atoms/spinner/Spinner';
import NoMatch from 'modules/layouts/match/NoMatch';
import MainLayout from 'modules/layouts/main/MainLayout'

import DirectPopupLayout from 'modules/layouts/popup/DirectPopupLayout';
import Storage from 'modules/pages/system/storage/Storage'

import 'style/layout.css';
import 'style/font.css';
import 'style/main.css'
import 'utils/object/DateFormat'

@inject(stores => ({
    checkToken: stores.commonStore.checkToken,
}))
class App extends Component {

    async componentWillMount() {
        if (window.location.pathname.indexOf('auth') < 0) {
            const data = await this.props.checkToken();

            if (data && data.result != 'success') {
                window.location.href = '/auth/login';
                // window.location.href = '/auth/app';
            }
        }

        history.pushState(null, null, location.href);
        window.onpopstate = function () {
            history.go(1);
        };
    }

    render() {
        return (
            <React.Fragment>
                <Spinner />
                <BrowserRouter>
                    <Switch>
                        <Route path="/popup" component={DirectPopupLayout} />
                        <Route path="/download/:target" component={Storage} />
                        <PrivateRoute path="/app" component={MainLayout} />
                        <Redirect from="/" to="/app" />
                        <Route component={NoMatch} />
                    </Switch>
                </BrowserRouter>
            </React.Fragment>
        );
    }
}

export default App;
