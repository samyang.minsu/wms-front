import React from 'react';
import 'components/template/search_style.css';

const SearchTemplate = ({ searchItem }) => {
    return (
        <div className="search_wrap">
            <div className="search_line_wrap">                
                {searchItem}
            </div>
        </div>
    );
};

export default SearchTemplate;
