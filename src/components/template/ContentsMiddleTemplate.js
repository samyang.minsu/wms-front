import React, { Component } from 'react'
import 'components/template/middle_style.css'
import SButton from 'components/atoms/button/SButton';
import Alert from 'components/override/alert/Alert';

class ContentsMiddleTemplate extends Component {    
    
    constructor(props) {
        super(props);
        this.state = {
            hide: true
        };
    }

    expandToggle = async () => {
        let contentsWrapAll = document.getElementById(this.props.expandTargetId);        
        if (!this.state.hide) {
            contentsWrapAll.style.display='block';
            this.setState({ hide: true });
        } else {
            contentsWrapAll.style.display='none';
            this.setState({ hide: false });
        }
    } 

    render() {
        const { contentSubTitle, middleItem, expandTargetId } = this.props;
        return (
            <div className="middle_wrap">
                {contentSubTitle ? (
                    <div className="sub_tit">
                        {contentSubTitle}
                    </div>)
                    : (null)
                }                
                {expandTargetId ? (
                    <div className="toggle_warp" style = {{ float : "right"}}>
                        {/* <label className="item_lb w60">그리드</label> */}
                        <label className="toggle_label">
                            <input type="checkbox" defaultChecked={true} onClick={this.expandToggle} />
                            <span className="back">
                                <span className="toggle"></span>
                                <span className="label on">ON</span>
                                <span className="label off">OFF</span>
                                {/* <span className={this.state.className}>{this.state.label}</span> */}
                            </span>
                        </label>
                    </div>

                    // <div className="btn_wrap">
                    //     <SButton
                    //         buttonName={'그리드on/off'}
                    //         type={'default'}
                    //         onClick={this.expandToggle}
                    //     />
                    // </div>
                    )
                    : (null)
                }
                <ul className="btn_wrap">
                    {middleItem}
                </ul>
            </div>
        );
    }
}

export default ContentsMiddleTemplate;