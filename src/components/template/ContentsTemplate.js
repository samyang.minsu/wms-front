import React, { Component } from 'react'
import PropTypes from 'prop-types';
import $ from 'jquery';
import 'jquery-ui/ui/core';
import ReactDOM from 'react-dom';
import 'components/template/contents_style.css';

class ContentsTemplate extends Component {

    constructor(props) {
        super(props);

        this.state = {
            height: 'calc(100vh - 40px)',
            padding: '8px 10px',
        }

        this.UpdateContentsSize = this.UpdateContentsSize.bind(this);
    }

    componentDidMount() {
        this.UpdateContentsSize();
        window.addEventListener('resize', this.UpdateContentsSize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.UpdateContentsSize);
    }

    UpdateContentsSize() {

        const contentsWrapAll = document.getElementsByClassName('contents_wrap');
        const own = contentsWrapAll[this.props.id];

        // 자식중에 ContentsTemplate이 존재하면 padding 0px
        if (this.isExistContentsWrap(own)) {
            this.setState({ padding: '0px' });
        }

        // IE 오류 방지
        if (own.length <= 1 || own.length == undefined) {
            // // SModal에서는 무조건 padding 처리
            if (own.hasChildNodes() && own.childNodes[0].className == "pop_panel_body") {
                this.setState({ padding: '8px 10px' });
            }
        }

        const parent = own.parentNode;

        if (parent == null) {
            return;
        }

        const children = parent.childNodes;

        const parentTopPadding = parseInt($(parent).css("padding-top").replace('px', ''));
        const parentTop = this.getAbsoluteTop(parent);
        let contentsTop;

        for (let i = 0; i < children.length; i++) {

            if (children[i].id != undefined && children[i].id == this.props.id) {
                contentsTop = this.getAbsoluteTop(children[i]);
                break;
            }
        }
        contentsTop -= parentTopPadding;

        this.setState({ height: `calc(100% - ${contentsTop - parentTop}px)` });
    }

    isExistContentsWrap = (element) => {

        // 내 자식중에 ContentsTemplate이 존재하는지 확인
        if (element.hasChildNodes) {
            for (let i = 0; i < element.childNodes.length; i++) {
                const item = element.childNodes[i];

                if (item.getElementsByClassName('contents_wrap').length > 0) {
                    return true;
                } else {
                    if (item.hasChildNodes) {
                        if (this.isExistContentsWrap(item.childNodes)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    //요소의 절대좌표 구하기.
    getAbsoluteTop(element) {

        //// 출처: https://mommoo.tistory.com/85
        //// 부모요소의 시작지점을 기준으로한 상대좌표
        // const parentElement = element.parentElement;
        // const parentAbsoluteTop = this.getAbsoluteTop(parentElement);
        // const absoluteTop = this.getAbsoluteTop(element);
        // const relativeTop = absoluteTop - parentAbsoluteTop;
        // return absoluteTop;

        return window.pageYOffset + element.getBoundingClientRect().top;
    }

    render() {
        const { contentItem, height, id, overflowY, overflowX, minWidth, minHeight } = this.props;

        let bodyHeight;
        if (height != undefined) {
            bodyHeight = height;
        }
        else {
            bodyHeight = this.state.height;
        }

        return (
            <div id={id} className='contents_wrap'
                style={{
                    height: bodyHeight
                    , minWidth: `${minWidth}`
                    , minHeight: `${minHeight}`
                    , overflowY: `${overflowY}`
                    , overflowX: `${overflowX}`
                    , clear: 'both'
                    , overflow: 'visible'
                    , padding: this.state.padding
                }}>
                {contentItem}
            </div>
        );
    };
}

export default ContentsTemplate;

// Type 체크
ContentsTemplate.propTypes = {
    overflowY: PropTypes.string
}

// Default 값 설정
ContentsTemplate.defaultProps = {
    overflowY: 'auto',
    overflowX: 'auto',
    minWidth: '10px',
    minHeight: '10px'
}