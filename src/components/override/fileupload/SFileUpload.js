import React from 'react';
import PropTypes from 'prop-types';
import SIconButton from 'components/atoms/button/SIconButton'
import Alert from 'components/override/alert/Alert';
import FileRepository from 'utils/file/FileRepository';

export default class SFileUpload extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    async onChange(event) {
        const file = event.target.files[0];
        if (file) {
            let formData = new FormData();
            formData.append('file', file);

            const { data, status } = await FileRepository.fileUpload(formData);

            if (status == 200) {
                if (data.isSuccess) {
                    const param = { "id": this.props.id, "value": data.fileInfo.fileId };
                    this.props.onChange(param);
                } else {
                    Alert.meg(data.message);
                }
            } else {
                Alert.meg('업로드중 문제가 발생 하였습니다.');
            }
        } else {
            const param = { "id": this.props.id, "value": '' };
            this.props.onChange(param);
        }
    }

    render() {
        const { fileNm, onClick } = this.props;

        if (fileNm == '' || fileNm == '0') {
            return (
                <React.Fragment>
                    <input type="file" onChange={this.onChange} />
                </React.Fragment>
            );
        }
        else {
            return (
                <React.Fragment>
                    <span>{fileNm.split(',')[1]}</span>&nbsp;
                    <SIconButton type='delete' onClick={onClick} />
                </React.Fragment>
            );
        }

    }
}

// Type 체크
SFileUpload.propTypes = {
    id: PropTypes.string.isRequired,
    onChange: PropTypes.func,
}

// Default 값 설정
SFileUpload.defaultProps = {
}