import React, { Component } from 'react'
import { inject } from 'mobx-react';
import { observable } from 'mobx';
import PropTypes from 'prop-types';
import { SConsumer } from 'utils/context/SContext'
import { SModalConsumer } from 'utils/context/SModalContext';
import SSelectBox from 'components/atoms/selectbox/SSelectBox'

@inject("commonStore")
export default class SBuSelectBox extends Component {

    @observable buList = [];

    constructor(props) {
        super(props);
        this.state = {
            optionGroup: [],
        }
    }

    getBuAuth = (context) => {

        const { commonStore, id, value, onChange, isAddAll } = this.props;
        const buAuth = context.split(",");

        const list = [];

        buAuth.map(item => {
            const x = commonStore.buList.find(x => x.buId == item);
            if (x) {
                list.push(x);
            }
        });

        if (isAddAll && buAuth.length > 1) {
            list.unshift({ buId: context, buNm: "전체" });
        }

        this.buList = list;

        // if (value == "" && buAuth.length > 1) {
        //     if (onChange) {
        //         onChange({ id: id, value: context });
        //     }
        // }
    }

    render() {

        const { title, id, value, titleWidth, contentWidth, marginLeft, marginRight, onChange, onSelectionChange, disabled } = this.props;

        return (

            <SConsumer>
                {
                    (context) => {
                        if (this.buList.length <= 0) {
                            this.getBuAuth('1361,13GA');
                        }

                        return (
                            <SSelectBox
                                title={title}
                                id={id}
                                value={value}
                                title={title}
                                valueMember={'buId'}
                                displayMember={'buNm'}
                                titleWidth={titleWidth}
                                contentWidth={contentWidth}
                                marginLeft={marginLeft}
                                marginRight={marginRight}
                                disabled={disabled}
                                onChange={onChange}
                                onSelectionChange={onSelectionChange}
                                optionGroup={this.buList}
                            />
                        )
                    }
                }
            </SConsumer>
        )
    }
}

SBuSelectBox.propTypes = {
    title: PropTypes.string,
    id: PropTypes.string,
    // valueMember: PropTypes.string,
    // displayMember: PropTypes.string,
    // value: PropTypes.oneOfType([
    //     PropTypes.string,
    //     PropTypes.number
    // ]),
    titleWidth: PropTypes.number,
    contentWidth: PropTypes.number,
    onChange: PropTypes.func,
    codeGroup: PropTypes.string,
    isAddAll: PropTypes.bool,
}

SBuSelectBox.defaultProps = {
    valueMember: 'cd', // value property
    displayMember: 'cdNm', // display text property
    optionGroup: [],

    titleWidth: 90,
    contentWidth: 110,

    marginLeft: 0,
    marginRight: 0,
    isAddAll: true,
    disabled: false,
}