import React, { Component } from 'react';
import Tree, { TreeNode } from 'rc-tree';
import PropTypes from 'prop-types';
import 'rc-tree/assets/index.less';
import 'components/override/tree/style.css';

export default class STree extends Component {

    constructor(props) {
        super(props);
    }

    handleSelect = (selectedKeys, info) => {
        const { onSelect } = this.props;

        if (onSelect) {
            onSelect(selectedKeys, info);
        }
    }

    handleCheck = (checkedKeys, info) => {
        const { onCheck } = this.props;

        if (onCheck) {
            onCheck(checkedKeys, info);
        }
    }

    makeTreeNode = (data) => {

        const { keyMember, pIdMember, titleMember } = this.props;

        let makeTree = [];

        for (let i = 0; i < data.length; i++) {
            const item = data[i];

            const target = { key: item[keyMember], title: item[titleMember] };
            this.makeChildren(data, target);

            /** 데이터의 root만 추가한다. 
             *  위치정보 관리에서는 부모자식이 INT 
             *  수정자: 박성현
             *  일시  : 2019.11.06*/ 
            if (item[pIdMember] == null || item[pIdMember] == 0) {
                makeTree = makeTree.concat(target);
            }
        }

        return makeTree;
    };

    makeChildren = (data, target) => {

        const { keyMember, pIdMember, titleMember } = this.props;

        // 나(target)를 부모로 가지고 있는 자식 가져오기
        const children = data.filter(x => x[pIdMember] == target.key);

        // 자식이 있으면
        if (children) {

            // children 초기값 설정
            target.children = [];

            for (let i = 0; i < children.length; i++) {
                const item = children[i];

                // 자식 object 생성
                const child = { key: item[keyMember], title: item[titleMember] };

                // 자식 key에도 자식이 있는지 확인하여 자식이 있으면 makeChildren 재귀호출
                if (this.isExistChildren(data, child.key)) {
                    this.makeChildren(data, child);
                }

                // 자식 설정 후 target에 concat
                target.children = target.children.concat(child);
            }
        }
    }

    isExistChildren = (data, pId) => {
        const { pIdMember } = this.props;
        // 자식이 있는지 확인 
        return data.some(x => x[pIdMember] == pId);
    }

    render() {

        const { treeData, checkable } = this.props;

        const treeNode = this.makeTreeNode(treeData);

        return (
            <div className="basic" style={{ overflowY: 'auto' }}>
                <Tree
                    treeData={treeNode}
                    checkable={checkable}
                    onCheck={this.handleCheck}
                    onSelect={this.handleSelect}
                />
            </div>
        )
    }
}

STree.propTypes = {
    keyMember: PropTypes.string,
    pIdMember: PropTypes.string,
    titleMember: PropTypes.string,
    checkable: PropTypes.bool,
    onSelect: PropTypes.func,
    onCheck: PropTypes.func,
}

STree.defaultProps = {
    treeData: [],
    keyMember: 'cd',
    pIdMember: 'pCd',
    titleMember: 'cdNm',
    checkable: false,
}