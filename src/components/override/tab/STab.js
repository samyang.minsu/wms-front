import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tabs, TabList, Tab, PanelList, Panel } from 'react-tabtab';
import 'components/override/tab/style.css'
import ReactDOM from "react-dom";
import styled from 'styled-components'
import { styled as tabtabStyle } from 'react-tabtab';
let { PanelStyle } = tabtabStyle;

export default class STab extends Component {

    constructor(props) {
        super(props);

        this.state = {
            template: { header: [], content: [] },
            styled: undefined,
        }
    }

    componentDidMount() {

        // 1) PanelTemplate의 Height 정의
        const getHeight = () => {
            const me = ReactDOM.findDOMNode(this);
            // return `${me.offsetHeight - 42}px !important`
            return `${this.props.parentHeight - 92}px !important`
        }

        const CustomPanelStyle = styled(PanelStyle)`
            height: ${getHeight()};
            padding: 0px;
        `;

        const customStyle = {
            Panel: CustomPanelStyle
        }

        this.setState({ styled: customStyle })

        // 2) children 정의
        const { children } = this.props;

        if (!children) {
            return;
        }

        const tabTemplate = [];
        const panelTemplate = [];

        if (!children.length) {
            // children이 하나일 때
            tabTemplate.push(<Tab key={`business-tab-${0}`}>{children.props.header}</Tab>);
            panelTemplate.push(<Panel key={`business-panel-${0}`}>{children}</Panel>);

        } else {
            // children이 여러개일 때
            for (let i = 0; i < children.length; i++) {
                const item = children[i];
                tabTemplate.push(<Tab key={`business-tab-${i}`}>{item.props.header}</Tab>);
                panelTemplate.push(<Panel key={`business-panel-${i}`}>{item}</Panel>);
            }
        }

        this.setState(state => ({
            template: {
                header: state.template.header.concat(tabTemplate)
                , content: state.template.content.concat(panelTemplate)
            }
        }));
    }

    handleTabChange = (index) => {

        const { id, onChange, onTabChange } = this.props;
        const data = { id: id, value: index };
        onChange(data);

        if (onTabChange) {
            onTabChange(index);
        }
    }

    render() {

        const { activeIndex } = this.props;
        const { template } = this.state;

        let index = 0;

        // 최초 로딩 후 header.length가 0 미만일 때 activeIndex 높으면 오류.
        if (template.header.length > 0) {
            index = activeIndex;
        }

        return (
            <div className='basic'>
                <Tabs onTabChange={this.handleTabChange} activeIndex={index} customStyle={this.state.styled}>
                    <TabList>
                        {template.header}
                    </TabList>
                    <PanelList >
                        {template.content}
                    </PanelList>
                </Tabs>
            </div>
        )
    }
}

STab.propTypes = {
    activeIndex: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onTabChange: PropTypes.func,
    parentHeight: PropTypes.number,
}

STab.defaultProps = {
    activeIndex: 0,
    parentHeight: 800,
}