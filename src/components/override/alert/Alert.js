
import Swal from 'sweetalert2'
import 'components/override/alert/style.css'

class Alert {

    meg(message) {
        Swal.fire({
            html: `<font size="3">${message}</font>`
            , animation: false
        })
    }

    async confirm(message) {
        const { value } = await Swal.fire({
            html: `<font size="3">${message}</font>`
            , showCancelButton: true
            , cancelButtonColor: '#f1547f'
            , animation: false
        }).then((result) => {
            return result;
        });
        return value;
    }

    info() {
        console.log('error');
    }

    error() {
        console.log('error');
    }

    success() {
        console.log('success');
    }

    warning() {
        console.log('warning');
    }

}
export default new Alert();