
import React, {Component} from 'react'

import ico_day from 'style/img/ico_day.png';
import PropTypes from 'prop-types';
import 'components/override/datepicker/style.css'

export default class DatePickerIcon extends Component {

    render() {
        return (
            <div className="day_select"  onClick={this.props.onClick}>
                <img src={ico_day} alt="" ></img>
            </div>
        );
    }
}

// Type 체크
DatePickerIcon.propTypes = {
    onClick: PropTypes.func,
    value: PropTypes.string
};

// Default 값 설정
DatePickerIcon.defaultProps = {
}
