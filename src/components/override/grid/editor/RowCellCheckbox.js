import React, { Component } from "react";

export default class RowCellCheckbox extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.refs.inputcheckbox.addEventListener('click', this.handleChange);
        this.refs.inputcheckbox.checked = this.props.value == "Y" ? true : false;
    }

    componentWillUnmount() {
        this.refs.inputcheckbox.removeEventListener('click', this.handleChange);
    }

    handleChange(event) {
        // console.log('rowcellcheckbox', event, event.target.id);
        eval(`this.props.data.${event.target.id}=event.target.checked == true ? "Y" : "N"`);
        // console.log(this.props)
        this.props.data.edit = true;

        const { onChange } = this.props;
        if (onChange) {
            const target = this.props.data;
            const value = event.target.checked == true ? "Y" : "N";

            onChange(target, value);
        }
    }

    render() {
        const field = this.props.colDef.field;
        return (
            <input type='checkbox' ref="inputcheckbox" id={field} readOnly={true}/>
        );
    }
}