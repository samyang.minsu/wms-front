import React, { Component } from "react";

export default class RowCellSelectBox extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value
            , values: this.props.values
        }

        console.log(`select : ${this.props.value}`);
        console.log(`select : ${this.values}`);

        this.handleChange = this.handleChange.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
    }


    // props 변화 시 첫번째 실행
    componentWillReceiveProps() {
        //this.setState({cd: this.props.defaultValue}); 
        console.log('componentWillReceiveProps');
    }
    afterGuiAttached() {
        const eSelect = this.refs.select;
        eSelect.focus();
    }

    componentDidMount() {
        this.refs.select.addEventListener('keydown', this.onKeyDown);
    }

    componentWillUnmount() {
        this.refs.select.removeEventListener('keydown', this.onKeyDown);
    }

    getValue() {
        return this.state.value;
    }

    handleChange(event) {
        console.log(event.target.value);
        this.setState({ value: event.target.value });
    }


    onKeyDown(event) {
    }


    render() {
        const selectOption =
            (this.state.values && this.state.values.map.length > 0) && this.state.values.map((select) =>
                <option key={select.cd} value={select.cd}>{select.cdNm}</option>
            );
        return (
            <li className="ag-cell-select" >
                <select ref="select" value={this.state.value} onChange={this.handleChange}>
                    {selectOption}
                </select>
            </li>
        );
    }
};