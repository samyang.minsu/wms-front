import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { observable } from 'mobx'
import SearchTemplate from 'components/template/SearchTemplate'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import SGrid from 'components/override/grid/SGrid'
import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import SInput from 'components/atoms/input/SInput'
import SDatePicker from 'components/override/datepicker/SDatePicker'
import SNumericInput from 'components/override/numericinput/SNumericInput'
import SButton from 'components/atoms/button/SButton'
import HelperRepository from 'components/codehelper/service/HelperRepository'
import SModal from 'components/override/modal/SModal'
import SBuSelectBox from 'components/override/business/SBuSelectBox';
import CommonStore from 'modules/pages/common/service/CommonStore';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

export default class EmployeeMultiModalHelper extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        }
    }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (nextProps.isEmpOpen != prevState.isModalOpen) {
            return { isModalOpen: nextProps.isEmpOpen }
        }
        return null;
    }

    handleClose = (result) => {
        const { onHelperResult, id, handleChange } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }

        if (handleChange) {
            handleChange({ id: id, value: false })
        }
    }

    render() {

        return (
            <SModal
                isOpen={this.state.isModalOpen}
                onRequestClose={() => this.handleClose(null)}
                contentLabel={this.props.title}
                width={1000}
                contents={<EmployeeHelper {...this.props} onHelperResult={this.handleClose} />}
            />
        )
    }
}

EmployeeMultiModalHelper.propTypes = {
    onHelperResult: PropTypes.func,
    title: PropTypes.string,
    isEmpOpen: PropTypes.bool.isRequired,
}

EmployeeMultiModalHelper.defaultProps = {
    title: "사용자 추가",
}

@observer
class EmployeeHelper extends Component {

    @observable employeeList = [];

    gridApi = undefined;

    @observable buId = CommonStore.buId;
    @observable buList = CommonStore.buList
    @observable employee = '';

    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        await this.handleSearch();
    }

    handleChange = (data) => {
        this[data.id] = data.value;
    }

    handleSearch = async () => {

        const params = {
            buId: this.buId,
            empNo: this.employee,
            useYN: 'Y',
        };

        const { data, status } = await HelperRepository.getEmployee(params);

        if (data.success) {

            const { selectedEmployeeList } = this.props;

            if (selectedEmployeeList) {
                const tempList = [];

                data.data.forEach(item => {
                    if (!selectedEmployeeList.some(x => x.empNo == item.empNo)) {
                        tempList.push(item);
                    }
                });

                this.employeeList = tempList;
            } else {
                this.employeeList = data.data;
            }

            this.gridApi.setRowData(this.employeeList);
        }
    }

    handleSelect = () => {

        const selectedRows = this.gridApi.getSelectedRows();
        if (selectedRows.length <= 0) {
            Alert.meg("선택할 사용자를 선택해주세요.");
            return;
        }

        console.log(selectedRows)

        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(selectedRows);
        }
    }

    setGridApi = (gridApi) => {
        this.gridApi = gridApi;

        const columnDefs = [
            {
                headerName: ""
                , field: "check"
                , width: 29
                , checkboxSelection: true
                , headerCheckboxSelection: true
            },
            { headerName: "로그인ID", field: "loginId", width: 100, cellClass: 'cell_align_center' },
            { headerName: "사번", field: "empNo", width: 90, cellClass: 'cell_align_center' },
            { headerName: "이름", field: "loginNm", width: 140, cellClass: 'cell_align_center' },
            { headerName: "부서", field: "deptNm", width: 100 },
            { headerName: "이메일", field: "loginEmail", width: 200 },
            { headerName: "전화번호", field: "loginTel", width: 120 },
            { headerName: "권한그룹", field: "grpList", width: 120 } //권한그룹 추가 *수정자: 박성현    일시:2019.10.30
            
        ];

        this.gridApi.setColumnDefs(columnDefs);
    }

    render() {

        const { } = this.props;
        return (
            <React.Fragment>
                <div className='basic'>

                    <SearchTemplate searchItem={
                        <React.Fragment>
                            
                            <SSelectBox // 공장이 없는 사용자도 있기때문에 권한 없이 드롭다운 *수정자: 박성현  일시:2019.10.23
                                title={"공장"}
                                id={"buId"}
                                value={this.buId}
                                onChange={this.handleChange}
                                optionGroup={this.buList}
                                valueMember={"buId"}
                                displayMember={"buNm"}
                                addOption={"전체"}
                            />
                            {/* <SBuSelectBox
                                title={"공장"}
                                id={"buId"}
                                value={this.buId}
                                onChange={this.handleChange}
                            /> */}
                            <SInput
                                title={"사용자"}
                                id={"employee"}
                                value={this.employee}
                                onChange={this.handleChange}
                                onEnterKeyDown={this.handleSearch}
                                isFocus={true}
                            />

                            <div className='search_item_btn'>
                                <SButton
                                    buttonName={"조회"}
                                    type={"default"}
                                    onClick={this.handleSearch}
                                />
                            </div>

                        </React.Fragment>
                    } />

                    <ContentsMiddleTemplate middleItem={
                        <React.Fragment>
                            <SButton
                                buttonName={"선택"}
                                type={"default"}
                                onClick={this.handleSelect}
                            />
                            <SButton
                                buttonName={"취소"}
                                type={"default"}
                                onClick={() => this.props.onHelperResult(null)}
                            />
                        </React.Fragment>
                    } />

                    <ContentsTemplate id="employeeMultiHelper" contentItem={
                        <SGrid
                            grid={"employeeHelperGrid"}
                            gridApiCallBack={this.setGridApi}
                            rowData={this.employeeList}
                            suppressRowClickSelection={true}
                            editable={false}
                        />
                    } />

                </div>
            </React.Fragment>
        )
    }
}