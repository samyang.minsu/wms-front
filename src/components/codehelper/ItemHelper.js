import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer, inject } from 'mobx-react';
import { observable } from 'mobx'

import SInput from 'components/atoms/input/SInput'
import SSelectBox from 'components/atoms/selectbox/SSelectBox'
import CodeHelper from 'components/codehelper/HoC/CodeHelper'
import ItemRepository from 'modules/pages/system/item/service/ItemRepository';
import SearchTemplate from 'components/template/SearchTemplate'
import GridUtil from 'components/override/grid/utils/GridUtil'
import HelperRepository from 'components/codehelper/service/HelperRepository';
import CommonStore from 'modules/pages/common/service/CommonStore';
import STreeSelectBox from 'components/override/tree/STreeSelectBox';
import SBuSelectBox from 'components/override/business/SBuSelectBox';

@observer
export default class ItemHelper extends Component {

    @observable helperOption = observable({});

    // CodeHelper에 전달할 조회 내역 List
    @observable itemList = [];

    // // CodeHelper-Modal에 표시할 Grid ColumnDefs
    columnDefs = [
        {
            headerName: "제품타입"
            , field: "glclass"
            , width: 65
            , cellClass: "cell_align_center"
            , valueFormatter: (params) => GridUtil.getCodeNm(params, "MT002")
        },
        { headerName: "제품코드", field: "itemCd", width: 85, cellClass: "cell_align_center" },
        { headerName: "제품명", field: "itemNm", width: 230 },
        {
            headerName: "제품단위"
            , field: "itemUom"
            , width: 65
            , cellClass: "cell_align_center"
        },
        { headerName: "SearchText", field: "searchText", width: 150 },
        { headerName: "규격", field: "spec", width: 150 },
        {
            headerName: "유통기한"
            , field: "expiryDay"
            , width: 65
            , type: "numericColumn"
            , valueFormatter: GridUtil.numberFormatter
        },
        { headerName: "재고관리여부", field: "invenYn", width: 85, cellClass: "cell_align_center" },
        { headerName: "바코드관리여부", field: "barcodeYn", width: 95, cellClass: "cell_align_center" },
    ];

    constructor(props) {
        super(props);

        this.state = {
            helperOption: {},
        }
    }

    componentDidMount() {
        this.setData();
    }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (JSON.stringify(nextProps.helperOption) != JSON.stringify(prevState.helperOption)) {
            return { helperOption: JSON.parse(JSON.stringify(nextProps.helperOption)) }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.helperOption !== this.state.helperOption) {
            this.setData();
        }
    }

    setData = () => {
        this.helperOption = this.state.helperOption;
    }

    handleHelperChange = (data) => {
        this.helperOption[data.id] = data.value;
    }

    // CodeHelper에 전달할 Search 함수
    handleSearch = async () => {

        const { glclass, item, buId, barcodeYn } = this.helperOption;

        const params = {
            item: item,
            glclass: glclass,
            buId: buId,
            barcodeYn: barcodeYn,
        };

        console.log(params, 'params 파라미터')

        const { data, status } = await HelperRepository.getItem(params);

        if (status == 200) {
            this.itemList = data.data;
        }
    }
    render() {

        const { mode, buId, buIdReadOnly, glclass, item, modalTitle } = this.helperOption;

        return (
            <React.Fragment>

                <CodeHelper
                    {...this.props}
                    columnDefs={this.columnDefs}
                    onSearch={this.handleSearch}
                    onHelperChange={this.handleHelperChange}
                    onOpening={this.setData}
                    itemList={this.itemList}
                    helperCdNmValue={"item"}
                    cdName={"itemCd"}
                    cdNmName={"itemNm"}
                    modalTitle={modalTitle}
                    searchItem={
                        <SearchItem
                            mode={mode}
                            onSearch={this.handleSearch}
                            buId={buId}
                            buIdReadOnly={buIdReadOnly}
                            glclass={glclass}
                            item={item}
                            onHelperChange={this.handleHelperChange}
                        />
                    }
                />
            </React.Fragment>
        )
    }
}

// CodeHelper에서의 조회 조건 Component
class SearchItem extends Component {

    render() {

        const { mode, buId, buIdReadOnly, glclass, item, onHelperChange, onSearch } = this.props;
        return (
            <SearchTemplate searchItem={
                <React.Fragment>
                    <SBuSelectBox
                        title={"공장"}
                        id={"buId"}
                        value={buId}
                        onChange={onHelperChange}
                        disabled={buIdReadOnly}
                    />

                    {mode != "fix" ?
                        <STreeSelectBox
                            title={"제품구분"}
                            id={"glclass"}
                            contentWidth={200}
                            value={glclass}
                            onChange={onHelperChange}
                            codeGroup={"MT002"}
                        />
                        :
                        null
                    }

                    <SInput
                        title={"제품"}
                        id={"item"}
                        value={item}
                        onChange={onHelperChange}
                        onEnterKeyDown={onSearch}
                        isFocus={true}
                        contentWidth={130}
                    />
                </React.Fragment>
            } />
        )
    }
}