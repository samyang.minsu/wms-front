import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { observable } from 'mobx'
import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'
import SSelectBox from 'components/atoms/selectbox/SSelectBox'
import SList from "components/atoms/list/SList";
import SearchTemplate from 'components/template/SearchTemplate'
import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate';
import ContentsTemplate from 'components/template/ContentsTemplate';
import CommonStore from 'modules/pages/common/service/CommonStore'
import SModal from 'components/override/modal/SModal'
import Alert from 'components/override/alert/Alert';
import LocationRepository from 'modules/pages/system/location/service/LocationRepository'

class LocationModalHelper extends Component {

    @observable isModalOpen = false;

    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false
        }
    }

    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (nextProps.isLocationOpen != prevState.isModalOpen) {
            return { isModalOpen: nextProps.isLocationOpen }
        }
        return null;
    }

    handleClose = (result) => {
        const { onHelperResult, id, onChange } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }

        if (onChange) {
            onChange({ id: id, value: false })
        }
    }

    render() {

        return (
            <SModal
                id={"LocationHelper"}
                isOpen={this.state.isModalOpen}
                onRequestClose={() => this.handleClose(null)}
                contentLabel={this.props.title}
                width={800}
                height={600}
                contents={<LocationHelper {...this.props} onHelperResult={this.handleClose} />}
            />
        )
    }
}

LocationModalHelper.propTypes = {
    onHelperResult: PropTypes.func,
    title: PropTypes.string,
}

LocationModalHelper.defaultProps = {
    title: "위치조정",
    buReadOnly: false,
}

export default LocationModalHelper;

@observer
class LocationHelper extends Component {

    locationData = []; //data
    @observable locationList = observable([]); //set

    @observable buId = '';
    @observable level = '';
    @observable locationNm = '';
    @observable selectedLineText = '';
    buGroup = CommonStore.buList;

    constructor(props) {
        super(props);
    }

    async componentDidMount() {

        const { buId, level, locationNm, locId } = this.props;

        this.buId = buId;
        this.level = level;
        this.locationNm = locationNm;

        await this.getLocation();

        if (locId) {
            this.setLocation(locId);
        }
    }

    handleChange = (data) => {
        this[data.id] = data.value;
    }

    handleSelectClick = () => {
        const { onHelperResult } = this.props;

        const result = this.getSelectedItem();

        if (!result) {
            Alert.meg("위치를 선택해주세요.");
            return;
        }

        if (onHelperResult) {
            onHelperResult(result);
        }
    }

    handleItemClick = (item) => {
        console.log('item', item);
        this.selectedLineText = item.locNmPath;
        
        if (item.level != 0) {
            // 선택한 항목의 자식이 있는지 확인
            const nextList = this.filtering(this.locationList, 'level', `level${item.level + 1}`);

            if (nextList) {
                // 선택한 항목의 자식이 내 자식이면 return
                if (nextList.parentLocId == item.locId) {
                    return;
                }
            }

            let index = item.level;
            let size = this.locationList.length - 1;
            // 선택한 항목 제외한 나머지 삭제
            if (index != size) {
                this.locationList.splice(index + 1, size);
            }
        }
        
        let child = this.locationData.filter(x => x.plocId == item.locId); // 자식찾기
        // 자식이 없다면 아무것도 하지 않음
        if (!child || child.length <= 0) {
            return;
        }

        // 이미 해당 레벨이 있으면 return
        console.log(child[0].level, this.locationList.length)
        if (child[0].level < this.locationList.length) {
            return;
        }

        this.locationList = observable(this.locationList.concat({ level: `level${child[0].level}`, contents: child, parentLocId: item.locId, header: `LEVEL ${child[0].level}`, isSelected: false }));
    }

    getLocation = async () => {
        this.locationList = observable([]);

        const searchData = {
            "buId": this.buId,
        }

        const { data, status } = await LocationRepository.getLocation(searchData);
        if (status == 200) {
            this.locationData = data.data;
            const locLevel0 = this.locationData.filter(x => x.level == 0);
            this.locationList = observable(this.locationList.concat({ level: 'level0', contents: locLevel0, parentLocId: locLevel0[0].plocId, header: 'LEVEL 0', isSelected: false }));
        }
    }

    getSelectedItem = () => {
        for (let i = (this.locationList.length - 1); i >= 0; i--) {
            const items = this.locationList[i].contents;

            const selectedItem = this.filtering(items, 'isSelected', true);

            if (selectedItem) {
                return selectedItem;
            }
        }
        return null;
    }

    groupBy = (xs, key) => {
        return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    };

    handleSearch = () => {

        const findNm = this.filtering(this.locationData, 'locNm', this.locationNm);
        if (findNm) {
            this.setLocation(findNm.locId);
        } else {
            Alert.meg('조회 결과가 없습니다.');
        }
    }

    setLocation = (locId) => {

        // 조회할 locId의 정보
        const targetItem = this.filtering(this.locationData, 'locId', locId)

        // 전체 데이터 레벨 기준으로 GroupBy
        const levelGroup = this.groupBy(this.locationData, 'level');

        // 조회한 값을 마지막에 Binding하기 위한 임시 변수
        const tempList = [];

        // 나와 같은 부모를 가진 자식을 찾기 위한 locId의 부모Id
        let parentLocId = targetItem.plocId;

        // 현재 레벨의 갯수
        const levelCount = Object.keys(levelGroup).length;

        // 레벨 역순으로 구성
        for (let i = levelCount - 1; i >= 0; i--) {
            const contents = levelGroup[i];

            // 나와 같은 부모를 가진 자식을 찾는다.
            const sameLevel = contents.filter(x => x.plocId == parentLocId);
            targetItem.isSelected = true;

            // 임시 변수에 push
            if (sameLevel.length > 0) {
                tempList.push({ level: `level${i}`, contents: sameLevel, parentLocId: parentLocId, header: `LEVEL ${i}` });
            }

            if (i != 0) {

                // 현재 내 레벨-1 중의 내 부모를 찾는다.
                levelGroup[i - 1].filter(x => x.locId == parentLocId);

                // 내 부모의 부모를 parentLocId 변수에 저장                
                const parentContents = this.filtering(levelGroup[i - 1], 'locId', parentLocId);
                if (parentContents) {
                    parentContents.isSelected = true;
                    parentLocId = parentContents.plocId;
                }
            }
        }

        // 조회한 값을 역순으로 Binding
        this.locationList = observable(tempList.reverse());
    }

    filtering = (targetList, fieldName, value) => {
        const result = targetList.filter(x => x[fieldName] == value);

        if (result.length > 0) {
            return result[0]
        } else {
            return null;
        }
    }

    render() {
        return (
            <React.Fragment>                
                <SearchTemplate
                    searchItem={
                        <SearchItem
                            buId={this.buId}
                            buGroup={this.buGroup}
                            buReadOnly={this.props.buReadOnly}
                            level={this.level}
                            locationNm={this.locationNm}
                            handleChange={this.handleChange}
                            handleSearch={this.handleSearch}
                        />}
                />
                <ContentsMiddleTemplate contentSubTitle={'위치정보'}
                    middleItem={
                        <MiddleItem
                            onClick={this.handleSelectClick}
                            selectedLineText={this.selectedLineText}
                            handleChange={this.handleChange}
                        />}
                />
                <ContentsTemplate id='location'
                    contentItem={
                        <ContentItem
                            locationList={this.locationList}
                            handleItemClick={this.handleItemClick}
                        />}
                />
            </React.Fragment>
        )
    }
}

LocationHelper.propTypes = {
    buId: PropTypes.string,
    level: PropTypes.string,
    locationNm: PropTypes.string,
    locId: PropTypes.string,
}

LocationHelper.defaultProps = {
    buId: CommonStore.buId,
    level: '',
    locationNm: '',
}

@observer
class ContentItem extends Component {

    render() {

        const { locationList, handleItemClick } = this.props;

        const list = (
            locationList.map((item) => {
                return (
                    <SList
                        key={`loc-${item.level}`}
                        id={`loc-${item.level}`}
                        header={item.header}
                        contents={item.contents}
                        onClick={handleItemClick}
                        displayCd={'locNm'}
                        width={"750px"}
                        height={"500px"}
                    />
                )
            }));
        return (
            <React.Fragment>
                {list}
            </React.Fragment>
        )
    }

}

// const ContentItem = ({ locationList, handleItemClick }) => {

//     const list = (
//         locationList.map((item) => {
//             return (
//                 <SList
//                     key={`loc-${item.level}`}
//                     id={`loc-${item.level}`}
//                     header={item.header}
//                     contents={item.contents}
//                     onClick={handleItemClick}
//                     displayCd={'locNm'}
//                     width={"750px"}
//                     height={"500px"}
//                 />
//             )
//         }));

//     return (
//         <React.Fragment>
//             {list}
//         </React.Fragment>
//     )
// }

const SearchItem = ({ buId, buGroup, buReadOnly, level, locationNm, handleChange, handleSearch }) => {
    return (
        <React.Fragment>
            <SSelectBox
                title={'공장'}
                id={'buId'}
                value={buId}
                onChange={handleChange}
                optionGroup={buGroup}
                disabled={buReadOnly}
                valueMember={'buId'}
                displayMember={'buNm'}
            />
            <SSelectBox
                title={'레벨'}
                id={'level'}
                value={level}
                onChange={handleChange}
                codeGroup={'MT002'}
            />
            <SInput
                title={'위치명'}
                id={'locationNm'}
                value={locationNm}
                onChange={handleChange}
                onEnterKeyDown={handleSearch}
            />

            <div className='search_item_btn'>
                <SButton
                    buttonName={'조회'}
                    type={'default'}
                    onClick={handleSearch} />
            </div>
        </React.Fragment>
    )
}

const MiddleItem = ({ onClick, selectedLineText, handleChange }) => {
    return (
        <React.Fragment>
            <div className="msdisplay wkdisplay">
                <SInput
                    title={"선택한 위치"}
                    id={"selectedLineText"}
                    value={selectedLineText}
                    onChange={handleChange}
                    contentWidth={200}
                />
            </div>
            <div style={{ display: "inline", float: "right" }}>
                <SButton
                    type={'default'}
                    buttonName={'선택'}
                    onClick={onClick}
                />
            </div>
        </React.Fragment>
    )
}
