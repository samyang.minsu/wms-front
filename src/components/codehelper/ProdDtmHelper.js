import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { observer } from 'mobx-react';
import { observable } from 'mobx'
import moment from 'moment';
import 'moment/locale/ko';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import SNumericInput from 'components/override/numericinput/SNumericInput';
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';
import GridUtil from 'components/override/grid/utils/GridUtil';
import SModal from 'components/override/modal/SModal';
import SDatePicker from 'components/override/datepicker/SDatePicker';
import Alert from 'components/override/alert/Alert';

import EtcRepository from 'modules/pages/inout/etc/service/EtcRepository';
import HelperRepository from 'components/codehelper/service/HelperRepository';

@observer
export default class ProdDtmHelperModal extends Component {

    @observable helperOption = observable({});

    constructor(props) {
        super(props);
        this.state = {
            helperOption: {},
            prodDtmHelperModalIsOpen: false
        }
    }

    componentDidMount() {
        this.setData();
    }

    // Onload 정보
    static getDerivedStateFromProps = (nextProps, prevState) => {
        if (JSON.stringify(nextProps.helperOption) != JSON.stringify(prevState.helperOption)) {
            return { helperOption: JSON.parse(JSON.stringify(nextProps.helperOption)) }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.helperOption !== this.state.helperOption) {
            this.setData();
        }
    }

    handleHelperChange = (data) => {
        this.helperOption[data.id] = data.value;
    }

    // prodDtmHelperModalIsOpen ture or false 처리
    handleClose = (result) => {
        const { onHelperResult, id, handleChange } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }

        if (handleChange) {
            handleChange({ id: id, value: false })
        }
    }

    render() {
        return (
            <SModal
                formId={"output_prodDtm_add_p"}
                isOpen={this.state.prodDtmHelperModalIsOpen}
                onRequestClose={this.handleClose}
                contentLabel={this.props.title}
                width={400}
                height={350}
                minWidth={400}
                minHeight={350}
                contents={<ProdDtmHelper {...this.props} onHelperResult={this.handleClose} />}
            />
        )
    }
}

BarcodeAddHelper.propTypes = {
    onHelperResult: PropTypes.func,
    title: PropTypes.string,
    // barcodeHelplerOrderNo: PropTypes.number,
    // barcodeHelplerledgerType: PropTypes.string,
    isProdDtmOpen: PropTypes.bool.isRequired,
}

BarcodeAddHelper.defaultProps = {
    title: "생산시간 입력",
}

@observer
class ProdDtmHelper extends Component {

    // Barcode List를 불러오기 위한 변수	
    @observable barcode = '';
    @observable prodDt = moment().format('YYYYMMDD');
    @observable itemNmOrItemCd = '';
    @observable barcodeList = [];
    @observable barcodeGridApi = undefined;

    barcodeHelplerOrderNo = 0;
    barcodeHelplerledgerType = '';

    constructor(props) {
        super(props);
    }

    handleChange = (data) => {
        this[data.id] = data.value;
    }

    handleSave = async () => {
        const { barcodeHelplerOrderNo, barcodeHelplerledgerType } = this.props;
        const selectedRows = this.barcodeGridApi.getSelectedRows();
        if (selectedRows.length <= 0) {
            Alert.meg("저장할 바코드를 선택해주세요.");
            return;
        }
        // 저장 로직
        let result = '';
        const addData = {
            orderNo: barcodeHelplerOrderNo
            , ledgerType: barcodeHelplerledgerType
        }

        const params = JSON.stringify({ param1: selectedRows, param2: addData });
        const { data, status } = await EtcRepository.saveBarcode(params);
        if (data.success == true) {
            result = true;
        } else {
            Alert.meg("저장에 실패했습니다.");
        }
        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(result);
        }
    }

    handleClose = () => {
        const { onHelperResult } = this.props;

        if (onHelperResult) {
            onHelperResult(null);
        }
    }

    render() {

        // const { prodDtModalOrderNo, handleChange
        //     , prodDtModalLineNm
        //     , prodDtModalWoStatusNm
        //     , prodDtModalItemCd
        //     , prodDtModalItemNm
        //     , prodDtModalProdStartDt, prodDtModalProdStartDtm
        //     , prodDtModalProdEndDt, prodDtModalProdEndDtm
        //     , closeProdDtModal
        //     , prodDtModalSave
        // } = this.props;

        return (
            <React.Fragment>
                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <th>오더번호</th>
                            <td>
                                <SNumericInput
                                    id="prodDtModalOrderNo"
                                    value={prodDtModalOrderNo}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>라인명</th>
                            <td>
                                <SInput
                                    id="prodDtModalLineNm"
                                    value={prodDtModalLineNm}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>상태</th>
                            <td>
                                <SInput
                                    id="prodDtModalWoStatusNm"
                                    value={prodDtModalWoStatusNm}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>제품코드</th>
                            <td>
                                <SInput
                                    id="prodDtModalItemCd"
                                    value={prodDtModalItemCd}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>제품명</th>
                            <td>
                                <SInput
                                    id="prodDtModalItemNm"
                                    value={prodDtModalItemNm}
                                    onChange={handleChange}
                                    readOnly={true}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>생산 시작시간</th>
                            <td>
                                <SDatePicker
                                    id="prodDtModalProdStartDt"
                                    value={prodDtModalProdStartDt}
                                    onChange={handleChange}
                                />
                                <STimePicker
                                    id="prodDtModalProdStartDtm"
                                    value={prodDtModalProdStartDtm}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <th>생산 종료시간</th>
                            <td>
                                <SDatePicker
                                    id="prodDtModalProdEndDt"
                                    value={prodDtModalProdEndDt}
                                    onChange={handleChange}
                                />
                                <STimePicker
                                    id="prodDtModalProdEndDtm"
                                    value={prodDtModalProdEndDtm}
                                    onChange={handleChange}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div>
                    <SButton buttonName={"취소"} onClick={closeProdDtModal} type={"default"} />
                    <SButton buttonName={"저장"} onClick={prodDtModalSave} type={"btnProdDtmSave"} />
                </div>
            </React.Fragment>
        );
    }
}