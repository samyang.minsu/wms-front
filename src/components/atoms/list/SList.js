import React, { Component } from 'react'
import PropTypes from 'prop-types';
import ContentsTemplate from 'components/template/ContentsTemplate'
import 'components/atoms/list/style.css';

export default class SList extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { id, header, headerStyle } = this.props;

        return (
            <div className="loc-level">
                <div className="tit" style={headerStyle}>{header}</div>
                <Contents id={id} {...this.props} />
            </div>
        )
    }
}

class Contents extends Component {

    onClick = (item, event) => {
        const { contents, onClick } = this.props;

        const beforeSelected = contents.findIndex(x => x.isSelected);

        // 기존항목 isSelected=false
        if (beforeSelected >= 0) {
            contents[beforeSelected].isSelected = false;
        }

        item.isSelected = true;

        if (onClick) {
            onClick(item, event);
        }
    }

    render() {

        const { contents, id, displayCd } = this.props;

        const control =
            contents.map((item, index) => {

                const { display, isSelected } = item;

                return (
                    <div
                        className="list"
                        key={`list-${id}-${index}`}
                        onClick={(event) => this.onClick(item, event)}
                        data-selected={isSelected}
                        >
                        {item[displayCd]}
                    </div>
                )
            });

        return (
            <React.Fragment>
                {control}
            </React.Fragment>
        )
    }
}
SList.propTypes = {
    id: PropTypes.string.isRequired,
    header: PropTypes.string,
    width: PropTypes.string,
    height: PropTypes.string,
    onClick: PropTypes.func,
    // contents: PropTypes.array,
    displayCd: PropTypes.string,
}

SList.defaultProps = {
    contents: [],
    width: '100px',
    height: '200px',
}
