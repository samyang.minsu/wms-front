
import React, { Component } from 'react'
import PropTypes from 'prop-types';
import 'components/atoms/input/style.css'

export default class STextArea extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { isFocus } = this.props;
        if (isFocus) {
            this.textArea.focus();
        }
    }

    handleChange = (event) => {
        const value = event.target.value;
        const data = { "id": event.target.id, "value": value };

        if (this.props.onChange) {
            this.props.onChange(data);
        }
    }

    render() {

        const { id, title, value, readOnly, contentWidth, titleWidth, contentHeight, resize } = this.props;

        return (
            <div className="input_item">
                {title ? (<label className="item_lb" style={{ width: `${titleWidth}px`, textAlign: 'right' }}>{title}</label>) : (null)}
                <textarea className="textarea"
                    id={id}
                    style={{ width: `${contentWidth}px`, height: `${contentHeight}px`, resize: resize }}
                    value={value}
                    onChange={this.handleChange}
                    readOnly={readOnly}
                    ref={(input) => { this.textArea = input; }}

                />
            </div>
        );
    }
}

STextArea.propTypes = {
    // cols: PropTypes.number.isRequired,
    // rows: PropTypes.number.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func,
    titleWidth: PropTypes.number,
    contentWidth: PropTypes.number,
    contentHeight: PropTypes.number,
    isFocus: PropTypes.bool,
}

STextArea.defaultProps = {
    // cols: 10,
    // rows: 10,
    titleWidth: 90,
    contentWidth: 110,
    contentHeight: 50,
    resize: 'none'
}
