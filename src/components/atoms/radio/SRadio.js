import React, { Component } from 'react'
import { inject } from 'mobx-react';
import PropTypes from 'prop-types';
import 'components/atoms/radio/style.css'

@inject("commonStore")
export default class SRadio extends Component {

    constructor(props) {
        super(props);
        this.state = {
            optionList: [],
            selectedOption: '',
        }
    }

    async componentDidMount() {

        const { codeGroup, optionGroup, value } = this.props;

        if (codeGroup) {
            await this.getCodeGroup(codeGroup);
        } else {
            this.setState({ optionList: optionGroup });
        }

        this.setState({ selectedOption: value });
    }

    getCodeGroup = async (codeGroup) => {

        const { commonStore } = this.props;

        await commonStore.getCodeObject().then(codeObject => {

            let option = codeObject[codeGroup].concat();
            this.setState({ optionList: option });
        });
    }

    async componentWillReceiveProps(nextProps) {

        if (nextProps.codeGroup) {
            if (this.props.codeGroup != nextProps.codeGroup) {
                await this.getCodeGroup(nextProps.codeGroup);
            }
        } else {
            if (JSON.stringify(this.state.optionList) != JSON.stringify(nextProps.optionGroup)) {
                this.setState({ optionList: nextProps.optionGroup });
            }
        }

        if (nextProps.value != this.state.selectedOption) {
            this.setState({ selectedOption: nextProps.value });
        }
    }

    handleChange = (event) => {
        this.setSelectedOption(event.target.value);
    }

    handleClick = (event) => {
        this.setSelectedOption(event.currentTarget.id);
    }

    setSelectedOption = (value) => {
        this.setState({ selectedOption: value });

        const { id, onChange, onSelectionChange } = this.props;
        const data = { "id": id, "value": value };
        onChange(data);

        if (onSelectionChange) {
            onSelectionChange(value);
        }
    }

    render() {
        const { id, valueMember, displayMember, title, contentWidth, titleWidth } = this.props;

        const { optionList } = this.state;

        const radioList =
            <div style={{ float: 'right', width: { contentWidth } }}>
                {(optionList && optionList.map.length > 0) && optionList.map(item => {
                    const optionValue = item[valueMember];
                    const optionDisplay = item[displayMember];

                    return (
                        <div className="check_rao" key={optionValue} style={{ float: 'right' }}>
                            <input type="radio"
                                id={id}
                                value={optionValue}
                                checked={this.state.selectedOption === optionValue}
                                onChange={this.handleChange}
                            />
                            <label id={optionValue} onClick={this.handleClick}>{optionDisplay}</label>
                        </div>
                    )
                }
                ).reverse()}
            </div>;
        return (
            <React.Fragment>
                <div className="search_item">
                    {title ? (<label className="item_lb" style={{ width: `${titleWidth}px`, textAlign: 'right', height: '24px' }}>{title}</label>) : (null)}
                    {radioList}
                </div>
            </React.Fragment>
        );
    }
}

// Type 체크
SRadio.propTypes = {
    titleWidth: PropTypes.number,
    contentWidth: PropTypes.number,
}

// Default 값 설정
SRadio.defaultProps = {
    id: 'radio',
    valueMember: 'cd',
    displayMember: 'cdNm',
    titleWidth: 90,
    contentWidth: 110,
}