
import React, { Component } from 'react'
import 'components/atoms/button/style.css'
import { SConsumer } from 'utils/context/SContext'
import PropTypes from 'prop-types';
import { SModalConsumer } from 'utils/context/SModalContext';

class SButton extends Component {

    menuInfo = JSON.parse(window.sessionStorage.getItem('allSubMenuArr'));

    getButtonAuth = context => {

        try {
            
           return 'Y'

        } catch (exception) {
            console.warn('error', exception)
            return 'N';
        }
    }

    render() {
        const { width, float, onClick, buttonName, visible, type } = this.props;

        return (
            <SConsumer>
                {
                    (context) => {
                        return (
                            <SModalConsumer>
                                {
                                    (modalContext) => {
                                        const systemGrpYn = context ? context.systemGrpYn : 'N';
                                        const target = modalContext ? modalContext : (context ? context : null);
                                        if (visible == true) {
                                            return (
                                                <li className="button_item">
                                                    <input className="btn_blue" id={type} type="button" value={buttonName} onClick={onClick} style={{ width: width }} />
                                                </li>
                                            )
                                        } else {
                                            return null
                                        }
                                    }
                                }
                            </SModalConsumer>
                        )
                    }
                }
            </SConsumer>
        );
    }
}

export default SButton;

// Type 체크
SButton.propTypes = {
    type: PropTypes.string,//.isRequired,
    visible: PropTypes.bool,
}

SButton.defaultProps = {
    visible: true,
}