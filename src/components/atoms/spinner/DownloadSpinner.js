import React from 'react';
import 'components/atoms/spinner/style.css'

const DownloadSpinner = () => {
    return (
        <section id="downloadsection" className="download_section" style={{ display: 'none' }}>
            <div className="loader loader-3">
                <div className="dot dot1" style={{ background: '#E84E25' }}></div>
                <div className="dot dot2" style={{ background: '#E84E25' }}></div>
                <div className="dot dot3" style={{ background: '#E84E25' }}></div>
            </div>
        </section>
    );
};
export default DownloadSpinner;