import React, { Component } from 'react';
import 'components/atoms/checkbox/style.css'

class SCheckBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: ''
            , checked: false
        }

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        const { value } = this.props;

        if (value == 'Y') {
            this.setState({ checked: true, value: 'Y' });
        }
        else {
            this.setState({ checked: false, value: 'N' });
        }
    }

    handleChange(event) {
        let data = ''
        if (event.target.checked == true) {
            this.setState({ checked: true, value: 'Y' });
            data = { "id": this.props.id, "value": 'Y' };
        }
        else {
            this.setState({ checked: false, value: 'N' });
            data = { "id": this.props.id, "value": 'N' };
        }

        if (this.props.onChange) {
            this.props.onChange(data);
        }
    }

    render() {
        const { id, title, size } = this.props;

        return (
            <div className="check_item">
                <div className="check_box_warp">
                    <div className="check_box">
                        <input
                            id={id}
                            type="checkbox"
                            checked={this.state.checked}
                            onChange={this.handleChange}
                        />
                        <label htmlFor={id} className="item_lb" style={{ marginTop: "0px" }}>{title}</label>
                    </div>
                </div>
            </div>
        );
    }
}

export default SCheckBox;