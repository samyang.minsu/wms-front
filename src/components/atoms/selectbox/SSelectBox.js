
import React, { Component } from 'react'
import { inject } from 'mobx-react';
import PropTypes from 'prop-types';
import 'components/atoms/selectbox/style.css'

@inject("commonStore")
export default class SSelectBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            optionList: []
        }
    }

    async componentWillMount() {

        const { codeGroup, optionGroup } = this.props;

        // codeGroup : 공통코드 가져와서 Binding.
        // optionGroup : Custom List Binding.
        if (codeGroup) {
            await this.getCodeGroup(codeGroup);
        } else {
            await this.setList(optionGroup);
        }
    }

    handleChange = (event) => {

        const { onChange, valueMember, onSelectionChange } = this.props;
        let data = { "id": event.target.id, "value": event.target.value };
        onChange(data);

        if (onSelectionChange) {
            const selectedItem = this.state.optionList.filter(x => eval(`x.${valueMember}`) == event.target.value);
            onSelectionChange(selectedItem[0]);
        }
    }

    getCodeGroup = async (codeGroup) => {

        const { commonStore, id, defaultValue, onChange } = this.props;

        await commonStore.getCodeObject().then(codeObject => {

            let option = codeObject[codeGroup].concat();
            this.setList(option);
        });

        if (defaultValue) {
            onChange({ "id": id, "value": defaultValue });
        }
    }

    setList = async (optionList) => {
        const { addOption, valueMember, displayMember } = this.props;

        if (addOption && !optionList.some(x => x[valueMember] == "")) {
            let all = {};
            all[valueMember] = "";
            all[displayMember] = addOption;

            optionList.unshift(all);
        }

        await this.setState({ optionList });
    }

    async componentWillReceiveProps(nextProps) {

        if (nextProps.codeGroup) {
            if (this.props.codeGroup != nextProps.codeGroup) {
                await this.getCodeGroup(nextProps.codeGroup);
            }
        } else {
           if (JSON.stringify(this.state.optionList) != JSON.stringify(nextProps.optionGroup)) {
                this.setList(nextProps.optionGroup);
            }
        }
    }

    render() {

        const { id, value, pk, title, notIn, disabled, valueMember, displayMember, contentWidth, titleWidth, marginLeft, marginRight } = this.props;
        const options = this.state.optionList;

        const selectOption =
            options && options.map(function (select) {

                const optionValue = select[valueMember];
                const optionDisplay = select[displayMember];
                let optionKey = select[valueMember];

                if (pk) {
                    const keys = pk.split(',');
                    optionKey = '';
                    
                    keys.map(item => {
                        optionKey += select[item] + '_';
                    });
                }

                if (notIn) {
                    if (notIn.indexOf(optionValue) > -1) {
                    } else {
                        return <option key={optionKey} value={optionValue}>{optionDisplay}</option>
                    }
                } else {
                    return <option key={optionKey} value={optionValue}>{optionDisplay}</option>
                }
            });

        return (
            <div className="select_item" style={{ marginLeft: `${marginLeft}px`, marginRight: `${marginRight}px`}}>
                {title ? (<label className="item_lb" style={{ width: `${titleWidth}px`, textAlign: 'right' }}>{title}</label>) : (null)}
                <div className="select_box" style={{ width: `${contentWidth}px` }}>
                    <select id={id} value={value ? value : ""} disabled={disabled} onChange={this.handleChange} className="w250" style={{ color: "#333" }}>
                        {selectOption}
                    </select>
                </div>
            </div>
        );
    }
}

SSelectBox.propTypes = {
    title: PropTypes.string,
    id: PropTypes.string.isRequired,
    pk: PropTypes.string,
    valueMember: PropTypes.string,
    displayMember: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    titleWidth: PropTypes.number,
    contentWidth: PropTypes.number,
    onChange: PropTypes.func,
    codeGroup: PropTypes.string,
}

SSelectBox.defaultProps = {
    valueMember: 'cd', // value property
    displayMember: 'cdNm', // display text property
    optionGroup: [],

    titleWidth: 90,
    contentWidth: 110,

    marginLeft: 0,
    marginRight: 0,
}