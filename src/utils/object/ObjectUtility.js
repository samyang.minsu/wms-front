
class ObjectUtility {

    constructor(root) {
        this.root = root;
    }

    replaceAll(str, searchStr, replaceStr) {
        return str.split(searchStr).join(replaceStr);
    }

    objectSize(obj) {
        let size = 0, key;
        if (obj) {
            for (key in obj) {
                if (obj.hasOwnProperty(key)) { size++; }
            }
        }
        return size;
    }

    textareaOut(data) {
        return data.replace(/(?:\r\n|\r|\n)/g, '<br />');
    }

    textareaIn(data) {
        return data.replace(/<br \/>/g, '\n');
    }

    submitData(dataForm, data) {
        let resultData = {};
        const { fields, type } = dataForm;
        const length = fields.length;

        for (let i = 0; i < length; i++) {
            const f = fields[i];
            const d = data[f];
            resultData[f] = d;
        }

        if (type == "json") {
            resultData = JSON.stringify(resultData);
        }
        console.log(resultData);
        return resultData;
    }

    loadingLayer(isLayerPop) {

        const element = document.querySelector('#spinnersection');
        const layerOverlap = document.querySelector('#layerOverlap');

        let display1 = '';
        let display2 = '';

        if (element) {
            if (isLayerPop) {
                display1 = 'block';
            } else {
                display1 = 'none';
            }

            element.style.display = `${display1}`;
        }

        if (layerOverlap) {
            if (isLayerPop) {
                display2 = 'block';
            } else {
                display2 = 'none';
            }
            layerOverlap.style.display = `${display2}`;
        }
    }

    convertToDateFromYYYYMMDD(date) {
        const newValue = date.replace(/[^0-9]/g, '');
        const year = parseInt(newValue.substring(0, 4));
        const month = parseInt(newValue.substring(4, 6)) - 1;
        const day = newValue.substring(6, 8) == '' ? 1 : parseInt(newValue.substring(6, 8));

        return new Date(year, month, day);
    }

    convertToYYYYMMDDFromDate(date) {

        if(date != null)
        {
            const year = date.getFullYear();
            let month = (1 + date.getMonth());
            month = month >= 10 ? month : '0' + month;
            let day = date.getDate();
            day = day >= 10 ? day : '0' + day;

            return `${year}${month}${day}`
        }

        return ''
    }

    addDays(date, addDay) {
        const targetDate = this.convertToDateFromYYYYMMDD(date);
        targetDate.setDate(targetDate.getDate() + addDay);
        return this.convertToYYYYMMDDFromDate(targetDate);
    }

    convartToDateFromHHMM(date) {
        const newValue = date.replace(/[^0-9]/g, '');
        const hh = newValue.substring(0, 2);
        const mm = newValue.substring(2, 4);

        const now = new Date();
        now.setHours(parseInt(hh));
        now.setMinutes(parseInt(mm));

        return now;
    }
}

export default new ObjectUtility();