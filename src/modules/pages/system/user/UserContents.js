import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    userStore: stores.userStore
}))

class UserContents extends Component {

    constructor(props) {
        super(props);
    }
    render() {
        const { userStore } = this.props;
        return (
            <SGrid grid={"userGrid"}
                rowData={userStore.userList}
                gridApiCallBack={userStore.setGridApi}
                editable={false} />
        );
    }
}
export default UserContents;