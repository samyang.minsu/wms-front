import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';
import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    loginId: stores.userStore.loginId,
    handleChange: stores.userStore.handleChange,
    editType: stores.userStore.editType,

    empNo: stores.userStore.empNo,
    deptCd: stores.userStore.deptCd,

    loginNm: stores.userStore.loginNm,
    loginPass: stores.userStore.loginPass,
    loginEmail: stores.userStore.loginEmail,
    loginTel: stores.userStore.loginTel,
    loginMoblTelno: stores.userStore.loginMoblTelno,

    masterYn: stores.userStore.masterYn,
    useYN: stores.userStore.useYN,

    closeModal: stores.userStore.closeModal,
    onClickSave: stores.userStore.onClickSave

    , buId: stores.userStore.buId
    , buList: stores.userStore.buList
    , hrYN: stores.userStore.hrYN
    , outsourcingYn: stores.userStore.outsourcingYn

}))
@observer
class UserModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { loginId, handleChange, editType
            , empNo, deptCd
            , loginNm, loginPass, loginEmail, loginTel, loginMoblTelno
            , masterYn, useYN
            , closeModal, onClickSave 
            , buId
            , buList
            , hrYN
            , outsourcingYn } = this.props;

        return (
            <React.Fragment>
                <table style={{ width: "100%" }}>
                    <tbody>
                        <tr>
                            <td className="th">공장</td>
                            <td>
                                <SSelectBox 
                                    id={"buId"}
                                    value={buId}
                                    onChange={handleChange}
                                    optionGroup={buList}
                                    valueMember={"buId"}
                                    displayMember={"buNm"}
                                    disabled={(editType == 'edit') ? true : false}
                                    addOption={"선택없음"}
                                />
                            </td>
                            <td className="th"></td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td className="th">로그인 ID</td>
                            <td>
                                <SInput
                                    id="loginId"
                                    value={loginId}
                                    onChange={handleChange}
                                    readOnly={(editType == 'edit') ? true : false}
                                    width={200}
                                />
                            </td>
                            <td className="th">사번</td>
                            <td>
                                <SInput
                                    id="empNo"
                                    value={empNo}
                                    readOnly={(editType == 'edit') ? true : false}
                                    onChange={handleChange}
                                    width={200}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td className="th">이름.</td>
                            <td>
                                <SInput
                                    id="loginNm"
                                    value={loginNm}
                                    onChange={handleChange}
                                    width={200}
                                />
                            </td>
                            <td className="th">패스워드</td>
                            <td>
                                <SInput
                                    id="loginPass"
                                    value={loginPass}
                                    onChange={handleChange}
                                    width={200}
                                />
                            </td>
                        </tr>
                        <tr>
                            {/* <th>언어</th>
                            <td>
                                <SSelectBox
                                    id="langId"
                                    onChange={handleChange}
                                    codeGroup={""}
                                    value={langId}
                                     />
                            </td> */}
                            <td className="th">부서코드</td>
                            <td>
                                <SInput
                                    id="deptCd"
                                    value={deptCd}
                                    onChange={handleChange}
                                />
                            </td>
                            <td className="th">이메일</td>
                            <td>
                                <SInput
                                    id="loginEmail"
                                    value={loginEmail}
                                    onChange={handleChange}
                                    width={200}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td className="th">회사 전화번호</td>
                            <td>
                                <SInput
                                    id="loginTel"
                                    value={loginTel}
                                    onChange={handleChange}
                                    width={200}
                                />
                            </td>
                            <td className="th">모바일 번호</td>
                            <td>
                                <SInput
                                    id="loginMoblTelno"
                                    value={loginMoblTelno}
                                    onChange={handleChange}
                                    width={200}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td className="th">생산관리자 여부</td>
                            <td>
                                <SSelectBox
                                    id="masterYn"
                                    value={masterYn}
                                    onChange={handleChange}
                                    codeGroup={"SYS001"}
                                />
                            </td>
                            <td className="th">사용여부</td>
                            <td>
                                <SSelectBox
                                    id="useYN"
                                    value={useYN}
                                    onChange={handleChange}
                                    codeGroup={"SYS001"}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td className="th">HR여부</td>
                            <td>
                                <SSelectBox
                                    id="hrYN"
                                    value={hrYN}
                                    onChange={handleChange}
                                    codeGroup={"SYS001"}
                                />
                            </td>
                            <td className="th">외주여부</td>
                            <td>
                                <SSelectBox
                                    id="outsourcingYn"
                                    value={outsourcingYn}
                                    onChange={handleChange}
                                    codeGroup={"SYS001"}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div >
                    <SButton buttonName="취소" onClick={closeModal} type={"default"} />
                    <SButton buttonName="저장" onClick={onClickSave} type={"btnSave"} />
                </div>
            </React.Fragment>
        );
    }
}

export default UserModal;