import React, { Component } from "react";
import { inject } from "mobx-react";

import ContentsTemplate from 'components/template/ContentsTemplate';
import SModal from 'components/override/modal/SModal';

import GrpContents from 'modules/pages/system/grp/GrpContents';
import GrpModal from 'modules/pages/system/grp/grpmodal/GrpModal';

import EmployeeMultiHelper from 'components/codehelper/EmployeeMultiHelper';
import GrpCopyModal from "modules/pages/system/grp/grpCopyModal/GrpCopyModal";

@inject(stores => ({
    modalIsOpen: stores.grpStore.modalIsOpen
    , closeModal: stores.grpStore.closeModal
    , initCheck: stores.grpStore.initCheck
    , isEmpOpen: stores.grpStore.isEmpOpen
    , handleChange: stores.grpStore.handleChange
    , lotHelperResult:stores.grpStore.lotHelperResult
    , temp: stores.grpStore.temp
    , grpStore: stores.grpStore
    , copyModalOpen: stores.grpStore.copyModalOpen
    , copyModalClose: stores.grpStore.copyModalClose
}))

class Grp extends Component {

    constructor(props) {
        super(props);

        // 권한 조회 최초 한번만
        if (this.props.grpStore.initCheck != 'Y') {
            this.props.grpStore.searchGrp();
        }
    }

    render() {

        const { modalIsOpen, closeModal, isEmpOpen, handleChange, lotHelperResult, temp
                , copyModalOpen
                ,  copyModalClose
            
                } = this.props;

        return (
            <React.Fragment>
                <ContentsTemplate id='grp' contentItem={<GrpContents />} />
                <SModal
                    isOpen={modalIsOpen}
                    onRequestClose={closeModal}
                    contentLabel="버튼 권한 등록"
                    width={"400px"}
                    height={"400px"}
                    contents={<GrpModal />}
                />

                <SModal
                    isOpen={copyModalOpen}
                    onRequestClose={copyModalClose}
                    contentLabel="권한 복사"
                    width={450}
                    height={220}
                    contents={<GrpCopyModal />}
                />

                <EmployeeMultiHelper
                    id={'isEmpOpen'}
                    isEmpOpen={isEmpOpen}
                    handleChange={handleChange}
                    onHelperResult={lotHelperResult}
                    selectedEmployeeList={temp}
                />


            </React.Fragment>
        );
    }
}

export default Grp;