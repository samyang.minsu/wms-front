import React, { Component } from 'react';
import { inject } from 'mobx-react';

import STextArea from 'components/atoms/input/STextArea';
import SButton from 'components/atoms/button/SButton';
import SInput from 'components/atoms/input/SInput';




@inject(stores => ({
    copySave: stores.grpStore.copySave
    , newGrpId: stores.grpStore.newGrpId
    , newGrpNm: stores.grpStore.newGrpNm
    , grpDesc: stores.grpStore.grpDesc
    , handleChange: stores.grpStore.handleChange
}))

class GrpCopyModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const {   copySave
                , newGrpId
                , newGrpNm
                , grpDesc
                , handleChange } = this.props;

        return (
            <React.Fragment>


                <div className='btn_wrap'>
                    <SButton buttonName={"저장"} onClick={copySave} type={"btnCopySave"} />
                    {/* <SButton buttonName={"닫기"} onClick={etcInModalClose} type={"delete"} /> */}
                </div>

                <table style={{ width: "100%" }}>
                        <tbody>
                            <tr>
                                <th>신규권한ID</th>
                                <td>
                                    <SInput
                                        id={"newGrpId"}
                                        value={newGrpId}
                                        onChange={handleChange}
                                    />
                                </td>

                                <th>신규권한명</th>
                                <td>
                                    <SInput
                                        id={"newGrpNm"}
                                        value={newGrpNm}
                                        onChange={handleChange}
                                    />
                                </td>
                            </tr>
                            
                            <tr>
                                <th>설명</th>
                                <td colSpan="3">
                                    <STextArea
                                        id="grpDesc"
                                        value={grpDesc}
                                        onChange={handleChange}
                                        contentWidth={320}
                                    />
                                </td>
                            </tr>

                        </tbody>
                    </table>

            </React.Fragment>
        );
    }
}

export default GrpCopyModal;