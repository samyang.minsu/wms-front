import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
    setGridApiModal: stores.grpStore.setGridApiModal
}))

class GrpModalContents extends Component {

    render() {
        const { setGridApiModal } = this.props;

        return (
            <SGrid
                grid={"itemSearchGrid"}
                gridApiCallBack={setGridApiModal}
                editable={false} />
        );
    }
}
export default GrpModalContents;