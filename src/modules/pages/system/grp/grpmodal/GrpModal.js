import React, { Component } from 'react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import GrpModalSearchItem from 'modules/pages/system/grp/grpmodal/GrpModalSearchItem';
import GrpModalMiddleItem from 'modules/pages/system/grp/grpmodal/GrpModalMiddleItem';
import GrpModalContents from 'modules/pages/system/grp/grpmodal/GrpModalContents';

class GrpModal extends Component {
    constructor(props) {
        super(props);
    }
    render() {

        return (
            <React.Fragment>
                <ContentsMiddleTemplate middleItem={<GrpModalMiddleItem />} />
                <ContentsTemplate id='grpModal' contentItem={<GrpModalContents />} />
            </React.Fragment>
        );
    }
}

export default GrpModal;