import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from "components/atoms/button/SButton";

@inject(stores => ({
    grpStore: stores.grpStore
    , handleChange: stores.grpStore.handleChange
    , modalSearchText_s: stores.grpStore.modalSearchText_s
    , modalLvl_s: stores.grpStore.modalLvl_s
}))

class GrpModalSearchItem extends Component {

    render() {
        const { grpStore, handleChange, modalSearchText_s } = this.props;

        return (
            <React.Fragment>
                <SInput title={"로그인ID/성명"} isFocus={true} id={'modalSearchText_s'} value={modalSearchText_s} onChange={handleChange} onKeyDown={grpStore.onClickModalSearch} />

                <div className='search_item_btn'>
                    <SButton buttonName={'조회'} onClick={grpStore.onClickModalSearch} type={'btnSearch'} />
                </div>
            </React.Fragment>
        );
    }
}
export default GrpModalSearchItem;