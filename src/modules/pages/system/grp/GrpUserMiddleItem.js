import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from "components/atoms/button/SButton";

@inject(stores => ({
    onClickUserAdd: stores.grpStore.onClickUserAdd
    , onClickUserDelete: stores.grpStore.onClickUserDelete
}))
class GrpUserMiddleItem extends Component {
    render() {
        const { onClickUserAdd, onClickUserDelete } = this.props;

        return (
            <div>
                <SButton buttonName={'추가'} onClick={onClickUserAdd} type={'btnUserAdd'} />
                <SButton buttonName={'삭제'} onClick={onClickUserDelete} type={'btnUserDelete'} />
            </div>
        );
    }
}

export default GrpUserMiddleItem;
