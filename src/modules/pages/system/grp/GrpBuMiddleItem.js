import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from "components/atoms/button/SButton";

@inject(stores => ({
   onClickBuSave: stores.grpStore.onClickBuSave
}))
class GrpBuMiddleItem extends Component {
    render() {
        const { onClickBuSave } = this.props;

        return (
            <div>
                <SButton buttonName={'저장'} onClick={onClickBuSave} type={'btnBuSave'} />
            </div>
        );
    }
}

export default GrpBuMiddleItem;