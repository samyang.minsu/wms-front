import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from "components/atoms/button/SButton";

@inject(stores => ({
    onClickGrpAdd: stores.grpStore.onClickGrpAdd
    , onClickGrpSave: stores.grpStore.onClickGrpSave
    , onClickGrpDelete: stores.grpStore.onClickGrpDelete
    , onClickGrpCopy: stores.grpStore.onClickGrpCopy
}))
class GrpMiddleItem extends Component {
    render() {
        const { onClickGrpAdd, onClickGrpSave, onClickGrpDelete, onClickGrpCopy } = this.props;

        return (
            <div>
                <SButton buttonName={'추가'} onClick={onClickGrpAdd} type={'btnAdd'} />
                <SButton buttonName={'복사'} onClick={onClickGrpCopy} type={'btnCopy'} />
                <SButton buttonName={'저장'} onClick={onClickGrpSave} type={'btnSave'} />
                <SButton buttonName={'삭제'} onClick={onClickGrpDelete} type={'btnDelete'} />
            </div>
        );
    }
}

export default GrpMiddleItem;
