import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from "components/atoms/button/SButton";

@inject(stores => ({
    onClickDeptSave: stores.grpStore.onClickDeptSave
}))

class GrpDeptMiddleItem extends Component {
    render() {
        const { onClickDeptSave } = this.props;

        return (
            <div>
                <SButton buttonName={'저장'} onClick={onClickDeptSave} type={'btnDeptSave'} />
            </div>
        );
    }
}

export default GrpDeptMiddleItem;
