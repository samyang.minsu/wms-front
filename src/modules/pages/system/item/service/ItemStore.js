import { observable, action } from 'mobx';

import GridUtil from 'components/override/grid/utils/GridUtil'

import ItemRepository from 'modules/pages/system/item/service/ItemRepository';
import Alert from 'components/override/alert/Alert';

import CommonStore from 'modules/pages/common/service/CommonStore';

class ItemStore {
    /* 탭 */
    @observable uomTab = 'on';
    @observable bomTab = 'off';

    // Tab 전환 Event
    @observable activeIndex = 0;
	@action.bound
	handleTabChange = async (data) => {
        this[data.id] = data.value;
		if (data.id == 'activeIndex') {
			this.handleDoubleClick();
		}
    }
    @action.bound
	handleDoubleClick = async () => {
		if (this.activeIndex == 0) {
            //this.setUomGridApi();
            this.displayUom();
            this.uomList = [];
		}
		else if (this.activeIndex == 1) {
            //this.setBomGridApi();
            this.displayBom();
            this.bomList = [];
        }
        this.getTheOthers();
    }
    
    @observable buIdCheck = false;
    @observable buId = '';

    @action.bound
    getBuId = async () => {
        this.buId = CommonStore.buId;
        this.buIdCheck = true;
    }

    @action.bound
    displayUom() {
        this.uomTab = 'on';
        this.bomTab = 'off';
    }

    @action.bound
    displayBom() {
        this.uomTab = 'off';
        this.bomTab = 'on';
    }

    @observable glclass = 'S120';
    @observable searchText = '';

    @observable itemList = [];
    // 변경할 ITEM의 INVEN_YN, BARCODE_YN, LOC_YN 값 저장List 변수
    @observable setYnDataList = [];

    itemGridApi = undefined;

    @action.bound
    handleChange = (data) => {
        console.log(data)
        this[data.id] = data.value;
    }

    @action.bound
    setGridApi = async (gridApi) => {
        this.itemGridApi = gridApi;
        const columnDefs = [
            { headerName: "제품ID", field: "itemId", width: 60, cellClass: "cell_align_center" },
            { headerName: "제품코드", field: "itemCd", width: 60, cellClass: "cell_align_center" },
            { headerName: "제품명", field: "itemNm", width: 170, cellClass: "cell_align_left" },
            { headerName: "규격", field: "spec", width: 60, cellClass: "cell_align_left" },
            { headerName: "SEARCHTEXT", field: "searchText", width: 90, cellClass: "cell_align_left" },
            { headerName: "SEARCHTEXT2", field: "searchText2", width: 90, cellClass: "cell_align_left" },
            { headerName: "재고관리", field: "invenYn", width: 60, cellClass: "cell_align_center", editable: true },
            { headerName: "바코드관리", field: "barcodeYn", width: 80, cellClass: "cell_align_center", editable: true },
            { headerName: "LOT관리", field: "lotYn", width: 60, cellClass: "cell_align_center", editable: true },
            { headerName: "GLCLASS", field: "glclass", width: 60, cellClass: "cell_align_center" },
            { headerName: "제품타입", field: "itemType", width: 60, cellClass: "cell_align_center" },
            { headerName: "유통기한", field: "expiryDay", width: 60, cellClass: "cell_align_center" },
            { headerName: "품목단위", field: "itemUom", width: 60, cellClass: "cell_align_center" },
            { headerName: "가격단위", field: "pricingUom", width: 60, cellClass: "cell_align_center" },
            {
                headerName: "가격수량", field: "pricingConvtQty", width: 70, cellClass: "cell_align_right"
                , valueFormatter: GridUtil.numberFormatter
            },
            { headerName: "물류단위", field: "shippingUom", width: 60, cellClass: "cell_align_center" },
            {
                headerName: "물류수량", field: "shippingConvtQty", width: 70, cellClass: "cell_align_right"
                , valueFormatter: GridUtil.numberFormatter
            },
            { headerName: "생산단위", field: "productionUom", width: 60, cellClass: "cell_align_center" },
            {
                headerName: "생산수량", field: "productionConvtQty", width: 70, cellClass: "cell_align_right"
                , valueFormatter: GridUtil.numberFormatter
            },
            { headerName: "PL단위", field: "palletUom", width: 60, cellClass: "cell_align_center" },
            {
                headerName: "PL수량", field: "palletConvtQty", width: 70, cellClass: "cell_align_right"
                , valueFormatter: GridUtil.numberFormatter
            }
        ];

        const optList = [
            {
                cd: null,
                cdNm: '',
            },
            {
                cd: 'Y',
                cdNm: 'Y',
            },
            {
                cd: 'N',
                cdNm: 'N',
            }
        ];

        const col1 = GridUtil.findColumnDef(columnDefs, 'invenYn');
        const col2 = GridUtil.findColumnDef(columnDefs, 'barcodeYn');
        const col3 = GridUtil.findColumnDef(columnDefs, 'lotYn');
        GridUtil.setSelectCellByList(col1, optList, "cd", "cdNm");
        GridUtil.setSelectCellByList(col2, optList, "cd", "cdNm");
        GridUtil.setSelectCellByList(col3, optList, "cd", "cdNm");

        this.itemGridApi.setColumnDefs(columnDefs);
    }

    // 조회
    @action.bound
    handleSearchClick = async () => {

        const searchdata = {
            "glclass": this.glclass
            , "searchText": this.searchText
        }

        const { data, status } = await ItemRepository.getItems(searchdata);

        if (status == 200) {
            this.itemList = data.data;
            this.itemGridApi.setRowData(this.itemList);
        }
    }

    //INVEN_YN, BARCODE_YN, LOC_YN 변경사항 Event
    @action.bound
    setChangeYn = async (event) => {
        console.log('Row Change', event)
        if (event.oldValue != event.newValue) {
            console.log('1', event.node.data) // itemId,itemCd,itemNm
            console.log('2', event.colDef.field) // 칼럼명

            const setYnData = {
                "itemId": event.node.data.itemId
                , "itemCd": event.node.data.itemCd
                , "itemNm": event.node.data.itemNm
                , "setField": event.colDef.field
                , "setState": event.newValue == 'null' ? null : event.newValue
            }
            this.setYnDataList.push(setYnData);
        }
    }

    //INVEN_YN, BARCODE_YN, LOC_YN 변경 Btn
    @action.bound
    setYnUpdate = async () => {
        if (this.setYnDataList.length > 0) {
            const value = await Alert.confirm("변경하시겠습니까?").then(result => { return result; });
            if (value) {
                const submitData = JSON.stringify({ param: this.setYnDataList });
                const { data, status } = await ItemRepository.setYnState(submitData);
                if (data.success == false || status != 200) {
                    Alert.meg(data.msg);
                } else {
                    Alert.meg('성공 하였습니다.');
                }
                this.setYnDataList = [];
            } else {
                return;
            }
        } else {
            Alert.meg('변경할 관리대상이 없습니다.');
            return;
        }
    }

    /* 메인 그리드 클릭시 하단 그리드 조회 */
    @action.bound
    getTheOthers = async () => {
        if (this.uomTab == "on") {
            this.btnSearchUom();
        }
        if (this.bomTab == "on") {
            this.btnSearchBom();
        }
    }

    /* Uom 관련 변수 및 기능 */
    uomGridApi = undefined;
    @observable uomList = [];

    // 조회
    @action.bound
    btnSearchUom = async () => {
        const selectedRow = this.itemGridApi.getSelectedRows()[0];
        const searchData = {
            itemId: selectedRow.itemId
        }
        const { data, status } = await ItemRepository.getUom(searchData);
        if (data.success == false || status != 200) {
            Alert.meg(data.msg);
        } else {
            this.uomList = data.data;
            this.uomGridApi.setRowData(this.uomList);
        }
    }

    // 저장
    @action.bound
    btnInsertUom = async () => {
        const saveArr = GridUtil.getGridSaveArray(this.uomGridApi);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        //대소문자 변환
        saveArr.forEach(idx => {
            idx.fromUom = idx.fromUom.toUpperCase();
            idx.toUom = idx.toUom.toUpperCase();
        });

        //중복품목 제거
        let saveArrConfList = saveArr.reduce((accumulator, current) => {
            if (checkAlreadyExist(current)) {
                return accumulator;
            } else {
                return [...accumulator, current];
            }
    
            function checkAlreadyExist(currentVal) {
                return accumulator.some((item) => {
                    return (item.fromUom === currentVal.fromUom && item.toUom === currentVal.toUom);
                });
            }
        }, []);


        const selectedRowItem = this.itemGridApi.getSelectedRows()[0];
        const searchData = {
            "itemId": selectedRowItem.itemId
        }
        
        const param = JSON.stringify({ param1: saveArrConfList, param2: searchData });
        const value = await Alert.confirm("저장 하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await ItemRepository.updateUomData(param);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.uomList = data.data;
                this.uomGridApi.setRowData(this.uomList);
                Alert.meg("저장되었습니다.");
            }
        } else {
            return;
        }
    }

    // 추가
    @action.bound
    btnAddUom = async () => {
        const selectedRow = this.itemGridApi.getSelectedRows()[0];

        let count = this.uomGridApi.getDisplayedRowCount();
        const codeHdmt = {
            itemId: selectedRow.itemId,
            itemCd: selectedRow.itemCd,
            fromUom: '',
            fromQty: 1,
            toUom: '',
            toQty: 1,
            erpYN: 'N',
            eflag: 'T' //저장하기 전,후 ROW의 수정여부 확인을 위한 변수.
        };
        const result = this.uomGridApi.updateRowData({ add: [codeHdmt], addIndex: count });
        GridUtil.setFocus(this.uomGridApi, result.add[0]);

    }

    // 삭제
    @action.bound
    btnDeleteUom = async () => {
        // const selectedRows = this.masterGridApi.getSelectedRows();
        const selectedRow = GridUtil.getSelectedRowData(this.uomGridApi);
        if (!selectedRow) {
            Alert.meg("선택된 대상이 없습니다.");
            return;
        } else if (selectedRow.erpYN == 'Y') {
            Alert.meg('삭제할 수 없는 항목입니다.')
            return;
        }

        const selectedRowItem = this.itemGridApi.getSelectedRows()[0];
        const searchData = {
            "itemId": selectedRowItem.itemId
        }
        const params = JSON.stringify({ param1: selectedRow, param2: searchData });
        const value = await Alert.confirm("삭제 하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await ItemRepository.deleteUomData(params);
            if (data.success == false) {
                Alert.meg(data.msg);
            } else {
                this.uomList = data.data;
                this.uomGridApi.setRowData(this.uomList);
                Alert.meg("삭제되었습니다.");
            }
        } else {
            return;
        }
    }

    // 그리드
    @action.bound
    setUomGridApi = async (gridApi) => {
        this.uomGridApi = gridApi;
        const columnDefs = [
            { headerName: "제품ID", field: "itemId", width: 60, customCellClass: 'cell_align_center', editable: false },
            { headerName: "제품코드", field: "itemCd", width: 60, customCellClass: 'cell_align_center', editable: false },
            {
                headerName: "FROM단위", field: "fromUom", width: 80
                , cellClass: "cell_align_center"
                , editable: function (params) {
                    if (params.node.data.erpYN == 'Y') {
                        return false;
                    } else if (params.node.data.eflag == 'F') {
                        return false;
                    } else
                        return true;
                }
            },
            {
                headerName: "FROM수량", field: "fromQty", width: 80, cellClass: "cell_align_right"
                , valueFormatter: (params) => GridUtil.numberFormatter(params, 4)
                , valueParser: GridUtil.numberParser
                , editable: function (params) {
                    if (params.node.data.erpYN == 'Y') {
                        return false;
                    } else
                        return true;
                }
                , type: "numericColumn"
                , cellRendererParams: { onChange: this.checkValue }
            },
            { headerName: "TO단위", field: "toUom", width: 60, cellClass: "cell_align_center"
                , editable: function (params) {
                    if (params.node.data.erpYN == 'Y') {
                        return false;
                    } else if (params.node.data.eflag == 'F') {
                        return false;
                    } else
                        return true;
            }
            },
            {
                headerName: "TO수량", field: "toQty", width: 70, cellClass: "cell_align_right"
                , valueFormatter: (params) => GridUtil.numberFormatter(params, 4)
                , valueParser: GridUtil.numberParser
                , editable: function (params) {
                    if (params.node.data.erpYN == 'Y') {
                        return false;
                    } else
                        return true;
                }
                , type: "numericColumn"
                , cellRendererParams: { onChange: this.checkValue }
            },
            { headerName: "ERP단위", field: "erpYN", width: 60, customCellClass: 'cell_align_center', editable: false }
            // {
            //     headerName: "FROM단위", field: "fromUom", width: 80, cellClass: "cell_align_center"
            //     , editable: function (params) {
            //         return params.data.erpYN == "N"
            //     }
            // },
        ];
        this.uomGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    checkValue = async (event) => {
    }

    /* Bom 관련 변수 및 기능 */
    bomGridApi = undefined;
    @observable bomList = [];

    // 조회
    @action.bound
    btnSearchBom = async () => {
        const selectedRow = this.itemGridApi.getSelectedRows()[0];
        const searchData = {
            itemId: selectedRow.itemId
            ,buId: this.buId
        }
        const { data, status } = await ItemRepository.getBom(searchData);
        if (data.success == false || status != 200) {
            Alert.meg(data.msg);
        } else {
            console.log(data.data);
            this.bomList = data.data;
            this.bomGridApi.setRowData(this.bomList);
        }
    }
    // 그리드
    @action.bound
    setBomGridApi = async (gridApi) => {
        this.bomGridApi = gridApi;
        const columnDefs = [
            { headerName: "제품코드", field: "itemCd", width: 60, cellClass: "cell_align_center" },
            { headerName: "제품명", field: "itemNm", width: 170, cellClass: "cell_align_left" },
            { headerName: "자재ID", field: "citemCd", width: 60, cellClass: "cell_align_center" },
            { headerName: "자재코드", field: "citemCd", width: 60, cellClass: "cell_align_center" },
            { headerName: "자재명", field: "citemNm", width: 200, cellClass: "cell_align_left" },
            { headerName: "LineNo", field: "lineNo", width: 80, cellClass: "cell_align_center" },
            { headerName: "UOM", field: "standUom", width: 60, cellClass: "cell_align_center"},
            {
                headerName: "수량", field: "standQty", width: 80, cellClass: "cell_align_right"
                , valueFormatter: GridUtil.numberFormatter
            }            
        ];
        this.bomGridApi.setColumnDefs(columnDefs);
    }
}

export default new ItemStore();




// @action.bound
// setGridApi = async (gridApi) => {

//     this.itemGridApi = gridApi;

//     const columnDefs = [
//         { headerName: "제품코드", field: "itemCd", width: 110 },
//         { headerName: "제품ID", field: "itemId", width: 100 },
//         // { 
//         //     headerName: "아이템 구분"
//         //     , field: "itemGubn"
//         //     , width: 105
//         //     , cellClass: "cell_align_center"
//         // },
//         {
//             headerName: "제품타입"
//             , field: "itemType"
//             , width: 100
//             , cellClass: "cell_align_center"
//         },
//         { headerName: "제품명", field: "itemNm", width: 150 },
//         {
//             headerName: "제품단위"
//             , field: "itemUom"
//             , width: 105
//             , cellClass: "cell_align_center"
//         },
//         {
//             headerName: "GLCLASS"
//             , field: "glclassNm"
//             , width: 100
//             , cellClass: "cell_align_center"
//         },
//         {
//             headerName: "유통기한"
//             , field: "expiryDay"
//             , width: 105
//             , type: "numericColumn"
//             , valueFormatter: GridUtil.numberFormatter
//         },
//         {
//             headerName: "AGING_DAY"
//             , field: "agingDay"
//             , width: 105
//             , type: "numericColumn"
//             , valueFormatter: GridUtil.numberFormatter
//         },
//         {
//             headerName: "AGING_DAY_LOTM"
//             , field: "agingDayLotm"
//             , width: 140
//             , type: "numericColumn"
//             , valueFormatter: GridUtil.numberFormatter
//         },
//         { headerName: "규격", field: "spec", width: 150 },
//         { headerName: "검색단어", field: "searchText", width: 150 },
//         { headerName: "검색단어2", field: "searchText2", width: 150 },
//         {
//             headerName: "바코드 유무"
//             , field: "barcodeYN"
//             , width: 115
//             , cellClass: "cell_align_center"
//         },
//         {
//             headerName: "단위"
//             , field: "primaryUom1"
//             , width: 80
//             , cellClass: "cell_align_center"
//         },
//         {
//             headerName: "가격 단위"
//             , field: "pricingUom4"
//             , width: 105
//             , cellClass: "cell_align_center"
//         },
//         {
//             headerName: "선적 단위"
//             , field: "shippingUom6"
//             , width: 105
//             , cellClass: "cell_align_center"
//         },
//         { headerName: "분류1", field: "category1", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류2", field: "category2", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류3", field: "category3", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류4", field: "category4", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류5", field: "category5", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류6", field: "category6", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류7", field: "category7", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류8", field: "category8", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류9", field: "category9", width: 85, cellClass: "cell_align_center" },
//         { headerName: "분류10", field: "category10", width: 85, cellClass: "cell_align_center" },
//         {
//             headerName: "이전 제품코드"
//             , field: "oldItemCd"
//             , width: 140
//             , cellClass: "cell_align_center"
//         }
//         // { 
//         //     headerName: "INPUT_EMP_NO"
//         //     , field: "inputEmpNo"
//         //     , width: 80 
//         //     , cellClass: "cell_align_center"
//         // },
//         // { 
//         //     headerName: "INPUT_EMP_NM"
//         //     , field: "inputEmpNm"
//         //     , width: 80 
//         //     , cellClass: "cell_align_center"
//         // },
//         // { 
//         //     headerName: "INPUT_DTM"
//         //     , field: "inputDtm"
//         //     , width: 150 
//         //     , cellClass: "cell_align_center"
//         // },
//         // { 
//         //     headerName: "MOD_EMP_NO"
//         //     , field: "modEmpNo"
//         //     , width: 80 
//         //     , cellClass: "cell_align_center"
//         // },
//         // { 
//         //     headerName: "MOD_EMP_NM"
//         //     , field: "modEmpNm"
//         //     , width: 80 
//         //     , cellClass: "cell_align_center"
//         // },
//         // { 
//         //     headerName: "MOD_DTM"
//         //     , field: "modDtm"
//         //     , width: 150 
//         //     , cellClass: "cell_align_center"
//         // }
//     ];

//     this.itemGridApi.setColumnDefs(columnDefs);
// }