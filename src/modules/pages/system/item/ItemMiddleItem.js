import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    setYnUpdate: stores.itemStore.setYnUpdate
}))

class ItemMiddleItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {
            setYnUpdate
        } = this.props;

        return (
            <React.Fragment>
                <SButton
                    className="btn_red"
                    buttonName={'변경'}
                    type={'btnYnUpdate'}
                    onClick={setYnUpdate}
                />
            </React.Fragment>
        )
    }
}

export default ItemMiddleItem;