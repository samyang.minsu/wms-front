import React, { Component } from "react";
import { inject } from 'mobx-react';

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import SBuSelectBox from 'components/override/business/SBuSelectBox';

import BomMiddleItem from "modules/pages/system/item/contentsAssemble/bom/BomMiddleItem";
import BomGrid from "modules/pages/system/item/contentsAssemble/bom/BomGrid";

@inject(stores => ({
    buId: stores.itemStore.buId,
    handleChange: stores.itemStore.handleChange,
    btnSearchBom: stores.itemStore.btnSearchBom,
}))

class BomContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {buId, handleChange, btnSearchBom} = this.props;

        return (
            <React.Fragment>
                <div style={{float:'left', margin:'0 0 0 -8px'}}>
                <SBuSelectBox
                    title={"공장"}
                    id={"buId"}
                    value={buId}
                    isAddAll={false}
                    onChange={handleChange}
                    onSelectionChange={btnSearchBom}
                />
                </div>
                <ContentsTemplate id='uomGrid' contentItem={<BomGrid />} />
            </React.Fragment>
        )
    }
}

export default BomContents;