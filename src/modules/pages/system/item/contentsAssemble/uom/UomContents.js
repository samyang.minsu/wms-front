import React, { Component } from "react";

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";

import UomMiddleItem from "modules/pages/system/item/contentsAssemble/uom/UomMiddleItem";
import UomGrid from "modules/pages/system/item/contentsAssemble/uom/UomGrid";

class UomContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <React.Fragment>
                <ContentsMiddleTemplate middleItem={<UomMiddleItem />} />
                <ContentsTemplate id='uomGrid' contentItem={<UomGrid />} />
            </React.Fragment>
        )
    }
}

export default UomContents;