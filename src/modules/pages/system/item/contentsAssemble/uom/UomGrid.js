import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setUomGridApi: stores.itemStore.setUomGridApi
    , checkValue: stores.itemStore.checkValue
    , uomList: stores.itemStore.uomList
}))

class UomGrid extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setUomGridApi, checkValue, uomList } = this.props;

        return (
            <SGrid
                grid={'uomGrid'}
                gridApiCallBack={setUomGridApi}
                // onCellValueChanged={checkValue}
                rowData={uomList}
                editable={false}
                cellReadOnlyColor={true}
            />
        );
    }
}
export default UomGrid;