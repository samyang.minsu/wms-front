import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import STab from 'components/override/tab/STab';

import ItemSearchItem from "modules/pages/system/item/ItemSearchItem";
import ItemMiddleItem from "modules/pages/system/item/ItemMiddleItem";
import ItemContents from "modules/pages/system/item/ItemContents";

import BomContents from 'modules/pages/system/item/contentsAssemble/bom/BomContents';
import UomContents from 'modules/pages/system/item/contentsAssemble/uom/UomContents';

import ItemStore from 'modules/pages/system/item/service/ItemStore';

@inject(stores => ({
    // tab handle
    activeIndex: stores.itemStore.activeIndex
    , handleTabChange: stores.itemStore.handleTabChange

    // 컴파운드 컴포넌트별 가공비
    , uomTab: stores.itemStore.uomTab
    , displayUom: stores.itemStore.displayUom

    // 제품별 제조원가 실적
    , bomTab: stores.itemStore.bomTab
    , displayBom: stores.itemStore.displayBom
}))

class Item extends Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        if (ItemStore.buIdCheck == false) {
            await ItemStore.getBuId();
        }
    }

    render() {
        const { activeIndex, handleTabChange
            , uomTab, displayUom
            , bomTab, displayBom
        } = this.props;

        return (
            <React.Fragment>
                <div style={{ width: '100%', height: '55%' }}>
                    <SearchTemplate searchItem={<ItemSearchItem />} />
                    <ContentsMiddleTemplate contentSubTitle={'제품 관리'} middleItem={<ItemMiddleItem />} />
                    <ContentsTemplate id='item_contents' contentItem={<ItemContents />} />
                </div>
                <div style={{ padding:'10px 0 0 5px'}}></div>
                <div style={{ width: '100%', height: '43%' }}>
                    <STab activeIndex={activeIndex} onChange={handleTabChange} id={"activeIndex"} parentHeight={ 415 } >
                        <div style={{ width: '100%', height: '100%', float: 'left', overflowY: 'auto' }} header="UOM">
                            <ContentsTemplate id='uomTab' contentItem={<UomContents />} />
                        </div>
                        <div style={{ width: '100%', height: '100%', float: 'left', overflowY: 'auto' }} header="BOM">
                            <ContentsTemplate id='bomTab' contentItem={<BomContents />} />
                        </div>
                    </STab>
                </div>
                {/* <div style={{ width: '100%', height: '55%' }}>
                    <ul className="panel_tab">
                        <li className={uomTab}><a style={{ cursor: "pointer" }} onClick={displayUom}>UOM</a></li>
                        <li className={bomTab}><a style={{ cursor: "pointer" }} onClick={displayBom}>BOM</a></li>
                    </ul>
                    {uomTab == 'on' ? (
                        <ContentsTemplate id='uomTab' contentItem={<UomContents />} />
                    ) : (
                            <ContentsTemplate id='bomTab' contentItem={<BomContents />} />
                        )}
                </div> */}
            </React.Fragment>
        )
    }
}

export default Item;