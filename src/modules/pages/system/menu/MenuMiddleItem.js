import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    handleSaveClick: stores.menuMtStore.handleSaveClick,
    handleAddClick: stores.menuMtStore.handleAddClick,
    handleDeleteClick: stores.menuMtStore.handleDeleteClick
}))

class MenuMiddleItem extends Component {

    render() {

        const { handleSaveClick, handleAddClick, handleDeleteClick } = this.props;
        return (
            <React.Fragment>
                <SButton
                    buttonName={'추가'}
                    type={'btnAdd'}
                    onClick={handleAddClick}
                />
                <SButton
                    buttonName={'저장'}
                    type={'btnSave'}
                    onClick={handleSaveClick}
                />
                <SButton
                    buttonName={'삭제'}
                    type={'btnDelete'}
                    onClick={handleDeleteClick}
                />
            </React.Fragment>
        )
    }
}

export default MenuMiddleItem;