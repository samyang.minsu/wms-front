import API from "components/override/api/API";

class MenuMtRepository {

    URL = "/api/system/menumt";
    // 조회
    getMenuMts(params) {
        return API.request.get(encodeURI(`${this.URL}/search?menuId=${params.menuId}&menuNm=${params.menuNm}`))
    }

    //프로그램 조회
    getForm(params) {
        return API.request.get(encodeURI(`${this.URL}/getform?menuId=${params.menuId}`));
    }

    //프로그램 모달 조회
    getmodalFrom(params) {
        return API.request.get(encodeURI(`${this.URL}/getmodalform?formId=${params.formId}&formNm=${params.formNm}&formType=${params.formType}`));
    }

    // 저장
    saveMenuMts(params) {
        return API.request.post(`${this.URL}/upsert`, params)
    }

    //프로그램 저장
    saveForms(params) {
        return API.request.post(`${this.URL}/formupsert`, params)
    }

    // 삭제
    deleteMenuMts(params) {
        return API.request.delete(encodeURI(`${this.URL}/delete?menuId=${params.menuId}&searchMenuId=${params.searchMenuId}`))
    }

    //프로그램 삭제
    deleteForm(params) {
        return API.request.delete(encodeURI(`${this.URL}/deleteform?menuId=${params.menuId}&formId=${params.formId}`))
    }
}
export default new MenuMtRepository();