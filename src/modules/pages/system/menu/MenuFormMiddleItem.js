import React, { Component } from "react";
import { inject } from "mobx-react";

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    formAddClick: stores.menuMtStore.formAddClick,
    fromSaveClick: stores.menuMtStore.fromSaveClick,
    fromDeleteClick: stores.menuMtStore.fromDeleteClick
}))

class MenuFormMiddleItem extends Component {

    render() {

        const { formAddClick, fromSaveClick, fromDeleteClick } = this.props;
        return (
            <React.Fragment>
                <SButton
                    buttonName={'추가'}
                    type={'btnfromAdd'}
                    onClick={formAddClick}
                />
                <SButton
                    buttonName={'저장'}
                    type={'btnfromSave'}
                    onClick={fromSaveClick}
                />
                <SButton
                    buttonName={'삭제'}
                    type={'btnfromDelete'}
                    onClick={fromDeleteClick}
                />
            </React.Fragment>
        )
    }
}

export default MenuFormMiddleItem;