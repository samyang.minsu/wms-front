import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    menuId_s: stores.menuMtStore.menuId_s,
    handleChange: stores.menuMtStore.handleChange,
    handleSearchClick: stores.menuMtStore.handleSearchClick
}))

class MenuSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { menuId_s, handleChange, handleSearchClick } = this.props;

        return (

            <React.Fragment>
                <SInput
                    title={"메뉴ID/명"}
                    id={"menuId_s"}
                    value={menuId_s}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                />
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick} />
                </div>
            </React.Fragment>
        )
    }
}

export default MenuSearchItem;