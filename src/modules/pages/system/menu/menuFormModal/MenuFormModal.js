import React, { Component } from 'react';

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";


import MenuFormModalSearchItem from 'modules/pages/system/menu/menuFormModal/MenuFormModalSearchItem';
import MenuFormModalMiddleItem from 'modules/pages/system/menu/menuFormModal/MenuFormModalMiddleItem';
import MenuFormModalContents from 'modules/pages/system/menu/menuFormModal/MenuFormModalContents';



class MenuFormModal extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <React.Fragment>
                
                <SearchTemplate searchItem={<MenuFormModalSearchItem />} />
                <ContentsMiddleTemplate contentSubTitle={'프로그램 정보'} middleItem={<MenuFormModalMiddleItem />} />
                <ContentsTemplate id='menuFormModal' contentItem={<MenuFormModalContents />} />
               
            </React.Fragment>
        )
    }
}

export default MenuFormModal;