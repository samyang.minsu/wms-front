import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    formId_s: stores.menuMtStore.formId_s,
    handleChange: stores.menuMtStore.handleChange,
    modalSearchClick: stores.menuMtStore.modalSearchClick
}))

class MenuFormModalSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { formId_s, handleChange, modalSearchClick } = this.props;

        return (

            <React.Fragment>
                <SInput
                    title={"프로그램ID/명"}
                    id={"formId_s"}
                    value={formId_s}
                    onChange={handleChange}
                    onEnterKeyDown={modalSearchClick}
                />
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={modalSearchClick} />
                </div>
            </React.Fragment>
        )
    }
}

export default MenuFormModalSearchItem;