import React, { Component } from 'react';
import { inject } from 'mobx-react';
import DownloadSpinner from 'components/atoms/spinner/DownloadSpinner';

@inject(stores => ({
    downloadFile: stores.storageStore.downloadFile,
    downloadStateText: stores.storageStore.downloadStateText,
}))
export default class Storage extends Component {

    constructor(props) {
        super(props);
    }

    async componentDidMount() {
        const { match, downloadFile } = this.props;
        await downloadFile(match.params.target);
    }

    render() {

        return (
            <React.Fragment>
                <div style={{
                    fontSize: '60px'
                    , textAlign: 'center'
                    , top: '40%'
                    , position: 'absolute'
                    , width: '100%'
                }}>
                    {this.props.downloadStateText}
                </div>
                <DownloadSpinner />
            </React.Fragment>
        )
    }
}