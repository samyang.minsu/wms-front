import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.testStore.setGridApi,
    handleChange: stores.testStore.handleChange,
}))
class UserContents extends Component {

    constructor(props) {
        super(props);
    }
    render() {

        const { setGridApi, handleChange } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid gridApiCallBack={setGridApi} />
            </div>
        );
    }
}
export default UserContents;