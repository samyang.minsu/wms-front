import { observable, action, toJS } from 'mobx';

class TestStore {

    gridApi = undefined;
    gridColumnApi = undefined;

    selectedItem = {};

    @action.bound
    handleChange(data) {
        this[data.id] = data.value;
    }

    @action.bound
    setGridApi(gridApi) {
        this.gridApi = gridApi;

        this.columnDefs = [

            { headerName: "로그인ID", field: "loginId", width: 100, cellClass: 'cell_align_center' },
            { headerName: "사번", field: "empNo", width: 90, cellClass: 'cell_align_center' },
            { headerName: "이름", field: "loginNm", width: 80, cellClass: 'cell_align_center' },
            { headerName: "이메일", field: "loginEmail", width: 200 },
            { headerName: "전화번호", field: "loginTel", width: 120 },
            { headerName: "모바일", field: "loginMoblTelno", width: 120 },
            { headerName: "언어", field: "langNm", width: 80, cellClass: 'cell_align_center' },
            { headerName: "마스터여부", field: "masterYn", width: 100, cellClass: 'cell_align_center' },
            { headerName: "사용여부", field: "useFlag", width: 90, cellClass: 'cell_align_center' },
            { headerName: "등록자", field: "inputEmpNm", width: 80, cellClass: 'cell_align_center' },
            { headerName: "등록일", field: "inputDtm", width: 150 },

        ];

        this.gridApi.setColumnDefs(this.columnDefs);
    }

    @action.bound
    handleHelperResult(result) {
        console.log('userstore result', result, this.selectedItem);
        if (result) {
            
        }
    }
}

export default new TestStore();