import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import SButton from 'components/atoms/button/SButton'
import SOnOffButton from 'components/atoms/button/SOnOffButton'
import SInput from 'components/atoms/input/SInput'
import SSelectBox from 'components/atoms/selectbox/SSelectBox'
import SCheckBox from 'components/atoms/checkbox/SCheckBox'
import SRadio from 'components/atoms/radio/SRadio'
import ItemHelper from 'components/codehelper/ItemHelper'

@inject(stores => ({
    handleChange: stores.testStore.handleChange,
    handleHelperResult: stores.testStore.handleHelperResult,
    selectedItem: stores.testStore.selectedItem,
}))
@observer
class TestSearchItem extends Component {

    render() {

        const lableArr = ['검사', '공정', 'TEST'];
        const valueArr = ['1', '2', '3'];

        return (
            <React.Fragment>

                <div className='search_line'>
                    <ItemHelper
                        selectedItemId={"selectedItem"}
                        title={'제품'}
                        handleChange={this.props.handleChange}
                        handleHelperResult={this.props.handleHelperResult}
                        cdNmIsFocus={true}
                        defaultValue={this.props.selectedItem}
                        cdName={'itemId'}
                        cdNmName={'itemNm'} />

                    <SInput title={'코드'} />
                    <SSelectBox title={'상태'} id={'select'} />

                    <SCheckBox title={'CheckBox 1'} />
                    <SCheckBox title={'CheckBox 2'} />

                    <div className='search_item_btn'>
                        <SButton buttonName={'엑셀'} type={'print'} />
                        <SButton buttonName={'조회'} type={'select'} />
                    </div>
                </div>

                <div className='search_line'>

                    <SOnOffButton title={'On/Off'} />

                    <SRadio title={'radio button'}
                        lableArr={lableArr}
                        valueArr={valueArr} />

                </div>

            </React.Fragment>
        );
    }

}

export default TestSearchItem;