import API from "components/override/api/API";

class FormRepository {

    URL = "/api/system/form";
    // 조회
    getForms(params) {
        return API.request.get(encodeURI(`${this.URL}/search?formId=${params.formId}&formNm=${params.formNm}&formType=${params.formType}`))
    }
    // 저장
    saveForms(params) {
        return API.request.post(`${this.URL}/upsert`, params)
    }
    // 삭제
    deleteForms(params) {
        return API.request.delete(encodeURI(`${this.URL}/delete?formId=${params.formId}&formNm=${params.formNm}&formType=${params.formType}&searchFormId=${params.searchFormId}`))
    }

    //버튼 조회
    getBtn(params) {
        return API.request.get(encodeURI(`${this.URL}/getBtn?formId=${params.formId}`));
    }
    //버튼 저장
    saveBtn(params) {
        return API.request.post(`${this.URL}/saveBtn`, params);
    }
    //버튼 삭제
    deleteBtn(params) {
        return API.request.delete(encodeURI(`${this.URL}/deleteBtn?btnNo=${params.btnNo}&formId=${params.formId}`));
    }
}
export default new FormRepository();