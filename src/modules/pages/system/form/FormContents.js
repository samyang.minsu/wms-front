import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.formStore.setGridApi,
    formList: stores.formStore.formList,
    getFormBtn: stores.formStore.getFormBtn
}))

class FormContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, formList, getFormBtn } = this.props;

        return (
            <SGrid
                grid={'formGrid'}
                gridApiCallBack={setGridApi}
                rowData={formList}
                cellReadOnlyColor={true}
                onSelectionChanged={getFormBtn}
            />
        );
    }
}
export default FormContents;