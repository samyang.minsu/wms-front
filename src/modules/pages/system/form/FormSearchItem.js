import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SSelectBox from 'components/atoms/selectbox/SSelectBox';

@inject(stores => ({
    formId_s: stores.formStore.formId_s,
    handleChange: stores.formStore.handleChange,
    handleSearchClick: stores.formStore.handleSearchClick,
    formType_s: stores.formStore.formType_s
}))

class FormSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { formId_s, handleChange, handleSearchClick, formType_s } = this.props;

        return (

            <React.Fragment>
                <SInput
                    title={"프로그램ID/명"}
                    id={"formId_s"}
                    value={formId_s}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                />
                <SSelectBox
                    title={"프로그램 타입"}
                    id={"formType_s"}
                    value={formType_s}
                    onChange={handleChange}
                    codeGroup={"SYS005"}
                />

                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick} />
                </div>
            </React.Fragment>
        )
    }
}

export default FormSearchItem;