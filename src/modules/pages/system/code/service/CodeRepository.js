import API from "components/override/api/API";

class CodeRepository {

    URL = "/api";

    getCodeHds(cdGubn) {
        return API.request.get(encodeURI(`${this.URL}/getcodehds?cdGubn=${cdGubn}`));
    }

    getCodes(params) {
        return API.request.get(encodeURI(`${this.URL}/getcodes?cdGubn=${params.cdGubn}&useFlag=${params.useFlag}`));
    }

    getCodesLogin(params) {
        return API.request.get(encodeURI(`${this.URL}/codes/map?cdGubn=${params.cdGubn}&useFlag=${params.useFlag}`));
    }

    saveCodeHds(params) {
        return API.request.post(encodeURI(`${this.URL}/update/codehds`), params);
    }
    saveCodes(params) {
        return API.request.post(encodeURI(`${this.URL}/update/codes`), params);
    }

    deleteCodeHds(params) {
        return API.request.post(encodeURI(`${this.URL}/delete/codehds`), params);
    }
    deleteCodes(params) {
        return API.request.post(encodeURI(`${this.URL}/delete/codes`), params);
    }
}

export default new CodeRepository();