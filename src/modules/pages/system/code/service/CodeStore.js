import { observable, action, toJS } from 'mobx';

import Alert from 'components/override/alert/Alert'
import CodeRepository from 'modules/pages/system/code/service/CodeRepository'
import GridUtil from 'components/override/grid/utils/GridUtil'
import CommonStore from 'modules/pages/common/service/CommonStore'
import BaseStore from 'utils/base/BaseStore';

class CodeStore extends BaseStore {

    @observable cdGubn = '';

    masterList = [];
    detailList = [];

    masterGridApi = undefined;
    detailGridApi = undefined;

    constructor() {
        super();
        this.setInitialState();
    }

    @action.bound
    handleChange = (data) => {
        this[data.id] = data.value;
    }

    @action.bound
    setMasterGridApi = async (gridApi) => {

        this.masterGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "마스터코드", field: "cdGubn", width: 100
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                , customCellClass: 'cell_align_center'
            },
            { headerName: "텍스트", field: "cdGubnNm", width: 300 },
            { headerName: "설명", field: "remark", width: 350 },
        ];

        this.masterGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    setDetailGridApi = async (gridApi) => {

        this.detailGridApi = gridApi;

        const selectOption = GridUtil.convertSelectMap(toJS(CommonStore.codeObject['SYS001']));

        const columnDefs = [
            { headerName: "마스터코드", field: "cdGubn", width: 75, editable: false, customCellClass: 'cell_align_center' },
            { headerName: "마스터명", field: "cdGubnNm", hide: true },
            {
                headerName: "상세코드"
                , field: "cd"
                , width: 60
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                , customCellClass: 'cell_align_center'
            },
            { headerName: "텍스트", field: "cdNm", width: 110 },
            { headerName: "Desc1", field: "desc1", width: 100 },
            { headerName: "Desc2", field: "desc2", width: 100 },
            { headerName: "Desc3", field: "desc3", width: 100 },
            { headerName: "Desc4", field: "desc4", width: 100 },
            { headerName: "Desc5", field: "desc5", width: 100 },
            { headerName: "설명", field: "remark", width: 200 },
            {
                headerName: "순서", field: "sort", width: 70
                , type: "numericColumn"
                , valueFormatter: GridUtil.numberFormatter
                , valueParser: GridUtil.numberParser
            },
            {
                headerName: "사용여부"
                , field: "useYn"
                , width: 80
                , cellClass: 'cell_align_center'
                // , valueFormatter: (params) => GridUtil.getCodeNm(params, 'SYS001')
                , cellEditor: "agSelectCellEditor"
                , cellEditorParams: { values: GridUtil.extractValues(selectOption) }
                , cellClass: 'cell_align_center'
            }
        ];

        this.detailGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    handleSearchClick = async () => {
        const { data, status } = await CodeRepository.getCodeHds(this.cdGubn);
        if (status == 200) {
            this.masterList = data;
            this.masterGridApi.setRowData(this.masterList);
            this.detailGridApi.setRowData([]);
        }
    }

    //공통코드 관리(CodeMasterContents) 코드그룹 클릭 Event
    @action.bound
    handleMasterSelectionChanged = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.masterGridApi);
        if (selectedRow) {
            this.cdGubn2 = selectedRow.cdGubn;
            if (this.cdGubn2 == '') {
                this.detailGridApi.setRowData([]);
            } else {
                await this.detailSearch();
            }
        }
    }

    //공통코드 관리(CodeMasterContents) 코드종류 리스트
    detailSearch = async () => {
        const params = { cdGubn: this.cdGubn2, useFlag: '' };
        const { data, status } = await CodeRepository.getCodes(params);
        if (status == 200) {
            console.log(data.data);
            this.detailList = data.data;
            this.detailGridApi.setRowData(this.detailList);
        }
    }

    handleMasterSaveClick = async () => {

        const saveArr = GridUtil.getGridSaveArray(this.masterGridApi);

        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        const param = JSON.stringify({ param1: saveArr, param2: this.cdGubn });

        const { data, status } = await CodeRepository.saveCodeHds(param);

        if (status == 200) {
            this.masterList = data;
            this.masterGridApi.setRowData(this.masterList);
            Alert.meg("저장되었습니다.");
        }
    }

    //공통코드 관리(CodeMasterContents) 코드그룹 추가 Btn
    handleMasterAddClick = () => {

        let count = this.masterGridApi.getDisplayedRowCount();

        const codeHdmt = {
            cdGubn: '',
            cdGubnNm: '',
            remark: '',
            useYn: 'Y',
            newItem: 'Y',
            edit: 'Y'
        };
        const result = this.masterGridApi.updateRowData({ add: [codeHdmt], addIndex: count });
        GridUtil.setFocus(this.masterGridApi, result.add[0]);
    }

    //공통코드 관리(CodeMasterContents) 코드그룹 삭제 Btn
    handleMasterDeleteClick = async () => {
        const selectedRows = this.masterGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("선택된 대상이 없습니다.");
            return;
        }
        const params = JSON.stringify({ param1: selectedRows, param2: this.cdGubn });
        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await CodeRepository.deleteCodeHds(params);
            if (status == 200) {
                this.masterList = data;
                this.masterGridApi.setRowData(this.masterList);
                this.detailGridApi.setRowData([]);
            }
        }
        else {
            return;
        }
    }

    handleDetailSaveClick = async () => {
        const saveArr = GridUtil.getGridSaveArray(this.detailGridApi);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }
        const searchData = { cdGubn: this.cdGubn2, useFlag: '' };
        const param = JSON.stringify({ param1: saveArr, param2: searchData });
        const { data, status } = await CodeRepository.saveCodes(param);
        if (status == 200) {
            this.detailList = data;
            this.detailGridApi.setRowData(this.detailList);
            Alert.meg("저장되었습니다.");
        }
    }

    handleDetailAddClick = () => {

        const selectedRow = this.masterGridApi.getSelectedRows();
        let cdGubn;

        if (selectedRow.length > 0) {
            cdGubn = selectedRow[0].cdGubn;
        } else {
            Alert.meg("공통코드를 선택해주세요.");
            return;
        }

        let sort;

        let lastIndex = this.detailGridApi.getLastDisplayedRow();

        let codeMt = {
            cdGubn: cdGubn,
            cdGubnNm: '',
            cd: '',
            cdNm: '',
            desc1: '',
            desc2: '',
            desc3: '',
            desc4: '',
            desc5: '',
            remark: '',
            sort: lastIndex + 2,
            useYn: 'Y',
            newItem: 'Y',
            edit: 'Y'
        };

        const result = this.detailGridApi.updateRowData({ add: [codeMt], addIndex: lastIndex + 1 });
        GridUtil.setFocus(this.detailGridApi, result.add[0]);
    }

    handleDetailDeleteClick = async () => {
        const selectedRows = this.detailGridApi.getSelectedRows();
        if (!selectedRows || selectedRows.length == 0) {
            Alert.meg("선택된 대상이 없습니다.");
            return;
        }
        const searchData = { cdGubn: this.cdGubn2, useFlag: '' };
        const params = JSON.stringify({ param1: selectedRows, param2: searchData });
        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
        if (value) {
            const { data, status } = await CodeRepository.deleteCodes(params);
            if (status == 200) {
                this.detailList = data;
                this.detailGridApi.setRowData(this.detailList);
            }
        }
        else {
            return;
        }
    }

}

export default new CodeStore();