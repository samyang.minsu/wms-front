import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setMasterGridApi: stores.codeStore.setMasterGridApi,
    handleMasterSelectionChanged: stores.codeStore.handleMasterSelectionChanged,
    masterList: stores.codeStore.masterList,
}))
class CodeMasterContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setMasterGridApi, handleMasterSelectionChanged, masterList } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'codeMasterGrid'}
                    gridApiCallBack={setMasterGridApi}
                    // onSelectionChanged={handleMasterSelectionChanged}
                    onCellClicked={handleMasterSelectionChanged}
                    rowData={masterList}
                    cellReadOnlyColor={true}
                />
            </div>
        );
    }
}
export default CodeMasterContents;