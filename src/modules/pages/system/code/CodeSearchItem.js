import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SInput from 'components/atoms/input/SInput'
import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
    cdGubn: stores.codeStore.cdGubn,
    handleChange: stores.codeStore.handleChange,
    handleSearchClick: stores.codeStore.handleSearchClick,
}))
class CodeSearchItem extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { cdGubn, handleChange, handleSearchClick } = this.props;
        return (
            <React.Fragment>
                <SInput
                    title={'코드'}
                    id={'cdGubn'}
                    value={cdGubn}
                    onChange={handleChange}
                    onEnterKeyDown={handleSearchClick}
                />

                <div className='search_item_btn'>
                    <SButton buttonName={'조회'} onClick={handleSearchClick} type={'btnSearch'} />
                </div>

            </React.Fragment>
        )
    }
}

export default CodeSearchItem;