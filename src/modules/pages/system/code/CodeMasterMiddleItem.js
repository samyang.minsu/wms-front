import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
    handleMasterSaveClick: stores.codeStore.handleMasterSaveClick,
    handleMasterAddClick: stores.codeStore.handleMasterAddClick,
    handleMasterDeleteClick: stores.codeStore.handleMasterDeleteClick,
}))
class CodeMasterMiddleItem extends Component {

    render() {

        const { handleMasterSaveClick, handleMasterAddClick, handleMasterDeleteClick } = this.props;
        return (
            <React.Fragment>
                <SButton
                    buttonName={'저장'}
                    type={'btnMasterSave'}
                    onClick={handleMasterSaveClick}
                />
                <SButton
                    buttonName={'추가'}
                    type={'btnMasterAdd'}
                    onClick={handleMasterAddClick}
                />
                <SButton
                    buttonName={'삭제'}
                    type={'btnMasterDelete'}
                    onClick={handleMasterDeleteClick}
                />
            </React.Fragment>
        )
    }
}

export default CodeMasterMiddleItem;