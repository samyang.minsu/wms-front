import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate';

import VersionSuperviseMiddleItem from 'modules/pages/system/versionSupervise/VersionSuperviseMiddleItem';
import VersionSuperviseContents from 'modules/pages/system/versionSupervise/VersionSuperviseContents';
import VersionSuperviseVerContents from 'modules/pages/system/versionSupervise/VersionSuperviseVerContents';

@inject(stores => ({
    initDevice : stores.versionSuperviseStore.initDevice
}))


class VersionSupervise extends Component {

    
    constructor(props) {
        super(props);
        
    }

    render() {
        const {initDevice} = this.props;
        initDevice();

        return (
            <React.Fragment>
                <ContentsMiddleTemplate contentSubTitle={'버전관리'} middleItem={<VersionSuperviseMiddleItem />} />
                <ContentsTemplate id='barcodeTracking' contentItem={
                    <div className='basic'>
                        <div style={{ width: '20%', height: '100%', float: 'left' }}>
                            <ContentsMiddleTemplate contentSubTitle={'디바이스'} />
                            <ContentsTemplate id='versionContnents' contentItem={<VersionSuperviseContents />} />
                        </div>
                        <div style={{ width: '80%', height: '100%', float: 'right' }}>
                            <ContentsMiddleTemplate contentSubTitle={'버전목록'} />
                            <ContentsTemplate id='deviceContnents' contentItem={<VersionSuperviseVerContents />}/>
                        </div>
                    </div>
                    } />
            </React.Fragment>
        )
    }
}

export default VersionSupervise;