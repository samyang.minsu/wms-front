import API from 'components/override/api/API';

class VersionSuperviseRepository {

    URL = "/api/system/versionsupervise";
 
    getLastVer() {
        return API.request.get(encodeURI(`${this.URL}/getlastver?device=WEB`))
    }

    getVer(device) {
        return API.request.get(encodeURI(`${this.URL}/getver?device=${device}`))
    }

    saveVersion(param) {
        return API.request.post(encodeURI(`${this.URL}/saveversion`), param);
    }
}

export default new VersionSuperviseRepository();