import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setVersionGridApi: stores.versionSuperviseStore.setVersionGridApi
    , versionList: stores.versionSuperviseStore.versionList
    
}))
class VersionSuperviseVerContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setVersionGridApi, versionList } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'versionGridApiGrid'}
                    gridApiCallBack={setVersionGridApi}
                    rowData={versionList}
                    cellReadOnlyColor={false}
                />
            </div>
        );
    }
}
export default VersionSuperviseVerContents;