import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
     setDeviceGridApi: stores.versionSuperviseStore.setDeviceGridApi
    ,deviceList: stores.versionSuperviseStore.deviceList
    ,handleDeviceChanged: stores.versionSuperviseStore.handleDeviceChanged
    
}))
class VersionSuperviseContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setDeviceGridApi, deviceList, handleDeviceChanged} = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'deviceGridApiuGrid'}
                    gridApiCallBack={setDeviceGridApi}
                    onSelectionChanged={handleDeviceChanged}
                    rowData={deviceList}
                    cellReadOnlyColor={true}
                />
            </div>
        );
    }
}
export default VersionSuperviseContents;