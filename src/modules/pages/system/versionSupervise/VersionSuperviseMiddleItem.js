import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton'

@inject(stores => ({
      saveClick: stores.versionSuperviseStore.saveClick
    , searchClick: stores.versionSuperviseStore.searchClick
    , addClick: stores.versionSuperviseStore.addClick,
}))
class VersionSuperviseMiddleItem extends Component {

    render() {

        const { saveClick, searchClick, addClick} = this.props;
        return (
            <React.Fragment>
                <SButton
                    buttonName={'추가'}
                    type={'btnAdd'}
                    onClick={addClick}
                />
                <SButton
                    buttonName={'저장'}
                    type={'btnSave'}
                    onClick={saveClick}
                />
                <SButton
                    buttonName={'조회'}
                    type={'btnSearch'}
                    onClick={searchClick}
                />
            </React.Fragment>
        )
    }
}

export default VersionSuperviseMiddleItem;