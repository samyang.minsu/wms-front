import React, { Component } from 'react';
import { inject } from 'mobx-react';
// import loading from 'style/img/loading.gif';

@inject(stores => ({
    getSSOTicket: stores.loginStore.getSSOTicket,
}))
class Sso extends Component {

    constructor(props) {
        super(props);
    }


    async componentDidMount() {
        const { getSSOTicket } = this.props;
        await getSSOTicket(this);
    }

    render() {

        return (
            <div>
                <div className="wrap_auto_img">
                    {/* <img src={loading} /> */}
                </div>
                <div className="wrap_auto_txt">
                    자동 로그인 진행중입니다.
                </div>
            </div>
        );
    }
}

export default Sso;