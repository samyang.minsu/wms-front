import API from "components/override/api/API";

class LoginRepository {

    URL_AUTH = "/auth";
    URL = "/api";

    loginUserCheck(params) {
        const param = JSON.stringify(params);
        return API.request.post(`${this.URL_AUTH}/login`, param);
    }

    getLoginUserInfo(loginId) {
        return API.request.get(`${this.URL}/system/user/${loginId}`);            
    }

    // getSSOTicket() {
    //     return API.request.get(`${this.URL_AUTH}/sso/ticket`);            
    // }

    // getSSOUserInfo(params) {
    //     return API.request.get(`${this.URL_AUTH}/sso/userinfo?ticket=${params}`);            
    // }

    login(params) {
        return API.request.post(`/auth/login`, params)
    }

    logout(loginId) {
        return API.request.get(encodeURI(`/auth/logout?loginId=${loginId}`))
    }
}
export default new LoginRepository();