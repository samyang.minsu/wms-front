import { observable, action } from 'mobx';

import LoginRepository from "modules/pages/login/service/LoginRepository"
import CommonStore from 'modules/pages/common/service/CommonStore';
import Alert from 'components/override/alert/Alert';

class LoginStore {

    constructor(root) {
        this.root = root;
    }

    @observable loginId = '';
    @observable loginPass = '';

    isSSO = false;

    @action.bound
    handleChange(event) {
        this[event.target.id] = event.target.value;
    }

    // @action.bound
    // async getSSOTicket(params) {

    //     if (this.isSSO) {

    //         const { data, status } = await LoginRepository.getSSOTicket();

    //         if (status == 200) {
    //             document.getElementById('ssoUpdatePage').src = `http://contract.samyang.com:9696/SetInfo.aspx?TicketID=${data.ticket}`;
    //             const response = await LoginRepository.getSSOUserInfo(data.ticket);

    //             if (response.data.success == true) {
    //                 await this.loginAfter(response.data.data);
    //                 this.getCode();
    //                 params.props.history.push("/app");
    //             } 
    //             else {
    //                 this.isSSO = false;
    //                 params.props.history.push("/auth/login");
    //             }
    //         }
    //     } else {
    //         params.props.history.push("/auth/login");
    //     }
    // }

    @action.bound
    async userLogin(params) {

        if (!this.loginId) {
            Alert.meg('ID/PW 를 입력해 주세요.');
            return;
        }

        const loginData = {
            loginId: this.loginId,
            loginPass: this.loginPass
        };

        const result = await LoginRepository.login(loginData);
        
        if (result.data.success) {
            this.getCode();
            await this.loginAfter(result.data.data);
            params.props.history.push("/app");
        } else {
            this.loginPass = '';
            Alert.meg('사용자 정보를 다시 입력해 주세요.');
        }
    }

    getCode() {
        // 바로 사용하는 것이 아니기 때문에 비동기처리
        CommonStore.requestGetCodes();
        CommonStore.getBuList();
    }

    loginAfter(loginInfo) {

        CommonStore.setToken(loginInfo.token); // 로컬스토리지에 로그인 정보 저장
        CommonStore.setLoginId(loginInfo.loginId);
        CommonStore.setLoginNm(encodeURIComponent(loginInfo.loginNm));
        CommonStore.setBuId(loginInfo.buId);

        CommonStore.setUserInfo(loginInfo);
    }

    @action.bound
    async logout() {
        this.isSSO = false;
        const data = await LoginRepository.logout(CommonStore.loginId);
        CommonStore.clearLocalStorage();
        return Promise.resolve();
    }
}

export default new LoginStore();