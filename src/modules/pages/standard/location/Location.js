import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate';

import STree from 'components/override/tree/STree';

import LocationMiddleItem from 'modules/pages/standard/location/LocationMiddleItem';
import LocationContents from 'modules/pages/standard/location/LocationContents';
import LocationSearchItem from 'modules/pages/standard/location/LocationSearchItem';


@inject(stores => ({
    handleSelect: stores.locationStore.handleSelect
    , handleCheck: stores.locationStore.handleCheck
    , treeData: stores.locationStore.treeData
    
}))

class Location extends Component {

    constructor(props) {
        super(props);

        // BuStore.getBu();
    }

    render() {

        const { handleSelect
                , handleCheck
                , treeData } = this.props;

        return (
            <React.Fragment>
                
                <div style={{ width: '100%', height: '100%', float: 'left' }}>
                    <div style={{ width: '25%', height: '100%', float: 'left' }}>
                        <ContentsMiddleTemplate contentSubTitle={'위치정보 관리'} middleItem={<LocationSearchItem />}/>
                        <ContentsTemplate id='locationTree'  contentItem = {
                            <STree
                                keyMember={"locId"}
                                pIdMember={"plocId"}
                                titleMember={"locNm"}
                                checkable={false}
                                onSelect={handleSelect}
                                onCheck={handleCheck}
                                treeData={treeData}
                            />
                        }/>
                        
                    </div>
                    
                    <div style={{ width: '74%', height: '100%', float: 'right' }}>
                        <ContentsMiddleTemplate middleItem={<LocationMiddleItem />} />
                        <ContentsTemplate id="locationGrid" contentItem={<LocationContents />} />
                    </div>

                </div>


            </React.Fragment>
        )
    }
}

export default Location;