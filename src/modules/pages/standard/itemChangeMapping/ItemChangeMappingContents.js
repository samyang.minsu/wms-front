import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid';

@inject(stores => ({
	setItemChangeMappingContentsGridApi: stores.itemChangeMappingStore.setItemChangeMappingContentsGridApi
	, itemChangeMappingContentsList: stores.itemChangeMappingStore.itemChangeMappingContentsList
	, rowDataHandleChange: stores.itemChangeMappingStore.rowDataHandleChange
}))

class ItemChangeMappingContents extends Component {

	constructor(props) {
		super(props);
	}

	render() {

		const {
			setItemChangeMappingContentsGridApi
			, itemChangeMappingContentsList
			, rowDataHandleChange
		} = this.props;

		return (
			<SGrid
				grid={'itemChangeMappingGrid'}
				gridApiCallBack={setItemChangeMappingContentsGridApi}
				rowData={itemChangeMappingContentsList}
				onCellClicked={rowDataHandleChange}
				suppressRowClickSelection={true}
				cellReadOnlyColor={true}
				editable={false}
			/>
		);
	}
}
export default ItemChangeMappingContents;