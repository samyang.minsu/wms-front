import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import STab from 'components/override/tab/STab';

import ItemChangeMappingStore from "modules/pages/standard/itemChangeMapping/service/ItemChangeMappingStore";
import SModal from "components/override/modal/SModal";
import SInput from 'components/atoms/input/SInput';
import SButton from 'components/atoms/button/SButton';
import SGrid from 'components/override/grid/SGrid';

import ItemChangeMappingSubModal from "modules/pages/standard/itemChangeMapping/itemchangemappingmodal/ItemChangeMappingSubModal"

@inject(stores => ({
	btnItemChangeMappingSelect: stores.itemChangeMappingStore.btnItemChangeMappingSelect
	, btnItemChangeMappingDcItemSearch: stores.itemChangeMappingStore.btnItemChangeMappingDcItemSearch
	, itemChangeMappingDcItemCdNm: stores.itemChangeMappingStore.itemChangeMappingDcItemCdNm
	, setItemChangeMappingModalDcItemGridApi: stores.itemChangeMappingStore.setItemChangeMappingModalDcItemGridApi
	, itemChangeMappingModalDcItemList: stores.itemChangeMappingStore.itemChangeMappingModalDcItemList
	, btnItemChangeMappingLocalItemSearch: stores.itemChangeMappingStore.btnItemChangeMappingLocalItemSearch
	, itemChangeMappingLocalItemCdNm: stores.itemChangeMappingStore.itemChangeMappingLocalItemCdNm
	, setItemChangeMappingModalLocalItemGridApi: stores.itemChangeMappingStore.setItemChangeMappingModalLocalItemGridApi
	, itemChangeMappingModalLocalItemList: stores.itemChangeMappingStore.itemChangeMappingModalLocalItemList
	, handleChange: stores.itemChangeMappingStore.handleChange
	, itemChangeMappingInsertItemModal: stores.itemChangeMappingStore.itemChangeMappingInsertItemModal
	, itemChangeMappingInsertItemModalIsClose: stores.itemChangeMappingStore.itemChangeMappingInsertItemModalIsClose
}))

//제품 UOM 구분별 조회
export default class ItemChangeMappingModal extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const {
			btnItemChangeMappingSelect
			, btnItemChangeMappingDcItemSearch
			, itemChangeMappingDcItemCdNm
			, setItemChangeMappingModalDcItemGridApi
			, itemChangeMappingModalDcItemList
			, btnItemChangeMappingLocalItemSearch
			, itemChangeMappingLocalItemCdNm
			, setItemChangeMappingModalLocalItemGridApi
			, itemChangeMappingModalLocalItemList
			, handleChange
			, itemChangeMappingInsertItemModal
			, itemChangeMappingInsertItemModalIsClose
		} = this.props;

		return (
			<React.Fragment>
				<div style={{ width: '100%', height: '10%' }}>
					<SButton
						buttonName={'추가'}
						type={'btnItemChangeMappingSelect'}
						onClick={btnItemChangeMappingSelect}
					/>
				</div>
				<div style={{ width: '49%', height: '90%', float: 'left' }}>
					<ContentsMiddleTemplate contentSubTitle={'내수당'} middleItem={
						<ItemChangeMappingModalDcItemMiddleItem
							btnItemChangeMappingDcItemSearch={btnItemChangeMappingDcItemSearch}
							itemChangeMappingDcItemCdNm={itemChangeMappingDcItemCdNm}
							handleChange={handleChange}
						/>} />
					<ContentsTemplate id='itemChangeMappingModal_contents' contentItem={
						<ItemChangeMappingModalDcItemContentItem
							setItemChangeMappingModalDcItemGridApi={setItemChangeMappingModalDcItemGridApi}
							itemChangeMappingModalDcItemList={itemChangeMappingModalDcItemList}
						/>} />
				</div>
				<div style={{ width: '49%', height: '90%', float: 'right' }}>
					<ContentsMiddleTemplate contentSubTitle={'로컬'} middleItem={
						<ItemChangeMappingModalLocalItemMiddleItem
							btnItemChangeMappingLocalItemSearch={btnItemChangeMappingLocalItemSearch}
							itemChangeMappingLocalItemCdNm={itemChangeMappingLocalItemCdNm}
							handleChange={handleChange}
						/>} />
					<ContentsTemplate id='itemChangeMappingModal_contents' contentItem={
						<ItemChangeMappingModalLocalItemContentItem
							setItemChangeMappingModalLocalItemGridApi={setItemChangeMappingModalLocalItemGridApi}
							itemChangeMappingModalLocalItemList={itemChangeMappingModalLocalItemList}
						/>} />
				</div>
				<SModal
					formId={'standard_item_change_mapping_insert_p'}
					id={'itemChangeMappingInsertModal'}
					isOpen={itemChangeMappingInsertItemModal}
					onRequestClose={itemChangeMappingInsertItemModalIsClose}
					contentLabel="내수/로컬 맵핑 추가"
					width={650}
					height={200}
					contents={<ItemChangeMappingSubModal />}
				/>
			</React.Fragment>
		);
	}
}
const ItemChangeMappingModalDcItemMiddleItem = ({
	btnItemChangeMappingDcItemSearch
	, itemChangeMappingDcItemCdNm
	, handleChange
}) => {
	return (
		<React.Fragment>
			<div style={{ display: "flex" }}>
				<SInput
					title={'제품코드/명'}
					id={"itemChangeMappingDcItemCdNm"}
					value={itemChangeMappingDcItemCdNm}
					onChange={handleChange}
					onEnterKeyDown={btnItemChangeMappingDcItemSearch}
				/>
				<SButton
					className='btn_red'
					buttonName={'조회'}
					type={'btnItemChangeMappingDcItemSearch'}
					onClick={btnItemChangeMappingDcItemSearch}
				/>
			</div>
		</React.Fragment>
	)
}

const ItemChangeMappingModalDcItemContentItem = ({
	setItemChangeMappingModalDcItemGridApi
	, itemChangeMappingModalDcItemList }) => {
	return (
		<SGrid
			grid={'itemChangeMappingModalDcitemGridApi'}
			gridApiCallBack={setItemChangeMappingModalDcItemGridApi}
			rowData={itemChangeMappingModalDcItemList}
			cellReadOnlyColor={true}
			editable={false}
		/>
	)
}

const ItemChangeMappingModalLocalItemMiddleItem = ({
	btnItemChangeMappingLocalItemSearch
	, itemChangeMappingLocalItemCdNm
	, handleChange
}) => {
	return (
		<React.Fragment>
			<div style={{ display: "flex" }}>
				<SInput
					title={'제품코드/명'}
					id={"itemChangeMappingLocalItemCdNm"}
					value={itemChangeMappingLocalItemCdNm}
					onChange={handleChange}
					onEnterKeyDown={btnItemChangeMappingLocalItemSearch}
				/>
				<SButton
					className='btn_red'
					buttonName={'조회'}
					type={'btnItemChangeMappingLocalItemSearch'}
					onClick={btnItemChangeMappingLocalItemSearch}
				/>
			</div>
		</React.Fragment>
	)
}

const ItemChangeMappingModalLocalItemContentItem = ({
	setItemChangeMappingModalLocalItemGridApi
	, itemChangeMappingModalLocalItemList }) => {
	return (
		<SGrid
			grid={'itemChangeMappingModalLocalitemGridApi'}
			gridApiCallBack={setItemChangeMappingModalLocalItemGridApi}
			rowData={itemChangeMappingModalLocalItemList}
			cellReadOnlyColor={true}
			editable={false}
		/>
	)
}