import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import STab from 'components/override/tab/STab';

import ItemChangeMappingSearchItem from "modules/pages/standard/itemChangeMapping/ItemChangeMappingSearchItem";
import ItemChangeMappingMiddleItem from "modules/pages/standard/itemChangeMapping/ItemChangeMappingMiddleItem";
import ItemChangeMappingContents from "modules/pages/standard/itemChangeMapping/ItemChangeMappingContents";
import ItemChangeMappingStore from "modules/pages/standard/itemChangeMapping/service/ItemChangeMappingStore";
import ItemChangeMappingModal from "modules/pages/standard/itemChangeMapping/itemchangemappingmodal/ItemChangeMappingModal"
import SModal from "components/override/modal/SModal";

@inject(stores => ({
	itemChangeMappingSelectItemModal: stores.itemChangeMappingStore.itemChangeMappingSelectItemModal
	, itemChangeMappingSelectItemModalIsClose: stores.itemChangeMappingStore.itemChangeMappingSelectItemModalIsClose
}))

//제품 UOM 구분별 조회
class ItemChangeMapping extends Component {

	constructor(props) {
		super(props);
	}

	async componentWillMount() {
		if(ItemChangeMappingStore.itemChangeMappingSearchItemBuIdCheck == false) {
			await ItemChangeMappingStore.getBuId();
		}
	}

	render() {
		const { 
			itemChangeMappingSelectItemModal
			,itemChangeMappingSelectItemModalIsClose
			} = this.props;

		return (
			<React.Fragment>
				<div style={{ width: '100%', height: '100%' }}>
					<SearchTemplate searchItem={<ItemChangeMappingSearchItem />} />
					<ContentsMiddleTemplate contentSubTitle={'내수/로컬 제품 관리'} middleItem={<ItemChangeMappingMiddleItem />}/>
					<ContentsTemplate id='itemChangeMapping_contents' contentItem={<ItemChangeMappingContents />} />
				</div>

				<SModal
					formId={"standard_item_change_mapping_select_p"}
					id={'itemChangeMappingSelectModal'}
					isOpen={itemChangeMappingSelectItemModal}
					onRequestClose={itemChangeMappingSelectItemModalIsClose}
					contentLabel="내수/로컬 맵핑 선택"
					width={800}
					height={500}
					contents={<ItemChangeMappingModal/>}
				/>
			</React.Fragment>
		)
	}
}

export default ItemChangeMapping;