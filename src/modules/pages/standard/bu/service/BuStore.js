import { observable, action} from 'mobx';
import BaseStore from 'utils/base/BaseStore';

import CommonRepository from 'modules/pages/common/service/CommonRepository';
import GridUtil from 'components/override/grid/utils/GridUtil';

import BuRepository from 'modules/pages/standard/bu/service/BuRepository';
import Alert from 'components/override/alert/Alert';


class BuStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    buGridApi = undefined;
    buList = [];

    //공장 조회
    @action.bound
    getBu = async () => {
        const params = {
            buId: '',
            useYn: ''
        }

        const { data, status } = await CommonRepository.getBu(params);
        if(status == 200) {
            this.buList = data.data;
            this.buGridApi.setRowData(this.buList);
        }
    }

    //공장 추가
    @action.bound
    addClick = () => {
        
        let count = this.buGridApi.getDisplayedRowCount();

        const buMt = {
            buId: '',
            buNm: '',
            addr: '',
            tel: '',
            sort: count+1,
            useYn: 'Y',
            useFlag: 'Y',
            newItem: 'Y',
            edit: 'Y'
        };
        
        const result = this.buGridApi.updateRowData({ add: [buMt], addIndex: count });
        GridUtil.setFocus(this.buGridApi, result.add[0]);
    }

    //공장 저장
    @action.bound
    saveClick = async () => {
        //중복체크 변수
        let checkList = [];
        let checkCnt = 0;
        //필수입력 체크 변수
        let cntBu = 0;
        let cntBuNm = 0;

        const saveArr = GridUtil.getGridSaveArray(this.buGridApi);
        
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }
        
        this.buGridApi.forEachNode(rowNode => {
            checkList.push(rowNode.data.buId);
            if(rowNode.data.buId.length < 1) {
                cntBu++
                return;
            } else if (rowNode.data.buNm.length < 1) {
                cntBuNm++
                return;
            }
        })
        
        if(cntBu > 0) {
            Alert.meg("공장ID가 입력되지 않았습니다.");
            return;
        } else if (cntBuNm > 0) {
            Alert.meg("공장명이 입력되지 않았습니다.");
            return;
        }

        for(let i = 0; i < checkList.length; i++) {
            for(let j = 0; j < i; j++) {
                if(checkList[i] == checkList[j]) {
                    checkCnt++
                    break;
                }
            }
        }

        if(checkCnt > 0) {
            Alert.meg("공장ID가 중복됬습니다.")
            return;
        }
        
        const searchData = {
            "buId": ''
            ,"useYn": ''
        }

        const param = JSON.stringify({ param1: saveArr, param2: searchData });
        const { data, status } = await BuRepository.saveBu(param);

        if(status == 200) {
            this.buList = data.data;
            this.buGridApi.setRowData(this.buList);
            // Alert.meg(data.msg);
            Alert.meg("저장되었습니다.");
        }
        
    }

    /*공장 삭제
      - 데이터를 삭제하는게 아니라 추가로 된 row만 삭제한다.  */
    @action.bound
    deleteClick = async () => {

        const selectedRow = GridUtil.getSelectedRowData(this.buGridApi);
        
        if(selectedRow) {
            if(selectedRow.newItem == 'Y') {
                this.buGridApi.updateRowData({ remove: [selectedRow] });
                return;
            }
        } else return;
    }
    
    // 공장그리드
    @action.bound
    setBuGridApi = async (gridApi) => {

        this.buGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "공장ID", field: "buId", width: 100
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                , customCellClass: 'cell_align_center'
            },
            { 
                headerName: "공장명"
                , field: "buNm"
                , width: 150
                , cellClass: 'cell_align_left'
            },
            { 
                headerName: "주소"
                , field: "addr"
                , width: 200
                , cellClass: 'cell_align_left'
            },
            { 
                headerName: "전화번호"
                , field: "tel"
                , width: 120
                , cellClass: 'cell_align_left'
            },
            { 
                headerName: "사용여부"
                , field: "useYn"
                , width: 80
                , cellClass: 'cell_align_center'
            }
            
        ];

        GridUtil.findColumnDef(columnDefs, 'buId');
        
        GridUtil.setSelectCellByCode(columnDefs[4], "SYS001"); 
        this.buGridApi.setColumnDefs(columnDefs);
    }
    
}

export default new BuStore();