import API from 'components/override/api/API';

class BuRepository {
    
  
    URL = "/api";
    
    //저장
    saveBu(params) {
        return API.request.post(encodeURI(`${this.URL}/upsertbu`), params);
    }
    
    //삭제
    deleteBu(params) {
        console.log(params.loginId);
        return API.request.delete(encodeURI(`${this.URL}/deletebu?buId=${params.buId}`))
    }
}

export default new BuRepository();