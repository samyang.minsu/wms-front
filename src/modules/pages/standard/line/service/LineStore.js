import React from 'react';
import { observable, action } from 'mobx';
import BaseStore from 'utils/base/BaseStore';

import ico_search from 'style/img/ico_search.png';

import SIconButton from 'components/atoms/button/SIconButton'
import CommonRepository from 'modules/pages/common/service/CommonRepository';
import GridUtil from 'components/override/grid/utils/GridUtil';

import Alert from 'components/override/alert/Alert';
import CommonStore from 'modules/pages/common/service/CommonStore';
import LineRepository from 'modules/pages/standard/line/service/LineRepository';

class LineStore extends BaseStore {

    constructor() {
        super();
        this.setInitialState();
    }


    @observable buId = CommonStore.buId; // 공장
    @observable lineGridApi = undefined; // 라인관리 Grid
    @observable lineList = []; // 라인관리 Grid List

    // 위치Modal 변수
    @observable isLocationOpen = false;
    @observable locationBuId = '';

    // 유저(외주)Modal 변수
    @observable isEmpOpen = false;
    @observable temp = [];


    // ----------------[EmployeeMultiHelper 대용 LineEmployeeModal 관련 변수]-----------------------
    @observable buList = CommonStore.buList; // 공장 분류
    @observable employee = ''; // 사용자(사번or이름)
    @observable empGridApi = undefined; // 사용자 추가 Modal Grid
    @observable employeeList = []; // 사용자 Modal Grid List

    // 사용자 추가 Modal 열기
    @action.bound
    getEmp = async () => {
        const data = GridUtil.getSelectedRowData(this.lineGridApi);
        if (data.outsourcingYn == 'Y') {
            this.isEmpOpen = true;
        } else {
            return;
        }
    }

    // 사용자 추가 Modal 닫기
    getEmpModalIsClose = async () => {
        this.employee = '';
        this.employeeList = [];
        this.isEmpOpen = false;
    }

    // 사용자 추가 Modal 조회
    @action.bound
    modalEmployeeSearch = async () => {

        const searchData = {
            'buId': this.buId
            , 'empNo': this.employee
            , 'useYN': 'Y'
            , 'outsourcingYn': 'Y'
        }

        const { data, status } = await LineRepository.getEmployee(searchData);
        if (data.success == false) {
            Alert.meg(data.msg);
        } else {
            this.employeeList = data.data;
            this.empGridApi.setRowData(this.employeeList);
        }
    }

    // 사용자 추가 Modal 선택
    @action.bound
    modalEmployeeSelect = async () => {
        const selectedData = GridUtil.getSelectedRowData(this.empGridApi);
        console.log(selectedData)
        if (selectedData != null) {
            let data = GridUtil.getSelectedRowData(this.lineGridApi);
            data.edit = "Y";
            data.outsourcingLoginNo = selectedData.empNo;
            data.outsourcingLoginNm = selectedData.loginNm;
            GridUtil.updateGridOneRow(this.lineGridApi, data);
            this.isEmpOpen = false;
        } else {
            // Alert.meg('현장대리인을 선택 해주세요.')
            // return;
        }
    }

    // 사용자 추가 Modal 그리드 정의
    @action.bound
    setEmpGridApi = async (gridApi) => {
        this.empGridApi = gridApi;
        const columnDefs = [
            { headerName: "로그인ID", field: "loginId", width: 100, cellClass: 'cell_align_center' },
            { headerName: "사번", field: "empNo", width: 90, cellClass: 'cell_align_center' },
            { headerName: "이름", field: "loginNm", width: 140, cellClass: 'cell_align_center' },
            { headerName: "부서", field: "deptNm", width: 100 },
            { headerName: "이메일", field: "loginEmail", width: 200 },
            { headerName: "전화번호", field: "loginTel", width: 120 },
            { headerName: "권한그룹", field: "grpList", width: 120 }
        ];

        this.empGridApi.setColumnDefs(columnDefs);
    }
    // --------------------------------------------------------------------------------------------


    //공장 조회
    @action.bound
    getBu = async () => {
        this.buId = CommonStore.buId;
    }

    //바뀐 데이터 확인
    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }

    //라인관리 조회
    @action.bound
    handleSearchClick = async () => {
        const searchData = {
            "buId": this.buId
        }
        const { data, status } = await LineRepository.getLine(searchData);
        if (status == 200) {
            this.lineList = data.data;
            this.lineGridApi.setRowData(this.lineList);
        }
    }

    //라인관리 추가
    @action.bound
    addClick = () => {

        let count = this.lineGridApi.getDisplayedRowCount();
        const line = {
            buId: '',
            lineId: '',
            lineNm: '',
            lineShortNm: '',
            defaultLocId: '',
            autoYn: 'Y',
            autoCd: '',
            autoCount: '',
            outsourcingYn: 'Y',
            outsourcingLoginNm: '',
            useYn: 'Y',
            useFlag: 'Y',
            newItem: 'Y',
            edit: 'Y',
            index: count
        };
        const result = this.lineGridApi.updateRowData({ add: [line], addIndex: count });
        GridUtil.setFocus(this.lineGridApi, result.add[0]);
    }

    //저장
    @action.bound
    saveClick = async () => {
        //중복체크 변수
        let checkList = [];
        //필수입력 체크 변수
        let cntBu = 0;
        let cntLineNm = 0;

        const saveArr = GridUtil.getGridSaveArray(this.lineGridApi);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }

        this.lineGridApi.forEachNode(rowNode => {
            if (rowNode.data.buId.length < 1) {
                cntBu++
                return;
            } else if (rowNode.data.lineNm.length < 1) {
                cntLineNm++
                return;
            }
        })

        if (cntBu > 0) {
            Alert.meg("공장이 입력되지 않았습니다.");
            return;
        } else if (cntLineNm > 0) {
            Alert.meg("라인명이 입력되지 않았습니다.");
            return;
        }


        let check = [];
        const searchData = {
            "buId": ''
        }

        let data = {};
        data = await LineRepository.getLine(searchData); // TB_LINE_MT에 등록된 LINE_ID 가져옴.
        if (data.status == 200) {
            check = data.data.data;
        }

        let dupCheck;
        saveArr.forEach(dup => {
            if (dup.newItem == 'Y') {
                dupCheck = check.filter(x => x.lineId == dup.lineId)
                return dupCheck;
            }
        })

        if (dupCheck) {
            if (dupCheck.length > 0) {
                Alert.meg("[" + dupCheck[0].lineId + "]사용중인 라인ID입니다.");
                return;
            }
        }

        const param = JSON.stringify({ param: saveArr });

        data = await LineRepository.saveLine(param);
        // const { data, status } = await LineRepository.saveLine(param);

        if (data.status == 200) {
            this.lineList = data.data.data;
            this.lineList.forEach(message => {
                if (message.sqlMessage != null) {
                    Alert.meg(message.sqlMessage);
                    return;
                } else {
                    Alert.meg("저장되었습니다.");
                    this.handleSearchClick();
                    return;
                }
            })
            // this.lineGridApi.setRowData(this.lineList);
            // Alert.meg("저장되었습니다.");
            // Alert.meg(data.msg);
        }
    }

    //삭제
    @action.bound
    deleteClick = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.lineGridApi);

        if (selectedRow) {
            if (selectedRow.newItem == 'Y') {
                this.lineGridApi.updateRowData({ remove: [selectedRow] });
                return;
            } else {
                const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
                if (value) {
                    const param = JSON.stringify({ param: selectedRow });
                    const { data, status } = await LineRepository.deleteLine(param);
                    if (status == 200) {
                        this.lineGridApi.updateRowData({ remove: [selectedRow] });
                        Alert.meg("삭제되었습니다.");
                    }
                } else return;
            }
        } else {
            Alert.meg("선택된 라인ID가 없습니다.");
        }
    }

    //위치변경Modal 활성
    getLocation = async () => {
        this.isLocationOpen = true;
    }

    //위치변경 선택시
    locationHelperResult = (result) => {
        if (result) {
            let data = GridUtil.getSelectedRowData(this.lineGridApi);
            data.edit = "Y";
            data.defaultLocId = result.locId;
            data.defaultLocNm = result.locNm;

            GridUtil.updateGridOneRow(this.lineGridApi, data);
        } else
            this.isLocationOpen = false;
    }

    //사원 선택시
    @action.bound
    lotHelperResult = (result) => {
        if (result) {
            let data = GridUtil.getSelectedRowData(this.lineGridApi);
            data.edit = "Y";
            data.outsourcingLoginNo = result[0].empNo;
            data.outsourcingLoginNm = result[0].loginNm;
            GridUtil.updateGridOneRow(this.lineGridApi, data);
        } else
            this.isEmpOpen = false;
    }


    // @action.bound
    // onChange = (value) => {
    //     console.log('value', value)
    //     let change = GridUtil.getSelectedRowData(this.lineGridApi);

    //     if (value.data.autoYn == 'N') {
    //         // const change = GridUtil.getSelectedRowData(this.lineGridApi);
    //         change.autoCd = '';
    //         change.autoGroupCds = '';
    //         change.autoCount = 0;
    //         GridUtil.updateGridOneRow(this.lineGridApi, change);
    //     }

    //     if (value.data.outsourcingYn == 'N') {
    //         change.outsourcingLoginNm = '';
    //         change.outsourcingLoginNo = '';
    //         GridUtil.updateGridOneRow(this.lineGridApi, change);
    //     }
    // }


    // 공장그리드
    @action.bound
    setLineGridApi = async (gridApi) => {

        this.lineGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "공장"
                , children: [
                    {
                        headerName: ""
                        , field: "buId"
                        , width: 80
                        , editable: function (params) {
                            if (params.node.data.newItem == 'Y') {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                        , customCellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "라인ID"
                , children: [
                    {
                        headerName: ""
                        , field: "lineId"
                        , width: 70
                        , editable: function (params) {
                            if (params.node.data.newItem == 'Y') {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                        , customCellClass: 'cell_align_center'
                    }
                ]
            },
            {
                headerName: "라인명"
                , children: [{
                    headerName: ""
                    , field: "lineNm"
                    , width: 120
                    , cellClass: 'cell_align_left'
                }]
            },
            {
                headerName: "라인단축명"
                , children: [{
                    headerName: ""
                    , field: "lineShortNm"
                    , width: 120
                    , cellClass: 'cell_align_left'
                }]
            },
            {
                headerName: "기본위치"
                , children: [
                    {
                        headerName: ""
                        , field: "defaultLocNm"
                        , width: 90
                        , cellClass: 'cell_align_left'
                        , editable: false
                    },
                    {
                        headerName: ""
                        , field: "findLocNm"
                        , width: 55
                        , cellClass: 'cell_align_center'
                        , editable: false
                        // , onCellClicked: this.getLocation
                        , cellRenderer: 'rowCellButtonRender'
                        , cellRendererParams: { buttonName: '변경', onClick: this.getLocation }
                    }
                ]
            },
            {
                headerName: "오토라벨러 여부"
                , children: [{
                    headerName: ""
                    , field: "autoYn"
                    , width: 105
                    , cellClass: 'cell_align_center'
                }]
            },
            {
                headerName: "오토라벨러 코드"
                , children: [{
                    headerName: ""
                    , field: "autoCd"
                    , width: 105
                    , cellClass: 'cell_align_center'
                    , editable: function (params) {
                        if (params.node.data.autoYn == 'Y') {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    , onCellValueChanged: function(param) {
                        if (param.node.data.autoYn == 'N') {
                            param.node.data.autoCd = '';
                            GridUtil.updateGridOneRow(param.api, param.node.data);
                        }
                    }
                }]
            },
            {
                headerName: "오토라벨러 그룹코드"
                , children: [{
                    headerName: ""
                    , field: "autoGroupCds"
                    , width: 120
                    , cellClass: 'cell_align_center'
                    , editable: function (params) {
                        if (params.node.data.autoYn == 'Y') {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    , onCellValueChanged: function(param) {
                        if (param.node.data.autoYn == 'N') {
                            param.node.data.autoGroupCds = '';
                            GridUtil.updateGridOneRow(param.api, param.node.data);
                        }
                    }
                }]
            },
            {
                headerName: "오토라벨러 개수"
                , children: [{
                    headerName: ""
                    , field: "autoCount"
                    , width: 105
                    , editable: function (params) {
                        if (params.node.data.autoYn == 'Y') {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    , cellClass: 'cell_align_right'
                    , onCellValueChanged: function(param) {
                        if (param.node.data.autoYn == 'N') {
                            param.node.data.autoCount = 0;
                            GridUtil.updateGridOneRow(param.api, param.node.data);
                        }
                    }
                }]
            },
            {
                headerName: "외주여부"
                , children: [{
                    headerName: ""
                    , field: "outsourcingYn"
                    , width: 80
                    , cellClass: 'cell_align_center'
                    // , onCellValueChanged: this.onChange
                }]
            },
            {
                headerName: "외주 현장대리인"
                , children: [
                    {
                        headerName: ""
                        , field: "outsourcingLoginNm"
                        , width: 75
                        , cellClass: 'cell_align_center'
                        , editable: false
                    },
                    {
                        headerName: ""
                        , field: "findOsLoginNm"
                        , width: 55
                        , cellClass: 'cell_align_center'
                        , editable: function (params) {
                            if (params.node.data.outsourcingYn == 'Y') {
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                        // , onCellClicked: this.getEmp
                        // <SIconButton onclick={() => this.handleOsSearch(item)}/>
                        , cellRenderer: 'rowCellButtonRender'
                        // , cellRendererParams : {className: 'btn_ico', onClick :this.getEmp}
                        , cellRendererParams: { buttonName: '찾기', onClick: this.getEmp }
                        , cellClassRules: {
                            'cellOutSourcingYn_Y': function (params) {
                                return params.node.data.outsourcingYn === 'Y'
                            },
                            'cellOutSourcingYn_N': function (params) {
                                return params.node.data.outsourcingYn === 'N'
                            }
                        }
                        , onCellValueChanged: function(param) {
                            if (param.node.data.outsourcingYn == 'N') {
                                param.node.data.outsourcingLoginNm = '';
                                param.node.data.outsourcingLoginNo = '';
                                GridUtil.updateGridOneRow(param.api, param.node.data);
                            }
                        }
                    }
                ]
            }
        ];
        this.lineGridApi.setColumnDefs(columnDefs);
        // GridUtil.setSelectCellByList(columnDefs[0], CommonStore.buList, "buId", "buNm");

        const selected_Yn = [
            { cd: 'Y' },
            { cd: 'N' }
        ];

        const col_a = GridUtil.findColumnDef(columnDefs, 'autoYn');
        const col_b = GridUtil.findColumnDef(columnDefs, 'outsourcingYn');
        GridUtil.setSelectCellByList(col_a, selected_Yn, 'cd', 'cd');
        GridUtil.setSelectCellByList(col_b, selected_Yn, 'cd', 'cd');
        this.lineGridApi.setColumnDefs(columnDefs);
    }

    // 공장그리드(기존)
    // @action.bound
    // setLineGridApi = async (gridApi) => {

    //     this.lineGridApi = gridApi;

    //     const columnDefs = [
    //         {
    //             headerName: "공장"
    //             , field: "buId"
    //             , width: 80
    //             , editable: function (params) {
    //                 if (params.node.data.newItem == 'Y') {
    //                     return true;
    //                 }
    //                 else {
    //                     return false;
    //                 }
    //             }
    //             , customCellClass: 'cell_align_center'
    //         },
    //         { 
    //             headerName: "라인ID"
    //             , field: "lineId"
    //             , width: 70
    //             , editable: function (params) {
    //                 if (params.node.data.newItem == 'Y') {
    //                     return true;
    //                 }
    //                 else {
    //                     return false;
    //                 }
    //             }
    //             , customCellClass: 'cell_align_center'
    //         },
    //         { 
    //             headerName: "라인명"
    //             , field: "lineNm"
    //             , width: 120
    //             , cellClass: 'cell_align_left'

    //         },
    //         { 
    //             headerName: "라인단축명"
    //             , field: "lineShortNm"
    //             , width: 120
    //             , cellClass: 'cell_align_left'
    //         },
    //         {
    //             headerName: "기본위치"
    //             , field: "defaultLocNm"
    //             , width: 80
    //             , cellClass: 'cell_align_left'
    //             , editable: false
    //             , onCellClicked: this.getLocation

    //         },
    //         { 
    //             headerName: "오토라벨러 여부"
    //             , field: "autoYn"
    //             , width: 105
    //             , cellClass: 'cell_align_center'
    //             , onCellValueChanged: this.onChange
    //         },
    //         { 
    //             headerName: "오토라벨러 코드"
    //             , field: "autoCd"
    //             , width: 105
    //             , cellClass: 'cell_align_center'
    //             , editable: function (params) {
    //                 if (params.node.data.autoYn == 'Y') {
    //                     return true;
    //                 }
    //                 else {
    //                     return false;
    //                 }
    //             }
    //         },
    //         { 
    //             headerName: "오토라벨러 그룹코드"
    //             , field: "autoGroupCds"
    //             , width: 120
    //             , cellClass: 'cell_align_center'
    //             , editable: function (params) {
    //                 if (params.node.data.autoYn == 'Y') {
    //                     return true;
    //                 }
    //                 else {
    //                     return false;
    //                 }
    //             }
    //         },
    //         { 
    //             headerName: "오토라벨러 개수"
    //             , field: "autoCount"
    //             , width: 105
    //             , editable: function (params) {
    //                 if (params.node.data.autoYn == 'Y') {
    //                     return true;
    //                 }
    //                 else {
    //                     return false;
    //                 }
    //             }
    //             , cellClass: 'cell_align_right'
    //         },
    //         { 
    //             headerName: "외주여부"
    //             , field: "outsourcingYn"
    //             , width: 80
    //             , cellClass: 'cell_align_center'
    //             , onCellValueChanged: this.onChange
    //         },
    //         { 
    //             headerName: "외주 현장대리인"
    //             , field: "outsourcingLoginNm"
    //             , width: 105
    //             , editable: function (params) {
    //                 if (params.node.data.outsourcingYn == 'Y') {
    //                     return true;
    //                 }
    //                 else {
    //                     return false;
    //                 }
    //             }
    //             , cellClass: 'cell_align_center'
    //             , onCellClicked: this.getEmp
    //         }
    //     ];
    //     GridUtil.setSelectCellByList(columnDefs[0], CommonStore.buList, "buId", "buNm"); 
    //     GridUtil.setSelectCellByCode(columnDefs[5], "SYS001");
    //     GridUtil.setSelectCellByCode(columnDefs[9], "SYS001");
    //     this.lineGridApi.setColumnDefs(columnDefs);
    // }
}

export default new LineStore();