import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setLineGridApi: stores.lineStore.setLineGridApi
    , lineList: stores.lineStore.lineList
    

    
}))
class LineContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLineGridApi, lineList } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'lineGrid'}
                    gridApiCallBack={setLineGridApi}
                    rowData={lineList}
                    // onCellClicked={buOnClick}
                    cellReadOnlyColor={true}
                    headerHeight={0}
                />
            </div>
        );
    }
}
export default LineContents;