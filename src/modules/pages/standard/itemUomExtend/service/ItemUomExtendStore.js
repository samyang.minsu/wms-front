import { observable, action } from 'mobx';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil'

import ItemUomExtendRepository from 'modules/pages/standard/itemUomExtend/service/ItemUomExtendRepository';

import CommonStore from 'modules/pages/common/service/CommonStore';
import BaseStore from 'utils/base/BaseStore';

class ItemUomExtendStore extends BaseStore {

	@observable itemUomExtendSearchItemItemCd = '';
	@observable itemUomExtendContentsGridApi = undefined;
	@observable itemUomExtendContentsList = [];

	constructor() {
        super();
        this.setInitialState();
    }

	@action.bound
	handleChange = (data) => {
		console.log(data)
		this[data.id] = data.value;
	}

	@action.bound
	rowDataHandleChange = (data) => {
		console.log('제품 UOM 관리 Data', data)
	}

	@action.bound
	handleSearchClick = async () => {
		console.log('value check', this.itemUomExtendSearchItemItemCd)
		const searchData = {
			"itemCd": this.itemUomExtendSearchItemItemCd
		}

		const { data, status } = await ItemUomExtendRepository.getItemUomExtendContents(searchData);
		if (data.success && status == 200) {
			this.itemUomExtendContentsList = data.data;
			this.itemUomExtendContentsGridApi.setRowData(this.itemUomExtendContentsList);
		} else {
			Alert.meg(data.msg);
			console.log('Error');
		}
	}

	@action.bound
	setItemUomExtendContentsGridApi = async (gridApi) => {
		this.itemUomExtendContentsGridApi = gridApi;
		const columnDefs = [
			{ headerName: "제품ID", field: "itemId", width: 70, cellClass: "cell_align_center" },
			{ headerName: "제품코드", field: "itemCd", width: 80, cellClass: "cell_align_center" },
			{ headerName: "UOM 구분", field: "uomGubn", width: 70, cellClass: "cell_align_center" },
			{ headerName: "FROM 단위", field: "fromUom", width: 70, cellClass: "cell_align_center" },
			{ headerName: "TO 단위", field: "toUom", width: 70, cellClass: "cell_align_center" },
			{
				headerName: "FROM 수량", field: "fromQty", width: 80, cellClass: "cell_align_right"
				, valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
				, valueParser: GridUtil.numberParser
			},
			{
				headerName: "TO 수량", field: "toQty", width: 80, cellClass: "cell_align_right"
				, valueFormatter: (params) => GridUtil.numberFormatter(params, 2)
				, valueParser: GridUtil.numberParser
			},

		];
		this.itemUomExtendContentsGridApi.setColumnDefs(columnDefs);
	}
}

export default new ItemUomExtendStore();