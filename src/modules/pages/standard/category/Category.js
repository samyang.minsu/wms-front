import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import ContentsMiddleTemplate from 'components/template/ContentsMiddleTemplate'
import ContentsTemplate from 'components/template/ContentsTemplate';

// import BuStore from 'modules/pages/standard/bu/service/BuStore';
import CategoryMiddleItem from 'modules/pages/standard/category/CategoryMiddleItem';
import CategoryContents from 'modules/pages/standard/category/CategoryContents';
import STree from 'components/override/tree/STree';
import SearchTemplate from 'components/template/SearchTemplate';
import CategorySearchItem from 'modules/pages/standard/category/CategorySearchItem';


@inject(stores => ({
    handleSelect: stores.categoryStore.handleSelect
    , handleCheck: stores.categoryStore.handleCheck
    , treeData: stores.categoryStore.treeData
    
}))

class Category extends Component {

    constructor(props) {
        super(props);

        // BuStore.getBu();
    }

    render() {

        const { handleSelect
                , handleCheck
                , treeData } = this.props;

        return (
            <React.Fragment>
                
                <div style={{ width: '100%', height: '100%', float: 'left' }}>
                    {/* <SearchTemplate searchItem={<CategorySearchItem />} /> */}
                    <div style={{ width: '25%', height: '100%', float: 'left' }}>
                        <ContentsMiddleTemplate contentSubTitle={'제품군 관리'} middleItem={<CategorySearchItem />}/>
                        <ContentsTemplate id='categoryTree'  contentItem = {
                            <STree
                                keyMember={"categoryCd"}
                                pIdMember={"pcategoryCd"}
                                titleMember={"categoryNm"}
                                checkable={false}
                                onSelect={handleSelect}
                                onCheck={handleCheck}
                                treeData={treeData}
                            />
                        }/>
                        {/* contentItem={<BuContents />} */}
                    </div>
                    
                    <div style={{ width: '74%', height: '100%', float: 'right' }}>
                        <ContentsMiddleTemplate middleItem={<CategoryMiddleItem />} />
                        <ContentsTemplate id="categoryGrid" contentItem={<CategoryContents />} />
                    </div>

                </div>


            </React.Fragment>
        )
    }
}

export default Category;