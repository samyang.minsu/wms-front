import { observable, action} from 'mobx';
import BaseStore from 'utils/base/BaseStore';

import GridUtil from 'components/override/grid/utils/GridUtil';

import Alert from 'components/override/alert/Alert';

import CommonStore from 'modules/pages/common/service/CommonStore';
import ItemCategoryRepository from 'modules/pages/system/itemCategory/service/ItemCategoryRepository';
import CategoryRepository from 'modules/pages/standard/category/service/CategoryRepository';

class CategoryStore extends BaseStore {

    constructor() {
        super()
        this.setInitialState();
    }

    @observable buId = CommonStore.buId;

    //트리 변수
    @observable treeData;
    
    //제품군 부모키 변수
    pKey = '';

    //공장 조회
    @action.bound
    getBu = async () => {
        this.buId = CommonStore,buId;
    }

    
    //제품군 조회
    @action.bound
    handleSearchClick = () => {
        this.setItemCategory();
    }

    //데이터변화 확인
    @action.bound
    handleChange = (data) => {
        console.log(data)
        this[data.id] = data.value;
    }

    
    info;
    //제품군 자식 조회
    @action.bound
    handleSelect = async (selectedKey, info) => {
        console.log(info);
        //selectedKey = 클릭한 자신의 키를 가지고 온다
        //info = 자식키 정보를 가지고 온다
        this.categoryGridApi.setRowData();
        let arr = info.node.children;
        this.pKey = info.node.key;
        
        if(arr) {
            arr.forEach( e => {
                //자식 코드가 없을때
                if(!e.children) {
                    const item = {
                        categoryCd: e.key,
                        categoryNm: e.title,
                        pcategoryCd: this.pKey,
                        child: ''
                    }
                    this.categoryGridApi.updateRowData({ add: [item] });
                } else {
                    //자식 코드가 있을때
                    const item = {
                        categoryCd: e.key,
                        categoryNm: e.title,
                        pcategoryCd: this.pKey,
                        child: 'Y'
                    }
                    this.categoryGridApi.updateRowData({ add: [item] });
                }


            });
        } else {
            this.categoryGridApi.setRowData();
        }

    }

    

    //제품군 추가
    @action.bound
    addClick = () => {
        
        if(this.pKey.length == 0){
            Alert.meg("선택된 제품군이 없습니다.");
            return;
        }

        let count = this.categoryGridApi.getDisplayedRowCount();

        const codeHdmt = {
            categoryCd: '',
            categoryNm: '',
            pcategoryCd: this.pKey,
            useFlag: 'Y',
            newItem: 'Y'
        };
        const result = this.categoryGridApi.updateRowData({ add: [codeHdmt], addIndex: count });
        GridUtil.setFocus(this.categoryGridApi, result.add[0]);
        
    }

    //제품군 저장
    @action.bound
    saveClick = async () => {
        //중복체크 변수
        let checkList = [];
        //코드 입력여부 체크 변수
        let cnt = 0;

        const saveArr = GridUtil.getGridSaveArray(this.categoryGridApi);
        if (saveArr.length == 0) {
            Alert.meg('저장할 데이터가 없습니다.');
            return;
        }
        
        this.categoryGridApi.forEachNode(rowNode => {
            if(rowNode.data.categoryCd.length < 1) {
                cnt++
                return;
            }  
        })
        if(cnt > 0) {
            Alert.meg("제품군 코드가 입력되지 않았습니다.");
            return;
        }
        
        this.treeData.forEach(check => {
            for(let i = 0; i < saveArr.length; i++ ) {
                if(saveArr[i].newItem == 'Y' && saveArr[i].categoryCd == check.categoryCd) {
                    checkList.push(saveArr[i].categoryCd);
                    break;
                } 
            }
        })

        if(checkList.length > 0) {
            Alert.meg("[" + checkList + "]이미 사용하고 있는 제품군 코드입니다.");
            return;
        }
        
        const param = JSON.stringify({ param: saveArr });
        
        const { data, status } = await CategoryRepository.saveCategory(param);
        if (status == 200) {
            
            this.setItemCategory();
            // Alert.meg(data.msg);
            Alert.meg("저장되었습니다.");
        } else {
            Alert.meg(data.msg);
        }
        
        
    }

    //제품군 삭제
    @action.bound
    deleteClick = async () => {
        const selectedRow = GridUtil.getSelectedRowData(this.categoryGridApi);

        if (!selectedRow || selectedRow.length == 0) {
			Alert.meg('선택된 제품군이 없습니다.');
			return;
        }

        const deleteData = {
            "categoryCd": selectedRow.categoryCd
        }

        if(selectedRow.newItem == 'Y') {
            this.categoryGridApi.updateRowData({ remove: [selectedRow] });
            return;
        }

        const value = await Alert.confirm("삭제하시겠습니까?").then(result => { return result; });
        if (value) {
            if(selectedRow.child == 'Y') {
                const realy = await Alert.confirm("하위 제품군이 있습니다. 삭제하시겠습니까?").then(result => { return result; });
                if(realy) {
                    
                    const { data, status } = await CategoryRepository.deleteCategory(deleteData);
                    if (status == 200) {
                        this.categoryGridApi.updateRowData({ remove: [selectedRow] });
                        this.setItemCategory();
                        Alert.meg("삭제되었습니다.");
                        return;
                    } else {
                        Alert.meg("실패하였습니다.");
                        return;
                    }
                } else return;
            }
            
            const { data, status } = await CategoryRepository.deleteCategory(deleteData);
            if (status == 200) {
                this.categoryGridApi.updateRowData({ remove: [selectedRow] });
                this.setItemCategory();
                Alert.meg("삭제되었습니다.");
            } else {
                Alert.meg("실패하였습니다.");
                return;
            }
        } else return;
    }
    

    


    //제퓸군 목록
    @action.bound
    setItemCategory = async () => {

        const params = {
            buId: this.buId
        }
        const { data, status } = await ItemCategoryRepository.getItemCategory(params);

        if (data.success) {
            
            this.treeData = data.data;
        }
    }






    // 제품군 그리드
    @action.bound
    setCategoryGridApi = async (gridApi) => {

        this.categoryGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "제품군 코드", field: "categoryCd", width: 100
                , editable: function (params) {
                    if (params.node.data.newItem == 'Y') {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                , customCellClass: 'cell_align_center'
            },
            { 
                headerName: "제품군명"
                , field: "categoryNm"
                , width: 150
                , cellClass: 'cell_align_left'
            },
            { 
                headerName: "상위 코드"
                , field: "pcategoryCd"
                , width: 200
                , cellClass: 'cell_align_left'
                , hide : true
            }
            
        ];

        
        this.categoryGridApi.setColumnDefs(columnDefs);
    }
    
}

export default new CategoryStore();