import React, { Component } from 'react';
import { inject } from 'mobx-react';
import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setCategoryGridApi: stores.categoryStore.setCategoryGridApi
    , categoryList: stores.categoryStore.categoryList
    
}))
class CategoryContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setCategoryGridApi, categoryList } = this.props;

        return (
            <div className="panel_body" style={{ height: '100%' }}>
                <SGrid
                    grid={'categoryGrid'}
                    gridApiCallBack={setCategoryGridApi}
                    rowData={categoryList}
                    cellReadOnlyColor={true}
                />
            </div>
        );
    }
}
export default CategoryContents;