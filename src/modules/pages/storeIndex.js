// layouts
import AuthStore from 'modules/layouts/main/service/AuthStore'
import MainStore from 'modules/layouts/main/service/MainStore'

// pages
import CommonStore from 'modules/pages/common/service/CommonStore'
import LoginStore from 'modules/pages/login/service/LoginStore'

// import LocationInoutStore from 'modules/pages/inout/locationInout/service/LocationInoutStore'



// pages-order
import InputStore from 'modules/pages/order/Input/service/InputStore'
import WherehouseStore from 'modules/pages/order/wherehouse/service/WherehouseStore'
import OutputStore from 'modules/pages/order/output/service/OutputStore'

// pages-system
import CodeStore from 'modules/pages/system/code/service/CodeStore'
import FormStore from 'modules/pages/system/form/service/FormStore'
import GrpStore from 'modules/pages/system/grp/service/GrpStore'
import ItemStore from 'modules/pages/system/item/service/ItemStore'
import MenuMtStore from 'modules/pages/system/menu/service/MenuMtStore'
import UserStore from 'modules/pages/system/user/service/UserStore'
import VersionSuperviseStore from 'modules/pages/system/versionSupervise/service/VersionSuperviseStore'

// pages-standard
import BuStore from 'modules/pages/standard/bu/service/BuStore'
import CategoryStore from 'modules/pages/standard/category/service/CategoryStore'
import LineStore from 'modules/pages/standard/line/service/LineStore'
import ItemUomExtendStore from 'modules/pages/standard/itemUomExtend/service/ItemUomExtendStore'
import ItemChangeMappingStore from 'modules/pages/standard/itemChangeMapping/service/ItemChangeMappingStore'
import LocationStore from 'modules/pages/standard/location/service/LocationStore'

// test
import TestStore from 'modules/pages/system/test/service/TestStore'

// etc
import StorageStore from 'modules/pages/system/storage/service/StorageStore'

class RootStore {

    constructor() {

        // layouts
        this.authStore = AuthStore
        this.mainStore = MainStore

        // pages
        this.commonStore = CommonStore
        this.loginStore = LoginStore

        // this.locationInoutStore = LocationInoutStore
        
        // pages-order
        this.inputStore = InputStore
        this.wherehouseStore = WherehouseStore
        this.outputStore = OutputStore
        // pages-system
        this.codeStore = CodeStore
        this.formStore = FormStore
        this.grpStore = GrpStore
        this.itemStore = ItemStore
        this.menuMtStore = MenuMtStore
        this.userStore = UserStore
        this.versionSuperviseStore = VersionSuperviseStore

        // pages-standard
        this.buStore = BuStore
        this.categoryStore = CategoryStore
        this.lineStore = LineStore
        this.itemUomExtendStore = ItemUomExtendStore
        this.itemChangeMappingStore = ItemChangeMappingStore
        this.locationStore = LocationStore

        // test
        this.testStore = TestStore

        // etc
        this.storageStore = StorageStore
    }

    reloadStore = (formClass) => {

        if (!formClass) {
            return
        }

        const className = formClass.substring(0, 1).toLowerCase() + formClass.substring(1, formClass.length) + "Store"

        try {
            this[className].reset()
        }
        catch (exception) {
            console.warn(`Store 초기화 실패: ${className}`)
            console.warn("가이드 확인 후 수정 바랍니다.")
        }
    }
}

//Singleton
const instance = new RootStore()
export default instance