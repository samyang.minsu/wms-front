import API from "components/override/api/API";
import APIWherehouse from "components/override/api/APIWherehouse";
import APIOut from "components/override/api/APIOut";

class InputRepository {
    getBarcode() {
        return API.request.get(encodeURI(`barcode/getbarcode`))
    }
    getLoclist(barcode) {   
        return APIWherehouse.request.get(encodeURI(`loc/getloclist/${barcode}`))
    }
    insertProd(params) {
        return API.request.post(encodeURI(`stock/setstockinsert`), params);
    }
}
export default new InputRepository();