import React, { Component } from "react";
import { inject } from "mobx-react";

import SearchTemplate from "components/template/SearchTemplate";
import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import InputSearchItem from "modules/pages/order/Input/InputSearchItem";
import InputContents from "modules/pages/order/Input/InputContents";
import LocContents from "modules/pages/order/Input/LocContents";
import LocSearchItem from "modules/pages/order/Input/LocSearchItem";

@inject(stores => ({
}))

class Input extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { 
        } = this.props

        return (
            <React.Fragment>
                <div className='basic'>
                        <div style={{ width: '50%', height: '100%', float: 'left' }}>
                            <ContentsMiddleTemplate contentSubTitle={'입고목록'} middleItem={<InputSearchItem />} />
                            <ContentsTemplate id='form' contentItem={<InputContents />} />
                        </div>
                        <div style={{ width: '50%', height: '100%', float: 'right' }}>
                            <ContentsMiddleTemplate contentSubTitle={'가용창고현황'} middleItem={<LocSearchItem />} />
                            <ContentsTemplate id='btn' contentItem={<LocContents />} />
                        </div>
                    </div>
            </React.Fragment>
        )
    }
}

export default Input;