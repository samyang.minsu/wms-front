import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setLocGridApi: stores.inputStore.setLocGridApi,
    vacancyLocList: stores.inputStore.vacancyLocList,
}))

class LocContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setLocGridApi, vacancyLocList} = this.props;

        return (
            <SGrid
                grid={'locGrid'}
                gridApiCallBack={setLocGridApi}
                rowData={vacancyLocList}
                editable={false}
            />
        );
    }
}
export default LocContents;