import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.inputStore.setGridApi,
    inputBarcodeList: stores.inputStore.inputBarcodeList,
    selectBarcodeLocSearch: stores.inputStore.selectBarcodeLocSearch
}))

class InputContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, inputBarcodeList, selectBarcodeLocSearch} = this.props;

        return (
            <SGrid
                grid={'InputGrid'}
                gridApiCallBack={setGridApi}
                rowData={inputBarcodeList}
                onSelectionChanged={selectBarcodeLocSearch}
                editable={false}
            />
        );
    }
}
export default InputContents;