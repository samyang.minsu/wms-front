import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    handleSearchClick: stores.inputStore.handleSearchClick
}))

class InputSearchItem extends Component {

    constructor(props) {
        super(props);

    }

    render() {

        const {handleSearchClick } = this.props;

        return (

            <React.Fragment>
                <div className='search_item_btn'>
                    <SButton
                        buttonName={'조회'}
                        type={'btnSearch'}
                        onClick={handleSearchClick} />
                </div>
            </React.Fragment>
        )
    }
}

export default InputSearchItem;