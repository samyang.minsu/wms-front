import API from "components/override/api/API";
import APIWherehouse from "components/override/api/APIWherehouse";
import APIOut from "components/override/api/APIOut";

class WherehouseRepository {
    getAllStocks() {
        console.log('여긴오나??')
        return API.request.get(encodeURI(`stock/getallstocks`))
    }
    getLoclist(barcode) {
        return APIWherehouse.request.get(encodeURI(`loc/getloclist/${barcode}`))
    }

    getStock() {
        return APIWherehouse.request.get(encodeURI(`loc/getwherehousestock`))
    }

    insertProd(params) {
        return API.request.post(encodeURI(`stock/setstockinsert`), params);
    }
}
export default new WherehouseRepository();