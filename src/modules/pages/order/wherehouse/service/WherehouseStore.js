import { observable, action, toJS } from 'mobx';


import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import BaseStore from 'utils/base/BaseStore';

import WherehouseRepository from 'modules/pages/order/wherehouse/service/WherehouseRepository';

class WherehouseStore extends BaseStore {

    // Prod contents
    gridApi = undefined;
    @observable allStockList = [];
    @observable StockList = [];
    @observable ChartList = [];

    // search Key
    barcode = ''
    locId = ''
    


    constructor() {
        super();
        this.chartSearch();
    }

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }

    /* MAIN */
    // main contents
    @action.bound
    setGridApi = async (gridApi) => {

        this.gridApi = gridApi;

        const columnDefs = [
            {
                headerName: "Sector"
                , field: "locNm"
                , width: 120
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "적재능력"
                , field: "capaQty"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 200
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "적재량"
                , field: "qty"
                , width: 80
                , cellClass: 'cell_align_center'
            },

        ];
        this.gridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    handleSearchClick = async () => {
        const { data, status } = await WherehouseRepository.getAllStocks();

        if (status !=  200) {
            Alert.meg('서버와 연결이 끊켰습니다.');
        } else {
            this.allStockList = data;
            this.StockList = [];
            let tempList = []
            for (let index = 0; index < data.length; index++) {
                tempList.push(data[index])
                if(tempList.length % 6 == 0) {
                    this.StockList.push(tempList)
                    tempList = []
                }
            }
            this.StockList = toJS(this.StockList)
            this.chartSearch()
            this.gridApi.setRowData(this.allStockList);
        }
    }

    @action.bound
    chartSearch = async () => {
        const { data, status } = await WherehouseRepository.getStock();

        if (status !=  200) {
            Alert.meg('서버와 연결이 끊켰습니다.');
        } else {
            this.ChartList = data;
            
            this.ChartList = toJS(this.ChartList)
        }
    }
}

export default new WherehouseStore();