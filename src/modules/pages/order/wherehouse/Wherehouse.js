import React, { Component } from "react";
import { inject } from "mobx-react";

import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import WherehouseSearchItem from "modules/pages/order/wherehouse/WherehouseSearchItem";
import WherehouseContents from "modules/pages/order/wherehouse/WherehouseContents";
import WherehouseTableContents from "modules/pages/order/wherehouse/WherehouseTableContents";
import WherehouseChartContents from "modules/pages/order/wherehouse/WherehouseChartContents";



@inject(stores => ({
}))

class Wherehouse extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { 
        } = this.props

        return (
            <React.Fragment>

                <div className='basic'>
                        <ContentsMiddleTemplate contentSubTitle={'창고현황'} middleItem={<WherehouseSearchItem />} />
                        <div style={{ width: '70%', height: '100%', float: 'left' }}>
                            <ContentsTemplate id='btn1' contentItem={<WherehouseTableContents />}/>
                        </div>
                        <div style={{ width: '30%', height: '100%', float: 'right' }}>
                            <ContentsTemplate id='btn2' contentItem={<WherehouseChartContents />}/>
                        </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Wherehouse;