import React, { Component } from 'react';
import { inject } from 'mobx-react';

import { toJS, action } from 'mobx';


@inject(stores => ({
    StockList: stores.wherehouseStore.StockList
}))

export default class WherehouseTableContens extends Component {

    constructor(props) {
        super(props);
    }
    render() {
        const {StockList}  = this.props;

         let fontStyle = {fontSize:'20px'};
        

        let thRows=[];
        let tbody=[];
        
        console.log(StockList, 'StockList')
        let resultData = null
        resultData = StockList;

        console.log(resultData, 'resultData')

        if(resultData.length == 0) {
            thRows.push(<th key={0} style={fontStyle}>로딩중...</th>);
        }else {
            for (let i = 0; i < 6; i++) {
                thRows.push(<th key={i+1} style={fontStyle}>{'Sector'+(i+1)}</th>);
            }
        }
        

        for (let i = 0; i < resultData.length; i++) {
            let content = resultData[i];
            
            let tr =(
                <tr key={i}>
                {             
                    content.map(function(item) {         
                        return  <td key={item.sort}  style={{backgroundColor:item.useYn != 'Y' ? '#f78969' : '#cbd656', border:"1px solid #d5d3cd", height: '80px', width: '150px'}}>
                                    <div style={{border:'3px solid #fff', padding:'4px 0', height:'100%', verticalAlign:'middle', width: '100%'}}>
                                        <span key={item.sort} style={{color: '#222', verticalAlign: 'middle', height:'100%', width: '100%'}}>{item.qty}/{item.capaQty}<br/> {item.itemNm != '' ? item.itemNm : '적재가능'}<br/> {item.locNm}</span>
                                    </div>
                                </td>
                    })
                }
                </tr>
            );
            tbody.push(tr);

            console.log(thRows, tbody , 'tbodytbody')
        }
        
        return (
            <React.Fragment>
                <div className="sub_table_wrap sub_table_scroll" style={{fontSize:'20px', border: "1px solid #dee1e3"}}>
                    <table className="sub_table sub_table_hover" style={{fontStyle}}>
                        <thead>
                            <tr>
                            {thRows}
                            </tr>
                        </thead>
                        <tbody>
                            {tbody}
                        </tbody>
                    </table>
                </div>
            </React.Fragment>
        )
    }
}