import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx';

import {ResponsiveContainer, ComposedChart, Bar, XAxis, YAxis, Legend, Tooltip} from 'recharts'

import SimpleChart from "modules/pages/order/wherehouse/SimpleChart";


@inject(stores => ({
    ChartList: stores.wherehouseStore.ChartList,
}))

class WherehouseChartContents extends Component {

    constructor(props) {
        super(props);
    }

    

    render() {

        const {ChartList} = this.props;

        const chart1Style = {
            width: '100%',
            minWidth: '400px',
            minHeight: '80',
            overflowX: 'hidden',
            overflowY: 'hidden',
            height: '45%',
            float: 'left',
            // position:'relative',
            // background:'#fff',
            // marginBottom:'8px',
            padding: '5px',
            paddingTop : '100px'
        };


        let list = [];
        list.push(ChartList)
       
        return (
            <div style={{height: '100%' , width: '100%'}}>
                <div style={chart1Style}>
                    <SimpleChart data={list} margin={{ top: 5, right: 0, bottom: 0, left: 0 }} >
                        <XAxis  tick={false}/>
                        <YAxis/>
                        <Tooltip />
                        <Legend />
                        <Bar name='적재량' stackId="a" dataKey='unUseQty' fill="#f78969" />
                        <Bar name='적재가능량' stackId="b" dataKey='useQty' fill="#cbd656" />
                        <Bar name='창고총Capa' stackId="c" dataKey='totalQty' fill="#6600FF" />
                    </SimpleChart>
                </div>
                <div style={chart1Style}>
                    <SimpleChart data={list} margin={{ top: 5, right: 0, bottom: 0, left: 0 }} >
                        <XAxis  tick={false} />
                        <YAxis/>
                        <Tooltip />
                        <Legend />
                        <Bar name='비가용공간(%)' stackId="b" dataKey='usePer' fill="#f78969" />          
                        <Bar name='가용공간(%)' stackId="a" dataKey='avPer' fill="#cbd656" />
                    </SimpleChart>
                </div>
            </div>
            
        );
    }
}
export default WherehouseChartContents;