import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SButton from 'components/atoms/button/SButton';

@inject(stores => ({
    handleSearchClick: stores.wherehouseStore.handleSearchClick
}))

class WherehouseSearchItem extends Component {

    constructor(props) {
        super(props);

    }

    render() {

        const {handleSearchClick } = this.props;

        return (

            <React.Fragment>
                <div className='search_item_btn'>
                    <ul style={{ lineHeight: '13px', marginTop: '5px' }}>
                        <li className="condition" style={{verticalAlign: 'super'}}><span className="ico_condition" style={{ background: "#f78969" }}>값</span>적재중</li>
                        <li className="condition" style={{verticalAlign: 'super'}}><span className="ico_condition" style={{ background: "#cbd656" }}>값</span>적재가능</li>
                        
                            <SButton
                            buttonName={'조회'}
                            type={'btnSearch'}
                            onClick={handleSearchClick} />
                        
                    </ul>
                </div>
            </React.Fragment>
        )
    }
}

export default WherehouseSearchItem;