
import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {ResponsiveContainer ,ComposedChart, Line, Bar, XAxis, YAxis, Legend, Tooltip} from 'recharts'

export default class SimpleChart extends Component {

    constructor(props) {
        super(props);
    }

    createChildren(children, newChildren) {

        for (let i = 0; i < children.length; i++) {

            if (children[i].type == undefined) {
                this.createChildren(children[i], newChildren);
            }

            else if (children[i].type.displayName == "YAxis") {
                newChildren.push(<YAxis key={`simpleChart_yAxis_${i}`}
                                        {...children[i].props}
                                        type='number'
                                        domain={['auto', 'auto']}
                                        tickFormatter={(value) =>`${String(value).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,')}`}                
                                />);
            }

            else if (children[i].type.displayName == "Legend") {
                newChildren.push(<Legend key={`simpleChart_legend_${i}`}
                                         {...children[i].props}
                                         layout="vertical"
                                         align='right'
                                         verticalAlign='middle' />);
            }

            else if (children[i].type.displayName == "Bar") {

                const maxBarSize = children[i].props.maxBarSize == undefined ? 30 : children[i].props.maxBarSize;

                newChildren.push(<Bar key={`simpleChart_bar_${i}`} {...children[i].props} 
                                      maxBarSize={maxBarSize}
                                 />);
            }

            else if (children[i].type.displayName == "Line") {
                newChildren.push(<Line key={`simpleChart_line_${i}`}
                                       {...children[i].props} 
                                       strokeWidth={3}
                                       type='monotone'
                                 />);
            }

            else if (children[i].type.displayName == "Tooltip") {
                newChildren.push(<Tooltip key={`simpleChart_tooltip_${i}`}
                                          {...children[i].props} 
                                          formatter={(value) => `${String(value).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,')}`}
                                  />);
            }

            else {
                newChildren.push(children[i]);
            }
        }
    }


    render() {
        
        let newChildren = [];
        const children = this.props.children;

        this.createChildren(children, newChildren)

        return (
            <ResponsiveContainer height={this.props.height}>
                <ComposedChart data={this.props.data} margin={this.props.margin}>
                    {newChildren}
                </ComposedChart>
            </ResponsiveContainer>
        );
    }
}