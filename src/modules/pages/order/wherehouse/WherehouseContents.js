import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setGridApi: stores.wherehouseStore.setGridApi,
    allStockList: stores.wherehouseStore.allStockList,
}))

class WherehouseContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setGridApi, allStockList} = this.props;

        return (
            <SGrid
                grid={'wherehouseGrid'}
                gridApiCallBack={setGridApi}
                rowData={allStockList}
                editable={false}
            />
        );
    }
}
export default WherehouseContents;