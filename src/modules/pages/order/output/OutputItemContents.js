import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setOutItemGridApi: stores.outputStore.setOutItemGridApi,
    outputItemList: stores.outputStore.outputItemList,
    selectOutputShipSearch: stores.outputStore.selectOutputShipSearch
}))

class OutputItemContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setOutItemGridApi, outputItemList, selectOutputShipSearch} = this.props;

        return (
            <SGrid
                grid={'OutputItemGrid'}
                gridApiCallBack={setOutItemGridApi}
                rowData={outputItemList}
                onSelectionChanged={selectOutputShipSearch}
                editable={false}
            />
        );
    }
}
export default OutputItemContents;