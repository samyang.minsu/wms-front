import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setOutLocGridApi: stores.outputStore.setOutLocGridApi,
    shipLocList: stores.outputStore.shipLocList,
}))

class OutLocContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setOutLocGridApi, shipLocList} = this.props;

        return (
            <SGrid
                grid={'OutLocGrid'}
                gridApiCallBack={setOutLocGridApi}
                rowData={shipLocList}
                editable={false}
            />
        );
    }
}
export default OutLocContents;