import React, { Component } from 'react';
import { inject } from 'mobx-react';

import SGrid from 'components/override/grid/SGrid'

@inject(stores => ({
    setOutGridApi: stores.outputStore.setOutGridApi,
    outputList: stores.outputStore.outputList,
    selectOutputItemSearch: stores.outputStore.selectOutputItemSearch
}))

class OutputContents extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { setOutGridApi, outputList, selectOutputItemSearch} = this.props;

        return (
            <SGrid
                grid={'OutputGrid'}
                gridApiCallBack={setOutGridApi}
                rowData={outputList}
                onSelectionChanged={selectOutputItemSearch}
                editable={false}
            />
        );
    }
}
export default OutputContents;