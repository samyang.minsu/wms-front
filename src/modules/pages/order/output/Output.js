import React, { Component } from "react";
import { inject } from "mobx-react";


import ContentsMiddleTemplate from "components/template/ContentsMiddleTemplate";
import ContentsTemplate from "components/template/ContentsTemplate";
import OutputSearch from "modules/pages/order/output/OutputSearch";
import OutputContents from "modules/pages/order/output/OutputContents";
import OutputItemContents from "modules/pages/order/output/OutputItemContents";
import OutLocContents from "modules/pages/order/output/OutLocContents";
import OutLocSearchItem from "modules/pages/order/output/OutLocSearchItem";

@inject(stores => ({
}))

class Output extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { 
        } = this.props

        return (
            <React.Fragment>
               
                    <div style={{ width: '100%', height: '30%' }}>
                        <ContentsMiddleTemplate contentSubTitle={'상차 지시 정보'} middleItem={<OutputSearch />} />
                        <ContentsTemplate id='form' contentItem={<OutputContents />} />
                    </div>
                    <div style={{ padding:'10px 0 0 5px'}}></div>
                    <div style={{ width: '100%', height: '69%' }}>
                         <div style={{ width: '40%', height: '100%', float: 'left' }}>
                            <ContentsMiddleTemplate contentSubTitle={'출하 정보'}  />
                            <ContentsTemplate id='formDn' contentItem={<OutputItemContents />} />
                        </div>
                        <div style={{ width: '60%', height: '100%', float: 'right' }}>
                            <ContentsMiddleTemplate contentSubTitle={'로케이션 정보'} middleItem={<OutLocSearchItem />} />
                            <ContentsTemplate id='btn' contentItem={<OutLocContents />} />
                        </div>
                    </div>
            </React.Fragment>
        )
    }
}

export default Output;