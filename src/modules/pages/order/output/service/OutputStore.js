import { observable, action } from 'mobx';

import Alert from 'components/override/alert/Alert';
import GridUtil from 'components/override/grid/utils/GridUtil';
import BaseStore from 'utils/base/BaseStore';

import OutputRepository from 'modules/pages/order/output/service/OutputRepository';

class OutputStore extends BaseStore {

    // out contents
    outgridApi = undefined;
    @observable outputList = [];
    // out contents
    outItemgridApi = undefined;
    @observable outputItemList = [];
    // Loc contents
    outLocGridApi = undefined;
    @observable shipLocList = [];

    // search Key
   

    constructor() {
        super();
        this.setInitialState();
    }

    @action.bound
    handleChange = (data) => {
        console.log(data);
        this[data.id] = data.value;
    }

    /* MAIN */
    // main contents
    @action.bound
    setOutGridApi = async (gridApi) => {

        this.outgridApi = gridApi;

        const columnDefs = [
            {
                headerName: "출하번호"
                , field: "shipNo"
                , width: 100
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "출하일자"
                , field: "orderDt"
                , width: 80
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "배송처명"
                , field: "shipToNm"
                , width: 100
                , cellClass: 'cell_align_left'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "주소"
                , field: "addr"
                , width: 200
                , cellClass: 'cell_align_left'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "우편번호"
                , field: "zipCd"
                , width: 60
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            
        ];
        this.outgridApi.setColumnDefs(columnDefs);
    }
    @action.bound
    setOutItemGridApi = async (gridApi) => {

        this.outItemgridApi = gridApi;

        const columnDefs = [
            {
                headerName: "출하번호"
                , field: "shipNo"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 200
                , cellClass: 'cell_align_left'
            },
            {
                headerName: "지시수량(Kg)"
                , field: "orderQty"
                , width: 80
                , cellClass: 'cell_align_right'
                , valueFormatter: GridUtil.intFormatter
            },
            {
                headerName: "상차수량(Kg)"
                , field: "outQty"
                , width: 80
                , cellClass: 'cell_align_right'
                , valueFormatter: GridUtil.intFormatter
            },
        ];
        this.outItemgridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    setOutLocGridApi = async (gridApi) => {

        this.outLocGridApi = gridApi;

        const columnDefs = [
            {
                headerName: "로케이션"
                , field: "locCd"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "로케이션명"
                , field: "locNm"
                , width: 100
                , cellClass: 'cell_align_left'
            },
            {
                headerName: "제품코드"
                , field: "itemCd"
                , width: 80
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "제품명"
                , field: "itemNm"
                , width: 150
                , cellClass: 'cell_align_left'
            },
            {
                headerName: "바코드"
                , field: "barcode"
                , width: 100
                , cellClass: 'cell_align_center'
            },
            {
                headerName: "수량(kg)"
                , field: "qty"
                , width: 60
                , cellClass: 'cell_align_right'
                , valueFormatter: GridUtil.intFormatter
            },
            {
                headerName: "제조일자"
                , field: "makeDt"
                , width: 80
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "유통기한"
                , field: "efftDt"
                , width: 80
                , cellClass: 'cell_align_center'
                , valueFormatter: GridUtil.dateFormatter
            },
            {
                headerName: "적재여부"
                , field: "useYn"
                , width: 80
                , cellClass: 'cell_align_center'
            },
        ];
        this.outLocGridApi.setColumnDefs(columnDefs);
    }

    @action.bound
    handleSearchClick = async () => {
        
        const { data, status } = await OutputRepository.getShipMt();

        if (status !=  200) {
            Alert.meg('서버와 연결이 끊켰습니다.');
        } else {
            this.outputList = data;

            console.log(this.outgridApi , '??')
            this.outgridApi.setRowData(this.outputList);
        }
    }
    @action.bound
    selectOutputItemSearch = async () => {

        const selectedRow = this.outgridApi.getSelectedRows()[0];
        
        if(selectedRow.shipNo != undefined) {

            const { data, status } = await OutputRepository.getShipDtl(selectedRow.shipNo);
           
            if (status !=  200) {
                Alert.meg('서버와 연결이 끊켰습니다.');
            } else {
                this.outputItemList = data;
                console.log(this.outputItemList , '??')
                this.outItemgridApi.setRowData(this.outputItemList);
            }

        }
    }
    selectOutputShipSearch = async () => {

        const selectedRow = this.outItemgridApi.getSelectedRows()[0];
        
        if(selectedRow.itemCd != undefined) {

         
            const { data, status } = await OutputRepository.getStockList(selectedRow.itemCd);

            if (status !=  200) {
                Alert.meg('서버와 연결이 끊켰습니다.');
            } else {
                this.shipLocList = data;
                this.outLocGridApi.setRowData(this.shipLocList);
            }

        }
    }
    @action.bound
    confrimProd = async () => {

        const selectedRow = this.outLocGridApi.getSelectedRows()[0];
        const selectedRow2 = this.outgridApi.getSelectedRows()[0];
        

        console.log(selectedRow, 'selectedRow???')
        
        if(selectedRow.locCd != undefined) {

            const saveData = {
                   "shipNo":selectedRow2.shipNo
                ,  "itemCd": selectedRow.itemCd      
                ,  "outQty": selectedRow.qty   
                ,  "locCd": selectedRow.locCd
                ,  "barcode": selectedRow.barcode
            }
    
            const params = JSON.stringify(saveData);

            const { data, status } = await OutputRepository.inertShipDtl(params);
            Alert.meg('서버와 연결이 끊켰습니다.');
            if (status !=  200) {
                Alert.meg('서버와 연결이 끊켰습니다.');
            } else {
                Alert.meg('상차가 완료되었습니다.');
                this.selectOutputItemSearch();
                this.outLocGridApi.setRowData([]);
            }
        }
    }
}

export default new OutputStore();