import API from "components/override/api/API";
//import APIWherehouse from "components/override/api/APIWherehouse";
import APIOut from "components/override/api/APIOut";

class OutputRepository {
    getShipMt() {
        return APIOut.request.get(encodeURI(`/out/getshipmt`))
    }

    getShipDtl(shipNo) {
        return APIOut.request.get(encodeURI(`/out/getshipdtl/${shipNo}`))
    }
    getStockList(itemCd) {
        return API.request.get(encodeURI(`/stock/getstocklist/${itemCd}`))
    }
    inertShipDtl(params) {
        return APIOut.request.post(encodeURI(`/out/setshipdtl`), params);
    }
}
export default new OutputRepository();