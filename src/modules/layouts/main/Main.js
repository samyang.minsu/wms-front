import React, { Component } from 'react';
import SMainTab from 'components/override/tab/SMainTab'
import { inject } from 'mobx-react'

@inject(stores => ({
    tabsInfo: stores.mainStore.tabsInfo,
    tabActiveIndex: stores.mainStore.tabActiveIndex,
    handleEdit: stores.mainStore.handleEdit,
    handleTabChange: stores.mainStore.handleTabChange,
}))
class Main extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        const { tabsInfo, tabActiveIndex, handleEdit, handleTabChange } = this.props;

        return (
            <React.Fragment>
                <div className='main_wrap'>
                    <SMainTab tabs={tabsInfo}
                        onTabEdit={handleEdit}
                        onTabChange={handleTabChange}
                        activeIndex={tabActiveIndex}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default Main;