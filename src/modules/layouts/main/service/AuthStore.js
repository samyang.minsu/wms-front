import { observable, action } from 'mobx'

class AuthStore {

    @observable isAuthenticated = false;

    constructor(root) {
        this.root = root;
    }

    @action authenticate = () => {
        this.isAuthenticated = true
    }

    @action signout = () => {
        this.isAuthenticated = false
    }

    @action getAuthenticated = () => {
        return this.isAuthenticated;
    }

}
export default new AuthStore();

