import React, { Component } from 'react';
import { observable, action, toJS } from 'mobx';
import { SProvider } from 'utils/context/SContext'
import MenuRepository from 'modules/layouts/main/service/MenuRepository';
import MenuModel from 'modules/layouts/main/model/MenuModel';
import ObjectUtility from 'utils/object/ObjectUtility';
import RootStore from 'modules/pages/storeIndex'
import CommonStore from 'modules/pages/common/service/CommonStore';
import Alert from 'components/override/alert/Alert';
import VersionSuperviseRepository from 'modules/pages/system/versionSupervise/service/VersionSuperviseRepository';

class MainStore {

    @observable isMenuExpand = true;
    @observable isMenuOverlap = false;

    @observable tabsInfo = [];
    @observable tabActiveIndex = 0;

    @observable sideMenuList = JSON.parse(window.sessionStorage.getItem('sideMenuList'));
    @observable allSubMenuArr = JSON.parse(window.sessionStorage.getItem('allSubMenuArr'));

    @observable siteArray = [];
    @observable isMenu = false;
    menuNameMapByUrl = JSON.parse(window.sessionStorage.getItem('menuNameMapByUrl'));

    @observable familySiteArr = JSON.parse(window.sessionStorage.getItem('familySite'));

    @observable profileImage = '';

    // 업데이트 시 사용자 알림
    getDataPolling = null;
    currentVersion = "";

    constructor(root) {
        this.root = root;
    }

    async loaded() {
        await CommonStore.getCodeObject();
        this.currentVersion = await this.getLastVersion();
        this.setUpdateTimer();
    }

    setUpdateTimer() {

        let timer = CommonStore.codeObject["SYS010"];
        timer = parseInt(timer[0].cdNm);

        if (timer == undefined || timer < 60000) {
            timer = 360000
        }
        
        this.getDataPolling = setInterval(async () => {
            try {
                await this.updateCheck();
            }
            catch (exception) {
                console.error('Update Exception', exception);
            }
        }, timer);
    }

    async getLastVersion() {
        const { data, status } = await VersionSuperviseRepository.getLastVer();

        if (data.success == true) {
            return data.data.version;
        }
    }

    @action.bound
    async updateCheck() {
        const lastVersion = await this.getLastVersion();

        if (lastVersion != this.currentVersion) {
            clearInterval(this.getDataPolling);
            this.getDataPolling = null;

            const element = document.querySelector('#updateLayer');
            element.style.display = 'block';
        }
    }

    @action.bound
    handleChange(data) {
        this[data.id] = data.value;
    }

    @action.bound
    async getSideMenu() {
        const { data, status } = await MenuRepository.getSideAuthMenu();

        const menuLength = data.data.length;

        //this.allSubMenuMap = new Map();
        this.allSubMenuArr = [];
        this.menuNameMapByUrl = {};
        for (let i = 0; i < menuLength; i++) {
            const menu = data.data[i];
            const subMenuArr = menu.subMenu;
            const subLength = subMenuArr.length;

            for (let j = 0; j < subLength; j++) {
                const subMenu = subMenuArr[j];
                //this.allSubMenuMap.set(subMenu.formUrl, subMenu);
                this.allSubMenuArr.push(subMenu);
                this.menuNameMapByUrl[subMenu.formUrl] = subMenu.menuNm;
            }
        }

        this.sideMenuList = data.data.map(sideMenu => new MenuModel(sideMenu));

        this.setMenuNameMapByUrl(this.menuNameMapByUrl);
        this.setSubMenuSesstion(this.allSubMenuArr);
        this.setMenuSesstion(this.sideMenuList);

        // this.getProfileImage();

    }
    @action.bound
    getProfileImage = async () => {
        const {data, status} = await MenuRepository.getProfileImage();
        if (data.success == true) {
            console.log('getprofile', data.data);
            console.log(data.data.empPhotoSrc);
            this.profileImage = data.data.empPhotoSrc;
        } else {
            Alert.meg(data.msg);
        }
        
    }
    @action clearMenu() {
        this.allSubMenuArr = undefined;
        this.sideMenuList = undefined;
        this.menuNameMapByUrl = undefined;
        window.sessionStorage.removeItem('menuNameMapByUrl');
        window.sessionStorage.removeItem('allSubMenuArr');
        window.sessionStorage.removeItem('sideMenuList');
    }

    @action setAllSubMenuArr(allSubMenuArr) {
        this.allSubMenuArr = allSubMenuArr;
    }

    @action setSideMenuList(sideMenuList) {
        this.sideMenuList = sideMenuList;
    }

    @action setMenuNameMapByUrl(menuNameMapByUrl) {
        window.sessionStorage.setItem('menuNameMapByUrl', JSON.stringify(menuNameMapByUrl));
    }

    @action setSubMenuSesstion(allSubMenuArr) {
        window.sessionStorage.setItem('allSubMenuArr', JSON.stringify(allSubMenuArr));
    }

    @action setMenuSesstion(sideMenuList) {
        window.sessionStorage.setItem('sideMenuList', JSON.stringify(sideMenuList));
    }

    @action setFamilySiteSesstion(familySite) {
        window.sessionStorage.setItem('familySite', JSON.stringify(familySite));
    }

    @action.bound
    async getFamilySite() {
        const codeGroup = [{ "code": "SYS020", "allAdd": false }, { "code": "SYS021", "allAdd": false }];

        commonStore.getCodeObjectSelectBox(codeGroup).then(codeObject => {
            let siteArr = [];
            for (let i = 0; i < codeGroup.length; i++) {
                const element = codeGroup[i];
                const arr = eval(`codeObject['${element.code}']`);

                for (let j = 0; j < arr.length; j++) {
                    const data2 = arr[j];
                    siteArr.push({ cd: data2.desc2, cdNm: `${data2.desc1}` });
                }

            }
            siteArr.unshift({ cd: "", cdNm: "FAMILY SITE" });

            this.familySiteArr = siteArr.concat();
            this.setFamilySiteSesstion(this.familySiteArr);

        })
    }

    @action.bound
    getMenuNameByUrl(url) {
        return this.menuNameMapByUrl[ObjectUtility.replaceAll(url, "/app", "")];
    }

    @action.bound
    setIsMenu(flag) {
        this.isMenu = flag;
    }

    @action.bound
    getButtonAuth(path) {
        const length = this.allSubMenuArr.length;

        for (let i = 0; i < length; i++) {
            const subMenu = this.allSubMenuArr[i];
            if (path.indexOf(subMenu.formUrl) > 0) {
                return subMenu;
            }
        }
    }

    @action.bound
    onMenuOpen(subMenu) {
        this.isMenuOverlap = false;
        this.addMenu(subMenu);
    }

    @action.bound
    async addMenu(selectedMenu) {

        const url = selectedMenu.url;
        const formId = selectedMenu.formId;
        const menuNm = selectedMenu.menuNm;

        const index = this.tabsInfo.findIndex(x => x.contentUrl == url);


        console.log(selectedMenu, '오잉 여긴가??');
        ObjectUtility.loadingLayer(true);
        if (index < 0) {
            this.tabsInfo = this.tabsInfo.concat({
                title: menuNm
                , formClass: selectedMenu.formClass
                , id: selectedMenu.menuId
                , contentUrl: url
                , content: await this.createMenu(url, formId, menuNm)
                , formId: formId
            });
            this.tabActiveIndex = this.tabsInfo.length - 1;
        } else {
            this.tabActiveIndex = index;
        }
        ObjectUtility.loadingLayer(false);
    }

    async createMenu(url, formId, menuNm) {

        console.log(url, formId, menuNm,'권한정보111')

        return await import(`modules/pages${url}`).then(module => {
            const Component = module.default;

            return (
                <SProvider menuPath={url} formId={formId} menuNm={menuNm}>
                    <div className='basic'>
                        <Component />
                    </div>
                </SProvider>
            )

        });
    }

    @action.bound
    handleTabChange(index) {
        this.tabActiveIndex = index;
    }

    @action.bound
    handleEdit({ type, index }) {

        if (type == 'delete') {
            const closeMenu = this.tabsInfo.find((x, i) => i == index);
            RootStore.reloadStore(closeMenu.formClass);
            
            this.tabsInfo = this.tabsInfo.filter((x, i) => i != index);
        }

        let activeIndex = 0;

        if (index - 1 >= 0) {
            activeIndex = index - 1;
        } else {
            activeIndex = 0;
        }

        this.handleTabChange(activeIndex);
    }

    @action.bound
    onMenuExpandClick = () => {
        this.isMenuExpand = !this.isMenuExpand;
        this.isMenuOverlap = false;
    }

    onOneDepthMenuClick = () => {
        this.isMenuOverlap = true;
    }
}

export default new MainStore();
