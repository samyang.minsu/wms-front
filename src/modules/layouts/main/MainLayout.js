import React, { Component } from 'react';
import { inject } from 'mobx-react';
import HeaderLayout from 'modules/layouts/main/HeaderLayout';
import MenuLayout from 'modules/layouts/menu/MenuLayout';
import Main from 'modules/layouts/main/Main';

@inject(stores => ({
    mainStore: stores.mainStore,
    commonStore: stores.commonStore,
}))
class MainLayout extends Component {

    async componentWillMount() {

        // const { commonStore, mainStore } = this.props;

        // if (!commonStore.userInfo) {
        //     await commonStore.commonUserInfo();
        // }

        // await mainStore.loaded();

        // // 최초 로딩 시에 무조건 Menu 가져오기
        // mainStore.getSideMenu();
    }

    render() {

        return (
            <div id='wrapper'>
                <div id="layerOverlap" style={{
                    zIndex: 2000
                    , position: 'absolute'
                    , width: '100%'
                    , height: '100%'
                    , backgroundColor: '#ffffff'
                    , overflow: 'hidden'
                    , opacity: 0
                    , display: 'none'
                }} />

                <div id="updateLayer" style={{
                    zIndex: 9999999
                    , position: 'absolute'
                    , width: '100%'
                    , height: '100%'
                    , backgroundColor: 'rgba(0, 0, 0, 0.4)'
                    , overflow: 'hidden'
                    , display: 'none'
                }}>
                    <div style={{
                        fontSize: '24px'
                        , color: 'red'
                        , width: '500px'
                        , height: '84px'
                        , top: 'calc(50% - 42px)'
                        , left: 'calc(50% - 250px)'
                        , position: 'absolute'
                        , backgroundColor: 'white'
                        , borderRadius: '3px'
                        , paddingTop : '9px'
                        , textAlign: 'center'
                    }}>
                        버전이 업데이트 되었습니다. <br />
                        새로고침(F5)을 해주세요.
                    </div>
                </div>

                <HeaderLayout />

                <div id='container'>
                    <MenuLayout />
                    <Main />
                </div>
            </div>
        )
    }
}

export default MainLayout;
