import React from 'react'
import { inject } from 'mobx-react'
import { Route, Redirect, withRouter } from 'react-router-dom'

@inject("authStore", "loginStore")
class PrivateRoute extends React.Component {

    constructor(props) {
        super(props);
    }

    async componentWillMount() {
        const { authStore, loginStore } = this.props;

        // render 되기 전에 loginId 있는지 확인함
        if (loginStore.loginId !== null) {
            // loginId 있으면 페이지 이동 권한 켜줌
            authStore.authenticate();
        } else {
            authStore.signout();
        }
    }

    render() {
        const { component: Component, authStore, match, ...rest } = this.props;
        const isAuthenticated = authStore.isAuthenticated;

        const renderRoute = props => {
            if (isAuthenticated) {
                return (
                    <Component {...props} />
                )
            } else {

                console.log('privateRoute');
                return (
                    <Redirect to='/auth/login' />
                    // <Redirect to='/auth/sso' />
                );
            }
        }

        return (
            <Route {...rest} render={renderRoute} />
        );
    }
}

export default withRouter(PrivateRoute);