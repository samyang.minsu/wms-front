import React, {Component} from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import LoginPage from 'modules/pages/login/Login'
import Sso from 'modules/pages/login/Sso'

class UnauthorizedLayout extends Component {
 
    render() {
        return(
            <div className="unauthorized-layout">
                <Switch>
                    <Route path="/auth/login" exact component={LoginPage} />
                    {/* <Route path="/auth/sso" exact component={Sso} /> */}
                    <Redirect to="/auth/login" />
                </Switch>
            </div>
 
        );
    }
}
export default UnauthorizedLayout